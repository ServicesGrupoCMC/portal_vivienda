package co.com.bbva.portalvivienda.comun;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by jquijano82 on 11/07/17.
 */
public class FormatoFechas {

    public final static String ISO_SHORT = "yyyy-mm-dd";

    public static String formatDate(String formato, Date fecha){
        SimpleDateFormat sdf  = new SimpleDateFormat(formato);
        return sdf.format(fecha);
    }
}
