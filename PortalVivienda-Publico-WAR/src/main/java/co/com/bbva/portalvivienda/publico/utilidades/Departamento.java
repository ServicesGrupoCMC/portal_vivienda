/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.bbva.portalvivienda.publico.utilidades;

import java.io.Serializable;

/**
 *
 * @author wilmer.estrada
 */
public class Departamento implements Serializable{
    private int id;
    private String nombre;
    
    public Departamento(int id, String nombre){
        this.id = id;
        this.nombre = nombre;
    }
    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the name to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    @Override
    public String toString() {
        return this.nombre;
    }
    
}
