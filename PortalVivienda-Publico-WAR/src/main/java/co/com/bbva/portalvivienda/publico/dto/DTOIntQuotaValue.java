
package co.com.bbva.portalvivienda.publico.dto;

import java.io.Serializable;




public class DTOIntQuotaValue implements Serializable{

    public final static long serialVersionUID = 1L;
    private double amount;
    private String currency;

    public DTOIntQuotaValue() {
        //default constructor
    }

    public DTOIntQuotaValue(double amount, String currency) {
        this.amount = amount;
        this.currency = currency;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

}
