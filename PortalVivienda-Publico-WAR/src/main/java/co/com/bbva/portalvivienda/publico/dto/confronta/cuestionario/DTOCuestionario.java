package co.com.bbva.portalvivienda.publico.dto.confronta.cuestionario;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by jquijano82 on 10/07/17.
 */
public class DTOCuestionario implements Serializable {

    private Integer indice;
    private List<DTOPregunta> preguntas;
    private boolean finalizado;
    private boolean error;
    private String mensaje;

    public boolean isUltima(){
        return indice == preguntas.size();
    }

    public List<DTOPregunta> getPreguntas() {
        if(preguntas == null){
            preguntas = new ArrayList<DTOPregunta>();
        }
        return preguntas;
    }

    public Integer getIndice() {
        return indice;
    }

    public void setIndice(Integer indice) {
        this.indice = indice;
    }

    public String getTextoActual(){
        return preguntas.get(indice -1).getTexto();
    }

    public DTOPregunta getPreguntaActual(){
        return preguntas.get(indice-1);
    }

    public boolean isFinalizado() {
        return finalizado;
    }

    public void setFinalizado(boolean finalizado) {
        this.finalizado = finalizado;
    }

    public boolean isError() {
        return error;
    }

    public void setError(boolean error) {
        this.error = error;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }
}
