
package co.com.bbva.portalvivienda.publico.dto;

import java.io.Serializable;




public class DTOIntCountryResidence implements Serializable{

    public final static long serialVersionUID = 1L;
    private String id;
    private String name;

    public DTOIntCountryResidence() {
        //default constructor
    }

    public DTOIntCountryResidence(String id, String name) {
        this.id = id;
        this.name = name;
    }
    

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
