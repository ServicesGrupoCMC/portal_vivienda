package co.com.bbva.portalvivienda.publico;

import co.com.bbva.portalvivienda.publico.dominio.Cuota;
import co.com.bbva.portalvivienda.publico.dominio.Oficina;
import co.com.bbva.portalvivienda.publico.dominio.Simulador;
import co.com.bbva.portalvivienda.publico.utilidades.GeoLocalizacion;
import org.primefaces.context.RequestContext;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.faces.event.AjaxBehaviorEvent;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedHashMap;

public class SimuladorPasoAPaso extends Simulador implements Serializable {


    public SimuladorPasoAPaso() {

        GeoLocalizacion geoLocalizacion = new GeoLocalizacion();
        this.iniVars();
        this.llenarActividadEconomica();
        this.llenarOcupacion();
        this.optionsPais = geoLocalizacion.OpcionesPais();
        this.optionsDepartamento = geoLocalizacion.opcionesDepartamento();
        this.llenarDepartamentoOfi();
        this.llenarZonaOfi();
        this.llenarCuotas();
        this.setCon("hipoteario");
        this.setQuiero("cartera");


    }

    private void llenarCuotas() {
        int consecutivo = 0;
        for (int i = 1; i < 13; i++) {
            consecutivo++;
            Cuota cuota = new Cuota();
            cuota.setMes(i + "");
            cuota.setCapital("$350.000");
            cuota.setIntereses("$800.000");
            cuota.setCuotaSinSeguros("$1.150.000");
            cuota.setSeguroVida("$50.000");
            cuota.setSeguroIncendioTerremoto("$50.000");
            cuota.setCuotaConSeguro("$1.250.000");
            cuota.setSaldo("$179.000.000");
            cuota.setConsecutivo(consecutivo);
            this.cuotas.add(cuota);

        }
    }

    private void llenarOficinas() {
        this.oficinas = new ArrayList<Oficina>();
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = null;
        int id = 0;
        try {
            dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(getClass().getResourceAsStream("/xml/ofi_bbva.xml"));


            NodeList nList = doc.getElementsByTagName("oficina");

            for (int temp = 0; temp < nList.getLength(); temp++) {
                Node nNode = nList.item(temp);

                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element eElement = (Element) nNode;

                    String ciudad = eElement.getElementsByTagName("ciudad").item(0).getTextContent();
                    String zona = eElement.getElementsByTagName("zona").item(0).getTextContent();
                    String nombre = eElement.getElementsByTagName("nombreOficina").item(0).getTextContent();
                    String direccion = eElement.getElementsByTagName("direccion").item(0).getTextContent();
                    String telefono = eElement.getElementsByTagName("telefono").item(0).getTextContent();
                    String horario = eElement.getElementsByTagName("horario").item(0).getTextContent();
                    String horarioAdicionalEntreSemana = eElement.getElementsByTagName("horarioAdicionalEntreSemana").item(0).getTextContent();
                    String horarioAdicionalSabados = eElement.getElementsByTagName("horarioAdicionalSabados").item(0).getTextContent();

                    if (ciudad.equals(this.getCiudadOficina()) && zona.equals(this.getZonaOficina())) {
                        id++;
                        Oficina oficina = new Oficina();
                        oficina.setNombre(nombre);
                        oficina.setDireccion(direccion);
                        oficina.setTelefono(telefono);
                        oficina.setHorario(horario);
                        oficina.setHorarioAdicionalEntreSemana(horarioAdicionalEntreSemana);
                        oficina.setHorarioAdicionalSabados(horarioAdicionalSabados);
                        oficina.setId(id + "");

                        this.oficinas.add(oficina);


                    }

                }
            }
        } catch (SAXException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }


        //RequestContext.getCurrentInstance().execute("alert('Aquí tu código Javascript');");
    }

    private void llenarZonaOfi() {
        this.optionsZonaOficina = new LinkedHashMap<String, Object>();
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = null;
        try {
            dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(getClass().getResourceAsStream("/xml/ofi_zona.xml"));

            NodeList nList = doc.getElementsByTagName("zona");

            for (int temp = 0; temp < nList.getLength(); temp++) {
                Node nNode = nList.item(temp);

                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element eElement = (Element) nNode;

                    this.optionsZonaOficina.put(
                            "ZONA " + eElement.getElementsByTagName("nombre").item(0).getTextContent(),
                            eElement.getElementsByTagName("nombre").item(0).getTextContent()
                    );

                }
            }
        } catch (SAXException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }


    }

    private void llenarDepartamentoOfi() {
        this.optionsCiudadOficina = new LinkedHashMap<String, Object>();
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = null;
        try {
            dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(getClass().getResourceAsStream("/xml/ofi_muni.xml"));

            NodeList nList = doc.getElementsByTagName("municipio");

            for (int temp = 0; temp < nList.getLength(); temp++) {
                Node nNode = nList.item(temp);

                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element eElement = (Element) nNode;

                    this.optionsCiudadOficina.put(
                            eElement.getElementsByTagName("nombre").item(0).getTextContent(),
                            eElement.getElementsByTagName("nombre").item(0).getTextContent()
                    );

                }
            }
        } catch (SAXException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }


    }

    public void departamentoResidenteOnchange(AjaxBehaviorEvent event) {


        if (this.getPaisResidencia().equals("COLOMBIA")) {

            this.llenarDepartamentoResidente();
        } else {

            this.optionsDepartamentoResidente.clear();
        }
    }

    private void llenarDepartamentoResidente() {
        this.optionsDepartamentoResidente = new LinkedHashMap<String, Object>();
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = null;
        try {
            dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(getClass().getResourceAsStream("/xml/dep_colombia.xml"));

            NodeList nList = doc.getElementsByTagName("departamento");

            for (int temp = 0; temp < nList.getLength(); temp++) {
                Node nNode = nList.item(temp);

                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element eElement = (Element) nNode;

                    this.optionsDepartamentoResidente.put(
                            eElement.getElementsByTagName("nombre").item(0).getTextContent(),
                            eElement.getElementsByTagName("nombre").item(0).getTextContent()
                    );

                }
            }
        } catch (SAXException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }


    }

    private void llenarOcupacion() {
        this.optionsOcupacion = new LinkedHashMap<String, Object>();
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = null;
        try {
            dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(getClass().getResourceAsStream("/xml/ocupaciones.xml"));
            NodeList nList = doc.getElementsByTagName("ocupacion");
            for (int temp = 0; temp < nList.getLength(); temp++) {
                Node nNode = nList.item(temp);
                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element eElement = (Element) nNode;
                    this.optionsOcupacion.put(
                            eElement.getElementsByTagName("nombre").item(0).getTextContent(),
                            eElement.getElementsByTagName("id").item(0).getTextContent()
                    );
                }
            }
        } catch (SAXException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    private void llenarActividadEconomica() {
        this.optionsActividadEconomica = new LinkedHashMap<String, Object>();
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = null;
        try {
            dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(getClass().getResourceAsStream("/xml/actividadEconomica.xml"));

            NodeList nList = doc.getElementsByTagName("actividadEconomica");

            for (int temp = 0; temp < nList.getLength(); temp++) {
                Node nNode = nList.item(temp);

                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element eElement = (Element) nNode;
                    this.optionsActividadEconomica.put(
                            eElement.getElementsByTagName("nombre").item(0).getTextContent(),
                            eElement.getElementsByTagName("id").item(0).getTextContent()
                    );

                }
            }
        } catch (SAXException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public void changeOptionsQuiero() {


        String value = this.getQuiero();
        this.optionCon.clear();

        /*this.optionQuiero.put("compra de vivienda", "vivienda");
        this.optionQuiero.put("compra de cartera", "cartera");
		this.optionQuiero.put("remodelación", "remodelacion");
		this.optionQuiero.put("construcción", "construccion");
		this.optionQuiero.put("cesión", "cesion");*/
 /*this.optionCon.put("crédito hipotecario", "hipoteario");
		this.optionCon.put("leasing habitacional", "habitacional");*/
        if (value.contentEquals("vivienda")
                || value.contentEquals("cartera")
                || value.contentEquals("remodelacion")
                || value.contentEquals("construcción vivienda")
                || value.contentEquals("")) {

            this.optionCon.put("crédito hipotecario", "hipoteario");
            this.setCon("hipoteario");

        }

        if (value.contentEquals("vivienda")
                || value.contentEquals("cartera")
                || value.contentEquals("cesion")
                || value.contentEquals("")) {

            this.optionCon.put("leasing habitacional", "habitacional");
            this.setCon("habitacional");
        }


    }

    public void linkUvr() {
        this.setMonedaCredito("U");

    }

    public void linkPesos() {
        this.setMonedaCredito("P");

    }

    public void contenedorProyectoFinanciado() {

        if (this.getNueva().equals("Si")) {

            RequestContext.getCurrentInstance().execute("document.getElementById('divProyectoFinanciado').style.display = 'block';");
        } else {

            RequestContext.getCurrentInstance().execute("document.getElementById('divProyectoFinanciado').style.display = 'none';");
        }
    }

    public void changeCiudadOficina() {
        this.setOficina(new Oficina());

        this.setMensageNoRecords("Seleccione la ciudad");
        if (this.getCiudadOficina().equals("BOGOTÁ D.C.")) {
            this.setMensageNoRecords("Seleccione la zona");
            this.oficinas.clear();
        } else {

            this.setZonaOficina("EMPTY");
            this.llenarOficinas();
        }
    }

    public void changeZonaOficinas() {
        this.setOficina(new Oficina());

        this.llenarOficinas();
    }

    public void changeOficina(AjaxBehaviorEvent event) {
        this.setOficina((Oficina) event.getComponent().getAttributes().get("oficina"));
    }

    public long getValorVivienda() {
        return valorVivienda;
    }

    public void setValorVivienda(long valorVivienda) {

        this.valorVivienda = valorVivienda;

    }


    public String toString() {
        return super.toString();
    }
}
