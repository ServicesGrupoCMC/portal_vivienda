
package co.com.bbva.portalvivienda.publico.dto;

import java.io.Serializable;




public class DTOIntAdditionalCosts implements Serializable {

    public final static long serialVersionUID = 1L;
    private DTOIntBuyer buyer;
    private DTOIntSeller seller;

    public DTOIntAdditionalCosts() {
        //default constructor
    }

    public DTOIntAdditionalCosts(DTOIntBuyer buyer, DTOIntSeller seller) {
        this.buyer = buyer;
        this.seller = seller;
    }
    
    public DTOIntBuyer getBuyer() {
        return buyer;
    }

    public void setBuyer(DTOIntBuyer buyer) {
        this.buyer = buyer;
    }

    public DTOIntSeller getSeller() {
        return seller;
    }

    public void setSeller(DTOIntSeller seller) {
        this.seller = seller;
    }

}
