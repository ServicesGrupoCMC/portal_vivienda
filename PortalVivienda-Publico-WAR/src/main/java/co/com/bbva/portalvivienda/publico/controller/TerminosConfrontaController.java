package co.com.bbva.portalvivienda.publico.controller;

import co.com.bbva.portalvivienda.publico.dto.confronta.terminos.DTOTerminosConfronta;

import java.io.Serializable;

/**
 * Created by jquijano82 on 10/07/17.
 */
public class TerminosConfrontaController implements Serializable{
    public TerminosConfrontaController() {
    }

    public boolean validarTerminos(DTOTerminosConfronta dto){
        return dto.isAceptoConsultaCentrales() && dto.isAceptoTerminos();
    }
}
