/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.bbva.portalvivienda.publico.controller;

import co.com.bbva.portalvivienda.publico.dto.DTOIntLoanSimulation;
import org.springframework.webflow.execution.RequestContext;
import org.springframework.webflow.execution.RequestContextHolder;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;
import java.util.regex.Pattern;

/**
 * @author wilmer.estrada
 */

public class DocumentValidator implements Validator {

    private static final String NUMBER_PATTERN = "[0-9]*";
    private static final String STRING_PATTERN = "[a-zA-Z0-9]*";


    @Override
    public void validate(FacesContext context, UIComponent component, Object value) throws ValidatorException {

        RequestContext context2 = RequestContextHolder.getRequestContext();
        DTOIntLoanSimulation dtoLoanSimulation = (DTOIntLoanSimulation) context2.getFlowScope().get("dtoLoanSimulation");
        String typeDocument = dtoLoanSimulation.getDocumentType().getId();

        if (typeDocument.equals("5")) {

            if (!Pattern.matches(STRING_PATTERN, value.toString())) {
                FacesMessage msg = new FacesMessage("Numero de Documento debe ser alfanumerico");
                msg.setSeverity(FacesMessage.SEVERITY_ERROR);
                throw new ValidatorException(msg);
            }
        } else {

            if (!Pattern.matches(NUMBER_PATTERN, value.toString())) {
                FacesMessage msg = new FacesMessage("Numero de Documento debe ser numerico");
                msg.setSeverity(FacesMessage.SEVERITY_ERROR);
                throw new ValidatorException(msg);
            }
        }
    }
}
