
package co.com.bbva.portalvivienda.publico.dto;

import java.io.Serializable;
import java.util.List;




public class DTOIntAmortizationTable implements Serializable {

    public final static long serialVersionUID = 1L;
    private DTOIntVtuPaymentCapital vtuPaymentCapital;
    private DTOIntVtuPaymentInterest vtuPaymentInterest;
    private DTOIntVtuPaymentInsurance vtuPaymentInsurance;
    private DTOIntVtuPercentage vtuPercentage;
    private List<DTOIntMonthlyAmortization> monthlyAmortization;

    public DTOIntAmortizationTable() {
        //default constructor
    }
    
    public DTOIntAmortizationTable(DTOIntVtuPaymentCapital vtuPaymentCapital,DTOIntVtuPaymentInterest vtuPaymentInterest,DTOIntVtuPaymentInsurance vtuPaymentInsurance,DTOIntVtuPercentage vtuPercentage,List<DTOIntMonthlyAmortization> monthlyAmortization) {
        this.vtuPaymentCapital = vtuPaymentCapital;
        this.vtuPaymentInterest = vtuPaymentInterest;
        this.vtuPaymentInsurance = vtuPaymentInsurance;
        this.vtuPercentage = vtuPercentage;
        this.monthlyAmortization = monthlyAmortization;
    }

    public DTOIntVtuPaymentCapital getVtuPaymentCapital() {
        return vtuPaymentCapital;
    }

    public void setVtuPaymentCapital(DTOIntVtuPaymentCapital vtuPaymentCapital) {
        this.vtuPaymentCapital = vtuPaymentCapital;
    }

    public DTOIntVtuPaymentInterest getVtuPaymentInterest() {
        return vtuPaymentInterest;
    }

    public void setVtuPaymentInterest(DTOIntVtuPaymentInterest vtuPaymentInterest) {
        this.vtuPaymentInterest = vtuPaymentInterest;
    }

    public DTOIntVtuPaymentInsurance getVtuPaymentInsurance() {
        return vtuPaymentInsurance;
    }

    public void setVtuPaymentInsurance(DTOIntVtuPaymentInsurance vtuPaymentInsurance) {
        this.vtuPaymentInsurance = vtuPaymentInsurance;
    }

    public DTOIntVtuPercentage getVtuPercentage() {
        return vtuPercentage;
    }

    public void setVtuPercentage(DTOIntVtuPercentage vtuPercentage) {
        this.vtuPercentage = vtuPercentage;
    }

    /**
     * @return the monthlyAmortization
     */
    public List<DTOIntMonthlyAmortization> getMonthlyAmortization() {
        return monthlyAmortization;
    }

    /**
     * @param monthlyAmortization the monthlyAmortization to set
     */
    public void setMonthlyAmortization(List<DTOIntMonthlyAmortization> monthlyAmortization) {
        this.monthlyAmortization = monthlyAmortization;
    }

}
