package co.com.bbva.portalvivienda.publico.dto;

import java.io.Serializable;

public class DTOIntAmountToFinanced implements Serializable{

    public final static long serialVersionUID = 1L;
    private double amount;
    private String currency;

    public DTOIntAmountToFinanced(double amount, String currency) {
        this.amount = amount;
        this.currency = currency;
    }

    public DTOIntAmountToFinanced() {
        //default constructor
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

}
