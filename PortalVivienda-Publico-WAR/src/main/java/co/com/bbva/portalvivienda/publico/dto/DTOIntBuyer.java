
package co.com.bbva.portalvivienda.publico.dto;

import java.io.Serializable;




public class DTOIntBuyer implements Serializable{

    public final static long serialVersionUID = 1L;
    private DTOIntOtherExpenses otherExpenses;
    private DTOIntNotaryFees notaryFees;
    private DTOIntRegistrationFees registrationFees;
    private DTOIntTotalSpends totalSpends;

    public DTOIntBuyer() {
        //default constructor
    }

    public DTOIntBuyer(DTOIntOtherExpenses otherExpenses, DTOIntNotaryFees notaryFees, DTOIntRegistrationFees registrationFees, DTOIntTotalSpends totalSpends) {
        this.otherExpenses = otherExpenses;
        this.notaryFees = notaryFees;
        this.registrationFees = registrationFees;
        this.totalSpends = totalSpends;
    }
    

    public DTOIntOtherExpenses getOtherExpenses() {
        return otherExpenses;
    }

    public void setOtherExpenses(DTOIntOtherExpenses otherExpenses) {
        this.otherExpenses = otherExpenses;
    }

    public DTOIntNotaryFees getNotaryFees() {
        return notaryFees;
    }

    public void setNotaryFees(DTOIntNotaryFees notaryFees) {
        this.notaryFees = notaryFees;
    }

    public DTOIntRegistrationFees getRegistrationFees() {
        return registrationFees;
    }

    public void setRegistrationFees(DTOIntRegistrationFees registrationFees) {
        this.registrationFees = registrationFees;
    }

    public DTOIntTotalSpends getTotalSpends() {
        return totalSpends;
    }

    public void setTotalSpends(DTOIntTotalSpends totalSpends) {
        this.totalSpends = totalSpends;
    }

}
