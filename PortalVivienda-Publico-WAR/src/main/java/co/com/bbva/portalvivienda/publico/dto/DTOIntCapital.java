
package co.com.bbva.portalvivienda.publico.dto;

import java.io.Serializable;




public class DTOIntCapital implements Serializable{

    public final static long serialVersionUID = 1L;
    private Double amount;
    private String currency;

    public DTOIntCapital() {
        //default constructor
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

}
