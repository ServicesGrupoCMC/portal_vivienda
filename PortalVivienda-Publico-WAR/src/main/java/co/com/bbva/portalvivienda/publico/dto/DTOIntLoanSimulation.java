
package co.com.bbva.portalvivienda.publico.dto;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;


public class DTOIntLoanSimulation implements Serializable {

    private static long serialVersionUID = 1L;

    public DTOIntLoanSimulation() {
        //default constructor
    }

    //lista de opciones
    private Map<String, String> financialLines;
    private Map<String, String> financialSublines;

    // Datos de Entrada
    private String financialLine;
    private String financialSubline;
    private String simulationTypeIndicator;
    private DTOIntPriceHousing priceHousing;
    private DTOIntMontlyIncome montlyIncome;
    private DTOIntAmountToFinanced amountToFinanced;
    private int yearsToFinanced;
    private String buyPercentage;
    private double yearIntegerAmortization;
    private String amortizationType = "P";
    private boolean isGovernmentCoverage;
    private boolean isLinkedMilitaryForces;
    private boolean isLivesInTheCountry;
    private boolean isBbvaPayroll;
    private boolean isHousingInProjectBbva;
    private boolean isLiveOnProperty;
    private boolean isNewHousing;

    private String names;
    private String surnames;
    private DTOIntDocumentType documentType;
    private String documentInteger;
    private Date birthDate;
    private String fechaNacimiento;
    private DTOIntCountryResidence countryResidence;
    private String stateResidence;
    private String cityResidence;
    private String email;
    private String phone;
    private String cellPhone;
    private Date fechaContacto;
    private String horarioContacto;
    private String numeroContacto;
    private String yearsToFinancedString;
    private String mensajeValidacion;

    // Datos de Salida

    private double totalCuotaMensual;

    private DTOIntQuotaValue quotaValue;
    private double percentageFixedQuota;
    private int subProduct;
    private double rateCoverage;
    private DTOIntValueCapitalAmount valueCapitalAmount;
    private double loanTermYears;
    private DTOIntValueInterestBonus valueInterestBonus;
    private DTOIntLifeInsuranceValue lifeInsuranceValue;
    private DTOIntValueFireEarthquake valueFireEarthquake;
    private DTOIntValueCoverage valueCoverage;
    private DTOIntAdditionalCosts additionalCosts;
    private DTOIntAmortizationTable amortizationTable;

    public Map<String, String> getFinancialLines() {
        return financialLines;
    }

    public void setFinancialLines(Map<String, String> financialLines) {
        this.financialLines = financialLines;
    }

    public Map<String, String> getFinancialSublines() {
        return financialSublines;
    }

    public void setFinancialSublines(Map<String, String> financialSublines) {
        this.financialSublines = financialSublines;
    }

    public String getFinancialLine() {
        return financialLine;
    }

    public void setFinancialLine(String financialLine) {
        this.financialLine = financialLine;
    }

    public String getFinancialSubline() {
        return financialSubline;
    }

    public void setFinancialSubline(String financialSubline) {
        this.financialSubline = financialSubline;
    }

    public String getSimulationTypeIndicator() {
        return simulationTypeIndicator;
    }

    public void setSimulationTypeIndicator(String simulationTypeIndicator) {
        this.simulationTypeIndicator = simulationTypeIndicator;
    }

    public DTOIntPriceHousing getPriceHousing() {
        return priceHousing;
    }

    public void setPriceHousing(DTOIntPriceHousing priceHousing) {
        this.priceHousing = priceHousing;
    }

    public DTOIntMontlyIncome getMontlyIncome() {
        return montlyIncome;
    }

    public void setMontlyIncome(DTOIntMontlyIncome montlyIncome) {
        this.montlyIncome = montlyIncome;
    }

    public DTOIntAmountToFinanced getAmountToFinanced() {
        return amountToFinanced;
    }

    public void setAmountToFinanced(DTOIntAmountToFinanced amountToFinanced) {
        this.amountToFinanced = amountToFinanced;
    }

    public int getYearsToFinanced() {
        return yearsToFinanced;
    }

    public void setYearsToFinanced(int yearsToFinanced) {
        this.yearsToFinanced = yearsToFinanced;
    }

    public String getBuyPercentage() {
        return buyPercentage;
    }

    public void setBuyPercentage(String buyPercentage) {
        this.buyPercentage = buyPercentage;
    }

    public double getYearIntegerAmortization() {
        return yearIntegerAmortization;
    }

    public void setYearIntegerAmortization(double yearIntegerAmortization) {
        this.yearIntegerAmortization = yearIntegerAmortization;
    }

    public String getAmortizationType() {
        return amortizationType;
    }

    public void setAmortizationType(String amortizationType) {
        this.amortizationType = amortizationType;
    }

    public boolean isGovernmentCoverage() {
        return isGovernmentCoverage;
    }

    public void setGovernmentCoverage(boolean governmentCoverage) {
        isGovernmentCoverage = governmentCoverage;
    }

    public boolean isLinkedMilitaryForces() {
        return isLinkedMilitaryForces;
    }

    public void setLinkedMilitaryForces(boolean linkedMilitaryForces) {
        isLinkedMilitaryForces = linkedMilitaryForces;
    }

    public boolean isLivesInTheCountry() {
        return isLivesInTheCountry;
    }

    public void setLivesInTheCountry(boolean livesInTheCountry) {
        isLivesInTheCountry = livesInTheCountry;
    }

    public boolean isBbvaPayroll() {
        return isBbvaPayroll;
    }

    public void setBbvaPayroll(boolean bbvaPayroll) {
        isBbvaPayroll = bbvaPayroll;
    }

    public boolean isHousingInProjectBbva() {
        return isHousingInProjectBbva;
    }

    public void setHousingInProjectBbva(boolean housingInProjectBbva) {
        isHousingInProjectBbva = housingInProjectBbva;
    }

    public boolean isLiveOnProperty() {
        return isLiveOnProperty;
    }

    public void setLiveOnProperty(boolean liveOnProperty) {
        isLiveOnProperty = liveOnProperty;
    }

    public boolean isNewHousing() {
        return isNewHousing;
    }

    public void setNewHousing(boolean newHousing) {
        isNewHousing = newHousing;
    }

    public String getNames() {
        return names;
    }

    public void setNames(String names) {
        this.names = names;
    }

    public String getSurnames() {
        return surnames;
    }

    public void setSurnames(String surnames) {
        this.surnames = surnames;
    }

    public DTOIntDocumentType getDocumentType() {
        return documentType;
    }

    public void setDocumentType(DTOIntDocumentType documentType) {
        this.documentType = documentType;
    }

    public String getDocumentInteger() {
        return documentInteger;
    }

    public void setDocumentInteger(String documentInteger) {
        this.documentInteger = documentInteger;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(String fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public DTOIntCountryResidence getCountryResidence() {
        return countryResidence;
    }

    public void setCountryResidence(DTOIntCountryResidence countryResidence) {
        this.countryResidence = countryResidence;
    }

    public String getStateResidence() {
        return stateResidence;
    }

    public void setStateResidence(String stateResidence) {
        this.stateResidence = stateResidence;
    }

    public String getCityResidence() {
        return cityResidence;
    }

    public void setCityResidence(String cityResidence) {
        this.cityResidence = cityResidence;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCellPhone() {
        return cellPhone;
    }

    public void setCellPhone(String cellPhone) {
        this.cellPhone = cellPhone;
    }

    public Date getFechaContacto() {
        return fechaContacto;
    }

    public void setFechaContacto(Date fechaContacto) {
        this.fechaContacto = fechaContacto;
    }

    public String getHorarioContacto() {
        return horarioContacto;
    }

    public void setHorarioContacto(String horarioContacto) {
        this.horarioContacto = horarioContacto;
    }

    public String getNumeroContacto() {
        return numeroContacto;
    }

    public void setNumeroContacto(String numeroContacto) {
        this.numeroContacto = numeroContacto;
    }

    public double getTotalCuotaMensual() {
        return totalCuotaMensual;
    }

    public void setTotalCuotaMensual(double totalCuotaMensual) {
        this.totalCuotaMensual = totalCuotaMensual;
    }

    public DTOIntQuotaValue getQuotaValue() {
        return quotaValue;
    }

    public void setQuotaValue(DTOIntQuotaValue quotaValue) {
        this.quotaValue = quotaValue;
    }

    public double getPercentageFixedQuota() {
        return percentageFixedQuota;
    }

    public void setPercentageFixedQuota(double percentageFixedQuota) {
        this.percentageFixedQuota = percentageFixedQuota;
    }

    public int getSubProduct() {
        return subProduct;
    }

    public void setSubProduct(int subProduct) {
        this.subProduct = subProduct;
    }

    public double getRateCoverage() {
        return rateCoverage;
    }

    public void setRateCoverage(double rateCoverage) {
        this.rateCoverage = rateCoverage;
    }

    public DTOIntValueCapitalAmount getValueCapitalAmount() {
        return valueCapitalAmount;
    }

    public void setValueCapitalAmount(DTOIntValueCapitalAmount valueCapitalAmount) {
        this.valueCapitalAmount = valueCapitalAmount;
    }

    public double getLoanTermYears() {
        return loanTermYears;
    }

    public void setLoanTermYears(double loanTermYears) {
        this.loanTermYears = loanTermYears;
    }

    public DTOIntValueInterestBonus getValueInterestBonus() {
        return valueInterestBonus;
    }

    public void setValueInterestBonus(DTOIntValueInterestBonus valueInterestBonus) {
        this.valueInterestBonus = valueInterestBonus;
    }

    public DTOIntLifeInsuranceValue getLifeInsuranceValue() {
        return lifeInsuranceValue;
    }

    public void setLifeInsuranceValue(DTOIntLifeInsuranceValue lifeInsuranceValue) {
        this.lifeInsuranceValue = lifeInsuranceValue;
    }

    public DTOIntValueFireEarthquake getValueFireEarthquake() {
        return valueFireEarthquake;
    }

    public void setValueFireEarthquake(DTOIntValueFireEarthquake valueFireEarthquake) {
        this.valueFireEarthquake = valueFireEarthquake;
    }

    public DTOIntValueCoverage getValueCoverage() {
        return valueCoverage;
    }

    public void setValueCoverage(DTOIntValueCoverage valueCoverage) {
        this.valueCoverage = valueCoverage;
    }

    public DTOIntAdditionalCosts getAdditionalCosts() {
        return additionalCosts;
    }

    public void setAdditionalCosts(DTOIntAdditionalCosts additionalCosts) {
        this.additionalCosts = additionalCosts;
    }

    public DTOIntAmortizationTable getAmortizationTable() {
        return amortizationTable;
    }

    public void setAmortizationTable(DTOIntAmortizationTable amortizationTable) {
        this.amortizationTable = amortizationTable;
    }

	public String getYearsToFinancedString() {
		return yearsToFinancedString;
	}

	public void setYearsToFinancedString(String yearsToFinancedString) {
		this.yearsToFinancedString = yearsToFinancedString;
	}

	public String getMensajeValidacion() {
		return mensajeValidacion;
	}

	public void setMensajeValidacion(String mensajeValidacion) {
		this.mensajeValidacion = mensajeValidacion;
	}
}