package co.com.bbva.portalvivienda.publico.controller;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashMap;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.webflow.execution.RequestContext;
import org.springframework.webflow.execution.RequestContextHolder;

import com.bbva.czic.appointments.facade.v00.dto.AmountToFinanced;
import com.bbva.czic.appointments.facade.v00.dto.Appointment;
import com.bbva.czic.appointments.facade.v00.dto.CityResidence;
import com.bbva.czic.appointments.facade.v00.dto.CountryBirth;
import com.bbva.czic.appointments.facade.v00.dto.CountryResidence;
import com.bbva.czic.appointments.facade.v00.dto.DocumentType;
import com.bbva.czic.appointments.facade.v00.dto.PriceOfHousing;
import com.bbva.czic.appointments.facade.v00.dto.StateResidence;

import co.com.bbva.portalvivienda.back.impl.AppoimentClientImpl;
import co.com.bbva.portalvivienda.publico.AgendamientoBean;
import co.com.bbva.portalvivienda.publico.dto.DTOIntLoanSimulation;
import co.com.bbva.portalvivienda.publico.utilidades.GeoLocalizacion;

/**
 * Created by JulianLopezRomero on 12/07/2017.
 */
public class AgendamientoController implements Serializable {
	@Autowired
	private transient AppoimentClientImpl agCliente;
	Appointment cliente;
	DocumentType tipoDoc;
	CityResidence ciudadRes;
	StateResidence deptoRes;
	CountryResidence paisRes;
	CountryBirth paisNac;
	AmountToFinanced monto;
	PriceOfHousing precio;
	String mensajeError;
	
	private LinkedHashMap<String,Object> ciudades = new LinkedHashMap<String,Object>();
	private final static Logger LOG = Logger.getLogger(CuestionarioConfrontaController.class);
	public void generarAgendamiento(){
		validaCampos();
		if(!mensajeError.isEmpty()){
			actualizaRespuesta();
			return;
		}
		try{
			Response respuesta = agCliente.createAppoiment(cliente);
			FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Agendamiento exitoso", mensajeError);
			FacesContext.getCurrentInstance().addMessage(null, message);			
			LOG.error("Respuesta Agendamiento " + respuesta.getStatus());
			actualizaRespuesta();			
		}
		catch (Exception err) {
	        mensajeError = err.getMessage();
			actualizaRespuesta();
			FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Agendamiento no existoso", err.getMessage());
			FacesContext.getCurrentInstance().addMessage(null, message);			
	        LOG.error("Error Agendamiento " + err.getMessage(), err);
	    }
	}

	public void generarAgendamientoSimulacion(){
		validaCamposSimulacion();
		try{
			if(mensajeError.isEmpty()){
				Response respuesta = agCliente.createAppoiment(cliente);
				FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Agendamiento exitoso", mensajeError);
				FacesContext.getCurrentInstance().addMessage(null, message);
				LOG.error("Respuesta Agendamiento " + respuesta.getStatus());
			} else {
				FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Agendamiento exitoso", mensajeError);
				FacesContext.getCurrentInstance().addMessage(null, message);				
				LOG.error("Error Agendamiento " + mensajeError);
			}
		}
		catch (Exception err) {
			mensajeError = err.getMessage();
			FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Agendamiento no exitoso", mensajeError);
			FacesContext.getCurrentInstance().addMessage(null, message);
	        LOG.error("Error Agendamiento " + err.getMessage(), err);
	    }
	}
    public void onDepartamentosChange() {
        RequestContext context = RequestContextHolder.getRequestContext();
        AgendamientoBean agendamientoBean = (AgendamientoBean) context.getFlowScope().get("agendamientoBean");
        String state = agendamientoBean.getDepartamentoResiencia();
        String depto = state;
        String pais = agendamientoBean.getPaisResidencia();
        agendamientoBean.setCiudadResidencia("");
        this.getCiudades().clear();
        if (state != null && !state.equals("")) {
        	agendamientoBean.setOptionsCiudades(ciudades);
        	GeoLocalizacion g = new GeoLocalizacion();
            ciudades = g.opcionesCiudades(pais, depto);
        }
        agendamientoBean.setOptionsCiudades(this.ciudades);
    }
    
	private LinkedHashMap<String,Object> getCiudades() {
		return ciudades;
	}

//	public void validaCampos(AgendamientoBean agendamientoBean){	
	public void validaCampos(){
        RequestContext context = RequestContextHolder.getRequestContext();
        AgendamientoBean agendamientoBean = (AgendamientoBean) context.getFlowScope().get("agendamientoBean");		
		cliente = new Appointment();
		mensajeError = "";
//MontoFinanciero        
		monto = new AmountToFinanced(); 
		monto.setAmount(0.0);
		monto.setCurrency("COP");
		cliente.setAmountToFinanced(monto);
//NumeroCelular
		if(agendamientoBean.getCelular().isEmpty()){ 
			mensajeError = "Se requiere un celular para continuar"; 
			agendamientoBean.setMensajeError(mensajeError);
			return;	
		}
		else{ 
			cliente.setCellPhone(longitudNumero(agendamientoBean.getCelular(),10)); 
		}
//CiudadResidencia
		if(agendamientoBean.getCiudadResidencia().isEmpty() || agendamientoBean.getCiudadResidencia().equals("Seleccione")){ 
			mensajeError = "Se requiere una ciudad para continuar"; 
			agendamientoBean.setMensajeError(mensajeError);
			return;	
		}
		else{ 
			ciudadRes = new CityResidence();
			ciudadRes.setId(longitudRegistro(agendamientoBean.getCiudadResidencia(),4)); 
//			ciudadRes.setId("1101");			
			ciudadRes.setName(agendamientoBean.getCiudadResidencia());
			cliente.setCityResidence(ciudadRes);
		}		
//PaisNacimiento
		if(agendamientoBean.getPaisNacimiento().isEmpty() || agendamientoBean.getPaisNacimiento().equals("Seleccione")){
			mensajeError = "Se requiere pais de nacimiento para continuar";
			agendamientoBean.setMensajeError(mensajeError);
			return;				
		} else{
			paisNac = new CountryBirth();
			
			paisNac.setId(longitudRegistro(agendamientoBean.getPaisNacimiento(),3));
			paisNac.setName("2010-" + convetirFecha(agendamientoBean.getFechaContacto()).substring(5));
			cliente.setCountryBirth(paisNac);
		}		
//PaisResidencia
		if(agendamientoBean.getPaisResidencia().isEmpty() || agendamientoBean.getPaisResidencia().equals("Seleccione")){ 
			mensajeError = "Se requiere un país para continuar"; 
			agendamientoBean.setMensajeError(mensajeError);
			return;	
		}
		else{
			paisRes = new CountryResidence();
			paisRes.setId(longitudRegistro(agendamientoBean.getPaisResidencia(),3));
			paisRes.setName(agendamientoBean.getPaisResidencia());
			cliente.setCountryResidence(paisRes);
		}		
//DocumentoIdentidad
		if(agendamientoBean.getIdentificacion().isEmpty()){ 
			mensajeError = "Se requiere una identificación para continuar"; 
			agendamientoBean.setMensajeError(mensajeError);
			return;	
		}
		else{
			cliente.setDocumentNumber(longitudRegistro(agendamientoBean.getIdentificacion(),15)); }		
//TipoDocumento
		if(agendamientoBean.getTipoIdentificacion().isEmpty() || agendamientoBean.getTipoIdentificacion().equals("Seleccione")){ 
			mensajeError = "Se requiere el tipo de documento para continuar"; 
			agendamientoBean.setMensajeError(mensajeError);
			return;	
		}		
		else{
			tipoDoc = new DocumentType();
			tipoDoc.setId(agendamientoBean.getTipoIdentificacion());
			tipoDoc.setName(agendamientoBean.getTipoIdentificacion());
			cliente.setDocumentType(tipoDoc);
		}
//CorreoElectronico
		if(agendamientoBean.getCorreo().isEmpty()){ 
			mensajeError = "Se requiere un correo para continuar"; 
			agendamientoBean.setMensajeError(mensajeError);
			return;	
		}
		else{ cliente.setEmail(agendamientoBean.getCorreo()); }
//NombreCliente
		if(agendamientoBean.getNombre().isEmpty()){ 
			mensajeError = "Se requiere el nombre para continuar";
			agendamientoBean.setMensajeError(mensajeError);
			return;
		} 
		else { cliente.setNames(agendamientoBean.getNombre()); }		
//OrigenAgendamiento
		if(agendamientoBean.getTerminos().toString().isEmpty()){ 
			mensajeError = "Se requiere que acepte los terminos y condiciones para continuar"; 
			agendamientoBean.setMensajeError(mensajeError);
			return;	
		} 
		else{ cliente.setOriginAppointmentScheduling("01"); }
//TelefonoFijo
		if(agendamientoBean.getTelefono().isEmpty()){
			mensajeError = "Se requiere un numero de telefono para continuar"; 
			agendamientoBean.setMensajeError(mensajeError);
			return;				
		} else { 
			cliente.setPhone(longitudRegistro(agendamientoBean.getTelefono(),10)); }
//PrecioVivienda
		precio = new PriceOfHousing();
		precio.setAmount(0.0);
		precio.setCurrency("0");
		cliente.setPriceOfHousing(precio);
//ScoreCifin
		cliente.setReplyScoreBuro(0);
//AgendamientoHorario
		if(agendamientoBean.getFechaContacto().toString().isEmpty()){ 
			mensajeError = "Se requiere un horario de contacto para continuar"; 
			agendamientoBean.setMensajeError(mensajeError);
			return;	
		}
		else{ cliente.setSchedule(agendamientoBean.getHorario().toUpperCase()); }		
//EstadoResidencia
		if(agendamientoBean.getDepartamentoResiencia().isEmpty() || agendamientoBean.getDepartamentoResiencia().equals("Seleccione")){ 
			mensajeError = "Se requiere un departamento para continuar"; 
			agendamientoBean.setMensajeError(mensajeError);
			return;	
		}
		else{
			deptoRes = new StateResidence();
			deptoRes.setId(longitudRegistro(agendamientoBean.getDepartamentoResiencia(),4));
//			deptoRes.setId(longitudRegistro("11",4));
			deptoRes.setName(convetirFecha(agendamientoBean.getFechaContacto()));
			cliente.setStateResidence(deptoRes);
		}
//AgendamientoHorario		
		if( agendamientoBean.getHorario().isEmpty()){ 
			mensajeError = "Se requiere un periodo de contacto"; 
			agendamientoBean.setMensajeError(mensajeError);
			return;	
		}
		else{ cliente.setDateContact(convetirFecha(agendamientoBean.getFechaContacto())); }
//AsuntoAgendamiento
		if( agendamientoBean.getAsunto().isEmpty()){ 
			mensajeError = "Se requiere un asunto para continuar"; 
			agendamientoBean.setMensajeError(mensajeError);
			return;	
		}
		else{ cliente.setSubject("Credito Vivienda"); }		
//SubProducto
		cliente.setSubProduct(1);		
//ApellidoCliente		
		if(agendamientoBean.getApellido().isEmpty()){ 
			mensajeError = "Se requiere el/los apellidos para continuar"; 
			agendamientoBean.setMensajeError(mensajeError);
			return;	
		}
		else{ cliente.setSurname(agendamientoBean.getApellido()); }
//Asignar ID temporal
		cliente.setId(0);
//Captcha
		LOG.error("Captcha "+agendamientoBean.getCaptchaVal().toString());
		if(agendamientoBean.getCaptchaVal().toString().isEmpty() || agendamientoBean.getCaptchaVal().toString().toUpperCase().equals("FALSE")){ 
			mensajeError = "Se requiere validar si no es un robot para continuar"; 
			agendamientoBean.setMensajeError(mensajeError);
			return;	
		}
	}

	public void actualizaRespuesta(){
        if(mensajeError.isEmpty()){
			FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, "Agendamiento exitoso", mensajeError);
			FacesContext.getCurrentInstance().addMessage(null, message);
        } else {
			FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Agendamiento no exitoso", mensajeError);
			FacesContext.getCurrentInstance().addMessage(null, message);
        }
	}
	
	public void validaCamposSimulacion(){
		cliente = new Appointment();
		mensajeError = "";
		RequestContext context = RequestContextHolder.getRequestContext();
        DTOIntLoanSimulation loanSimulation = (DTOIntLoanSimulation) context.getFlowScope().get("dtoLoanSimulation");
//MontoFinanciero
		monto = new AmountToFinanced(); 
		monto.setAmount(loanSimulation.getAmountToFinanced().getAmount());
		monto.setCurrency(loanSimulation.getAmountToFinanced().getCurrency());
		cliente.setAmountToFinanced(monto);
//NumeroCelular
		if(loanSimulation.getCellPhone().isEmpty()){ 
			mensajeError = "Se requiere un celular para continuar"; 
		}
		else{ 
			cliente.setCellPhone(longitudNumero(loanSimulation.getCellPhone(),10)); 
		}
//CiudadResidencia
		if(loanSimulation.getCityResidence().isEmpty() || loanSimulation.getCityResidence().equals("Seleccione")){ 
			mensajeError = "Se requiere una ciudad para continuar"; 
		}
		else{ 
			ciudadRes = new CityResidence();
			ciudadRes.setId(longitudRegistro(loanSimulation.getCityResidence(),4)); 
//			ciudadRes.setId(longitudRegistro("1101",4));			
			ciudadRes.setName(loanSimulation.getCityResidence());
			cliente.setCityResidence(ciudadRes);
		}		
//PaisNacimiento
		if(loanSimulation.getCountryResidence().getId().isEmpty() || loanSimulation.getCountryResidence().getId().equals("Seleccione")){ 
			mensajeError = "Se requiere un país para continuar"; 
		}
		else{
			paisNac = new CountryBirth();
			paisNac.setId(longitudRegistro(loanSimulation.getCountryResidence().getId(),3));
			paisNac.setName(convetirFecha(loanSimulation.getBirthDate()));
			cliente.setCountryBirth(paisNac);
		}		
//PaisResidencia
		if(loanSimulation.getCountryResidence().getId().isEmpty() || loanSimulation.getCountryResidence().getId().equals("Seleccione")){ 
			mensajeError = "Se requiere un país para continuar"; 
		}
		else{
			paisRes = new CountryResidence();
			paisRes.setId(longitudRegistro(loanSimulation.getCountryResidence().getId(),3));
			paisRes.setName(loanSimulation.getCountryResidence().getId());
			cliente.setCountryResidence(paisRes);
		}
//NumeroDocumento		
		if(loanSimulation.getDocumentInteger().isEmpty()){ 
			mensajeError = "Se requiere una identificación para continuar"; 
		}
		else{ 
			cliente.setDocumentNumber(longitudRegistro(loanSimulation.getDocumentInteger(),15)); 
		}
//TipoDocumento		
		if(loanSimulation.getDocumentType().getId().isEmpty() || loanSimulation.getDocumentType().getId().equals("Seleccione")){ 
			mensajeError = "Se requiere el tipo de documento para continuar"; 
		}		
		else{
			tipoDoc = new DocumentType();
			tipoDoc.setId(loanSimulation.getDocumentType().getId());
			tipoDoc.setName(loanSimulation.getDocumentType().getId());
			cliente.setDocumentType(tipoDoc);
		}		
//CorreoElectronico		
		if(loanSimulation.getEmail().isEmpty()){ 
			mensajeError = "Se requiere un correo para continuar"; 
		}
		else{ cliente.setEmail(loanSimulation.getEmail()); }		
//NombreCliente		
		if(loanSimulation.getNames().isEmpty()){ 
			mensajeError = "Se requiere el nombre para continuar";
		} 
		else { cliente.setNames(loanSimulation.getNames()); }
//OrigenAgendamiento		
		if( loanSimulation.getFinancialLine().isEmpty()){ 
			mensajeError = "Se requiere linea de credito para continuar"; 
		}
		else{ cliente.setOriginAppointmentScheduling("01"); }
//TelefonoFijo		
		cliente.setPhone(longitudRegistro(loanSimulation.getPhone(),10));
//ValorVivienda
		precio = new PriceOfHousing();
		precio.setAmount(loanSimulation.getPriceHousing().getAmount());
		precio.setCurrency(String.valueOf(loanSimulation.getYearsToFinanced()));
		cliente.setPriceOfHousing(precio);
//ScoreCifin		
		cliente.setReplyScoreBuro(0);
//AgendamientoHorario
		if( loanSimulation.getHorarioContacto().isEmpty()){ 
			mensajeError = "Se requiere un periodo de contacto"; 
		}
		else{ cliente.setSchedule(loanSimulation.getHorarioContacto().toUpperCase()); }		
//EstadoResidencia
		if(loanSimulation.getStateResidence().isEmpty() || loanSimulation.getStateResidence().equals("Seleccione")){ 
			mensajeError = "Se requiere un departamento para continuar"; 
		}
		else{
			deptoRes = new StateResidence();
			deptoRes.setId(longitudRegistro(loanSimulation.getStateResidence(),4));
//			deptoRes.setId(longitudRegistro("11",4));			
			deptoRes.setName(convetirFecha(loanSimulation.getFechaContacto()));
			cliente.setStateResidence(deptoRes);
		}
//AgendamientoFecha		
		if(loanSimulation.getFechaContacto().toString().isEmpty()){ 
			mensajeError = "Se requiere una fecha de contacto para continuar"; 
		}
		else{ cliente.setDateContact(convetirFecha(loanSimulation.getFechaContacto())); }
//AsuntoAgendamiento
		cliente.setSubject(loanSimulation.getFinancialLines().toString());
//SubProducto		
		cliente.setSubProduct(loanSimulation.getSubProduct());		
//ApellidoCliente
		if(loanSimulation.getSurnames().isEmpty()){ 
			mensajeError = "Se requiere el/los apellidos para continuar"; 
		}
		else{ cliente.setSurname(loanSimulation.getSurnames()); }
//Asignar ID temporal
		cliente.setId(0);
		if(!mensajeError.isEmpty()){
			FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Simulacion no Exitosa", mensajeError);
			FacesContext.getCurrentInstance().addMessage(null, message);
		}
	}
	
	public String convetirFecha(Date fecha){
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        String fechaRetorno = format.format(fecha);
        return fechaRetorno;
	}
	public String longitudRegistro(String registro, Integer longitud){
		String retorno = "",tmp = "";
		for(int i=0;i<longitud;i++){
			retorno = retorno + "0";
		}
		tmp = registro;
		if(tmp.length()<=0){ tmp = "0"; }		
		if(tmp.length()>=longitud){ retorno = tmp; }
		else{ retorno = retorno.substring(tmp.length()) + tmp; }
		if(retorno.length()>longitud){ retorno = retorno.substring(0,longitud); }
		return retorno;
	}
	public String longitudNumero(String registro, Integer longitud){
		String retorno = "",tmp = "";
		for(int i=0;i<longitud;i++){
			retorno = retorno + "0";
		}
		tmp = registro;
		if(tmp.length()<=0){ tmp = "0"; }		
		if(tmp.length()>=longitud){ retorno = tmp; }
		else{ retorno = tmp + retorno.substring(tmp.length()); }
		if(retorno.length()>longitud){ retorno = retorno.substring(0,longitud); }
		return retorno;
	}
    public void submitCaptcha() {
        RequestContext context = RequestContextHolder.getRequestContext();
        AgendamientoBean agendamientoBean = (AgendamientoBean) context.getFlowScope().get("agendamientoBean");
        String captcha = agendamientoBean.getCaptchaVal().toString().toUpperCase();
        if(captcha.equals("TRUE")){
        	FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Captcha", "Validación exitosa");
        	FacesContext.getCurrentInstance().addMessage(null, msg);
        	mensajeError = "";
        }
	}
    public void onPaisChange() {
        RequestContext context = RequestContextHolder.getRequestContext();
        AgendamientoBean agendamientoBean = (AgendamientoBean) context.getFlowScope().get("agendamientoBean");
        String pais = agendamientoBean.getPaisResidencia();
        if(pais.isEmpty() || "".equals(pais)){
        	agendamientoBean.setDepartamentoResiencia("");
        	agendamientoBean.getOptionsDepartamento().clear();        	
        } else {        	
        	GeoLocalizacion geoLocalizacion = new GeoLocalizacion();
        	agendamientoBean.setOptionsDepartamento(geoLocalizacion.opcionesDepartamentos(pais)); 
		}
    }    
}