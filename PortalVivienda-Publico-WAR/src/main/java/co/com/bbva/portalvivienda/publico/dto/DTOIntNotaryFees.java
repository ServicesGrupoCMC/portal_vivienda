
package co.com.bbva.portalvivienda.publico.dto;

import java.io.Serializable;




public class DTOIntNotaryFees implements Serializable{

    public final static long serialVersionUID = 1L;
    private DTOIntRetefuenteValue retefuenteValue;
    private DTOIntNotarialCharges notarialCharges;
    private DTOIntValueCopies valueCopies;
    private DTOIntTotalNotaryExpenses totalNotaryExpenses;

    public DTOIntNotaryFees() {
        //default constructor
    }

    public DTOIntNotaryFees(DTOIntRetefuenteValue retefuenteValue, DTOIntNotarialCharges notarialCharges, DTOIntValueCopies valueCopies, DTOIntTotalNotaryExpenses totalNotaryExpenses) {
        this.retefuenteValue = retefuenteValue;
        this.notarialCharges = notarialCharges;
        this.valueCopies = valueCopies;
        this.totalNotaryExpenses = totalNotaryExpenses;
    }

    public DTOIntRetefuenteValue getRetefuenteValue() {
        return retefuenteValue;
    }

    public void setRetefuenteValue(DTOIntRetefuenteValue retefuenteValue) {
        this.retefuenteValue = retefuenteValue;
    }

    public DTOIntNotarialCharges getNotarialCharges() {
        return notarialCharges;
    }

    public void setNotarialCharges(DTOIntNotarialCharges notarialCharges) {
        this.notarialCharges = notarialCharges;
    }

    public DTOIntValueCopies getValueCopies() {
        return valueCopies;
    }

    public void setValueCopies(DTOIntValueCopies valueCopies) {
        this.valueCopies = valueCopies;
    }

    public DTOIntTotalNotaryExpenses getTotalNotaryExpenses() {
        return totalNotaryExpenses;
    }

    public void setTotalNotaryExpenses(DTOIntTotalNotaryExpenses totalNotaryExpenses) {
        this.totalNotaryExpenses = totalNotaryExpenses;
    }

}
