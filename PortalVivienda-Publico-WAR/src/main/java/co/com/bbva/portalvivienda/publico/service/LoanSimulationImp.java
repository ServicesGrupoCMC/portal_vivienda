/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.bbva.portalvivienda.publico.service;

import co.com.bbva.portalvivienda.back.LoanSimulatorClient;
import co.com.bbva.portalvivienda.publico.SimuladorPasoAPaso;
import co.com.bbva.portalvivienda.publico.dto.*;
import com.bbva.czic.loansimulator.facade.v00.dto.*;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.webflow.execution.RequestContext;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.List;

/**
 * @author Administrator
 */
@Service("loanSimulationService")
public class LoanSimulationImp implements LoanSimulationService, Serializable {

    private LoanSimulatorClient loanSimulatorClient;

    @Override
    public DTOIntLoanSimulation loanSimulate(DTOIntLoanSimulation dtoLoanSimulation) throws Exception {
        LoanSimulation loanSimulation = new LoanSimulation();
        loanSimulation.setFinancialLine(dtoLoanSimulation.getFinancialLine());
        loanSimulation.setFinancialSubline(dtoLoanSimulation.getFinancialSubline());
        loanSimulation.setPriceHousing(new PriceHousing(dtoLoanSimulation.getPriceHousing().getAmount(), "COP"));
        loanSimulation.setMontlyIncome(new MontlyIncome(dtoLoanSimulation.getMontlyIncome().getAmount(), "COP"));
        loanSimulation.setAmountToFinanced(new AmountToFinanced(dtoLoanSimulation.getAmountToFinanced().getAmount(), "COP"));
        loanSimulation.setYearsToFinanced((double) dtoLoanSimulation.getYearsToFinanced());

        loanSimulation.setQuotaValue(new QuotaValue(dtoLoanSimulation.getQuotaValue().getAmount(), "COP"));
        loanSimulation.setYearIntegerAmortization(Double.parseDouble("1"));

        // numero de identificacion!
        loanSimulation.setDocumentType(new DocumentType(dtoLoanSimulation.getDocumentType().getId(), null));
        loanSimulation.setDocumentInteger(dtoLoanSimulation.getDocumentInteger());

        DataLoanSimulation response = loanSimulatorClient.simulateMortgageLoan(loanSimulation, "01");
        dtoLoanSimulation.setQuotaValue(new DTOIntQuotaValue(response.getData().getQuotaValue().getAmount(), "COP"));
        dtoLoanSimulation.setPercentageFixedQuota(Double.parseDouble(response.getData().getPercentageFixedQuota()));

        return dtoLoanSimulation;
    }

    @Override
    public DTOIntLoanSimulation completeLoanSimulation(DTOIntLoanSimulation dtoLoanSimulation) throws Exception {
        LoanSimulation loanSimulation = new LoanSimulation();

        // Datos Iniciales
        loanSimulation.setFinancialLine(dtoLoanSimulation.getFinancialLine());
        loanSimulation.setFinancialSubline(dtoLoanSimulation.getFinancialSubline());
        loanSimulation.setPriceHousing(new PriceHousing(dtoLoanSimulation.getPriceHousing().getAmount(), "COP"));
        loanSimulation.setMontlyIncome(new MontlyIncome(dtoLoanSimulation.getMontlyIncome().getAmount(), "COP"));
        loanSimulation.setAmountToFinanced(new AmountToFinanced(dtoLoanSimulation.getAmountToFinanced().getAmount(), "COP"));
        loanSimulation.setYearsToFinanced((double) dtoLoanSimulation.getYearsToFinanced());

        //Detalle de Credito
        loanSimulation.setAmortizationType(dtoLoanSimulation.getAmortizationType());
        loanSimulation.setIsGovernmentCoverage(dtoLoanSimulation.isGovernmentCoverage());
        loanSimulation.setIsLinkedMilitaryForces(dtoLoanSimulation.isLinkedMilitaryForces());
        loanSimulation.setIsBbvaPayroll(dtoLoanSimulation.isBbvaPayroll());

        loanSimulation.setIsNewHousing(dtoLoanSimulation.isNewHousing());
        if (dtoLoanSimulation.isNewHousing()) {
            loanSimulation.setIsHousingInProjectBbva(dtoLoanSimulation.isHousingInProjectBbva());
        } else {
            loanSimulation.setIsHousingInProjectBbva(false);
        }

        if (dtoLoanSimulation.getFinancialLine().equals("02")) {
            loanSimulation.setIsLiveOnProperty(false);
            loanSimulation.setBuyPercentage("0");
        } else {
            loanSimulation.setIsLiveOnProperty(dtoLoanSimulation.isLiveOnProperty());
            loanSimulation.setBuyPercentage(dtoLoanSimulation.getBuyPercentage());
        }

        // Datos del Solicitante
        loanSimulation.setDocumentType(new DocumentType(dtoLoanSimulation.getDocumentType().getId(), null));
        loanSimulation.setDocumentInteger(dtoLoanSimulation.getDocumentInteger());
        loanSimulation.setNames(dtoLoanSimulation.getNames());
        loanSimulation.setSurnames(dtoLoanSimulation.getSurnames());
        loanSimulation.setBirthDate(dtoLoanSimulation.getFechaNacimiento());
        loanSimulation.setEmail(dtoLoanSimulation.getEmail());
        loanSimulation.setPhone(dtoLoanSimulation.getPhone());
        loanSimulation.setCellPhone(dtoLoanSimulation.getCellPhone());

        //Esto debe Cmbiarse - No se envia la Direccion.
        loanSimulation.setCountryResidence(new CountryResidence("1", "Colombia"));
        loanSimulation.setStateResidence("Bogota D.C");
        loanSimulation.setCityResidence("Bogota D.C");

        loanSimulation.setYearIntegerAmortization(dtoLoanSimulation.getYearIntegerAmortization() == 0 ? 1 : dtoLoanSimulation.getYearIntegerAmortization());
        loanSimulation.setIsLivesInTheCountry(dtoLoanSimulation.isLivesInTheCountry());

        DataLoanSimulation response = loanSimulatorClient.simulateMortgageLoan(loanSimulation, "02");


        dtoLoanSimulation.setQuotaValue(new DTOIntQuotaValue(response.getData().getQuotaValue().getAmount(), response.getData().getQuotaValue().getCurrency()));
        dtoLoanSimulation.setPercentageFixedQuota(Double.parseDouble(response.getData().getPercentageFixedQuota()));
        dtoLoanSimulation.setSubProduct(response.getData().getSubProduct().intValue());
        dtoLoanSimulation.setRateCoverage(Double.parseDouble(response.getData().getRateCoverage()));

        dtoLoanSimulation.setValueCapitalAmount(new DTOIntValueCapitalAmount(response.getData().getValueCapitalAmount().getAmount(), response.getData().getValueCapitalAmount().getCurrency()));
        //  dtoLoanSimulation.setLoanTermYears(response.getData().getLoanTermYears());
        dtoLoanSimulation.setValueInterestBonus(new DTOIntValueInterestBonus(response.getData().getValueInterestBonus().getAmount(), response.getData().getValueInterestBonus().getCurrency()));
        dtoLoanSimulation.setLifeInsuranceValue(new DTOIntLifeInsuranceValue(response.getData().getLifeInsuranceValue().getAmount(), response.getData().getLifeInsuranceValue().getCurrency()));
        dtoLoanSimulation.setValueFireEarthquake(new DTOIntValueFireEarthquake(response.getData().getValueFireEarthquake().getAmount(), response.getData().getValueFireEarthquake().getCurrency()));
        dtoLoanSimulation.setValueCoverage(new DTOIntValueCoverage(response.getData().getValueCoverage().getAmount(), response.getData().getValueCoverage().getCurrency()));

        double totalCuotaMensual = dtoLoanSimulation.getLifeInsuranceValue().getAmount() + dtoLoanSimulation.getValueFireEarthquake().getAmount() + dtoLoanSimulation.getValueCoverage().getAmount() + dtoLoanSimulation.getValueCapitalAmount().getAmount() + dtoLoanSimulation.getValueInterestBonus().getAmount();
        dtoLoanSimulation.setTotalCuotaMensual(totalCuotaMensual);

        DTOIntValueAppraise valueAppraise = new DTOIntValueAppraise(Double.parseDouble(response.getData().getAdditionalCosts().getBuyer().getOtherExpenses().getValueAppraise().getAmount()), response.getData().getAdditionalCosts().getBuyer().getOtherExpenses().getValueAppraise().getCurrency());
        DTOIntValueStudyTitle valueStudyTitle = new DTOIntValueStudyTitle(Double.parseDouble(response.getData().getAdditionalCosts().getBuyer().getOtherExpenses().getValueStudyTitle().getAmount()), response.getData().getAdditionalCosts().getBuyer().getOtherExpenses().getValueAppraise().getCurrency());
        DTOIntTotalOtherExpenses totalOtherExpenses = new DTOIntTotalOtherExpenses(Double.parseDouble(response.getData().getAdditionalCosts().getBuyer().getOtherExpenses().getTotalOtherExpenses().getAmount()), response.getData().getAdditionalCosts().getBuyer().getOtherExpenses().getTotalOtherExpenses().getCurrency());
        DTOIntOtherExpenses otherExpenses = new DTOIntOtherExpenses(valueAppraise, valueStudyTitle, totalOtherExpenses);

        DTOIntRetefuenteValue retefuenteValue = new DTOIntRetefuenteValue(Double.parseDouble(response.getData().getAdditionalCosts().getBuyer().getNotaryFees().getRetefuenteValue().getAmount()), response.getData().getAdditionalCosts().getBuyer().getNotaryFees().getRetefuenteValue().getCurrency());
        DTOIntNotarialCharges notarialCharges = new DTOIntNotarialCharges(Double.parseDouble(response.getData().getAdditionalCosts().getBuyer().getNotaryFees().getNotarialCharges().getAmount()), response.getData().getAdditionalCosts().getBuyer().getNotaryFees().getNotarialCharges().getCurrency());
        DTOIntValueCopies valueCopies = new DTOIntValueCopies(Double.parseDouble(response.getData().getAdditionalCosts().getBuyer().getNotaryFees().getValueCopies().getAmount()), response.getData().getAdditionalCosts().getBuyer().getNotaryFees().getValueCopies().getCurrency());
        DTOIntTotalNotaryExpenses totalNotaryExpenses = new DTOIntTotalNotaryExpenses(Double.parseDouble(response.getData().getAdditionalCosts().getBuyer().getNotaryFees().getTotalNotaryExpenses().getAmount()), response.getData().getAdditionalCosts().getBuyer().getNotaryFees().getTotalNotaryExpenses().getCurrency());
        DTOIntNotaryFees notaryFees = new DTOIntNotaryFees(retefuenteValue, notarialCharges, valueCopies, totalNotaryExpenses);

        DTOIntRegistrationRight registrationRight = new DTOIntRegistrationRight(Double.parseDouble(response.getData().getAdditionalCosts().getBuyer().getRegistrationFees().getRegistrationRight().getAmount()), response.getData().getAdditionalCosts().getBuyer().getRegistrationFees().getRegistrationRight().getCurrency());
        DTOIntCertificateOfFreedom certificateOfFreedom = new DTOIntCertificateOfFreedom(Double.parseDouble(response.getData().getAdditionalCosts().getBuyer().getRegistrationFees().getCertificateOfFreedom().getAmount()), response.getData().getAdditionalCosts().getBuyer().getRegistrationFees().getCertificateOfFreedom().getCurrency());
        DTOIntBenefitTax benefitTax = new DTOIntBenefitTax(Double.parseDouble(response.getData().getAdditionalCosts().getBuyer().getRegistrationFees().getBenefitTax().getAmount()), response.getData().getAdditionalCosts().getBuyer().getRegistrationFees().getBenefitTax().getCurrency());
        DTOIntTotalExpensesRegistration totalExpensesRegistration = new DTOIntTotalExpensesRegistration(Double.parseDouble(response.getData().getAdditionalCosts().getBuyer().getRegistrationFees().getTotalExpensesRegistration().getAmount()), response.getData().getAdditionalCosts().getBuyer().getRegistrationFees().getTotalExpensesRegistration().getCurrency());
        DTOIntRegistrationFees registrationFees = new DTOIntRegistrationFees(registrationRight, certificateOfFreedom, benefitTax, totalExpensesRegistration);

        DTOIntTotalSpends totalSpends = new DTOIntTotalSpends(Double.parseDouble(response.getData().getAdditionalCosts().getBuyer().getTotalSpends().getAmount()), response.getData().getAdditionalCosts().getBuyer().getTotalSpends().getCurrency());
        DTOIntBuyer buyer = new DTOIntBuyer(otherExpenses, notaryFees, registrationFees, totalSpends);

        DTOIntValueAppraise valueAppraiseSeller = new DTOIntValueAppraise(Double.parseDouble(response.getData().getAdditionalCosts().getSeller().getOtherExpenses().getValueAppraise().getAmount()), response.getData().getAdditionalCosts().getSeller().getOtherExpenses().getValueAppraise().getCurrency());
        DTOIntValueStudyTitle valueStudyTitleSeller = new DTOIntValueStudyTitle(Double.parseDouble(response.getData().getAdditionalCosts().getSeller().getOtherExpenses().getValueStudyTitle().getAmount()), response.getData().getAdditionalCosts().getSeller().getOtherExpenses().getValueStudyTitle().getCurrency());
        DTOIntTotalOtherExpenses totalOtherExpensesSeller = new DTOIntTotalOtherExpenses(Double.parseDouble(response.getData().getAdditionalCosts().getSeller().getOtherExpenses().getTotalOtherExpenses().getAmount()), response.getData().getAdditionalCosts().getSeller().getOtherExpenses().getTotalOtherExpenses().getCurrency());
        DTOIntOtherExpenses otherExpensesSeller = new DTOIntOtherExpenses(valueAppraiseSeller, valueStudyTitleSeller, totalOtherExpensesSeller);

        DTOIntRetefuenteValue retefuenteValueSeller = new DTOIntRetefuenteValue(Double.parseDouble(response.getData().getAdditionalCosts().getSeller().getNotaryFees().getRetefuenteValue().getAmount()), response.getData().getAdditionalCosts().getSeller().getNotaryFees().getRetefuenteValue().getCurrency());
        DTOIntNotarialCharges notarialChargesSeller = new DTOIntNotarialCharges(Double.parseDouble(response.getData().getAdditionalCosts().getSeller().getNotaryFees().getNotarialCharges().getAmount()), response.getData().getAdditionalCosts().getSeller().getNotaryFees().getNotarialCharges().getCurrency());
        DTOIntValueCopies valueCopiesSeller = new DTOIntValueCopies(Double.parseDouble(response.getData().getAdditionalCosts().getSeller().getNotaryFees().getValueCopies().getAmount()), response.getData().getAdditionalCosts().getSeller().getNotaryFees().getValueCopies().getCurrency());
        DTOIntTotalNotaryExpenses totalNotaryExpensesSeller = new DTOIntTotalNotaryExpenses(Double.parseDouble(response.getData().getAdditionalCosts().getSeller().getNotaryFees().getTotalNotaryExpenses().getAmount()), response.getData().getAdditionalCosts().getSeller().getNotaryFees().getTotalNotaryExpenses().getCurrency());
        DTOIntNotaryFees notaryFeesSeller = new DTOIntNotaryFees(retefuenteValueSeller, notarialChargesSeller, valueCopiesSeller, totalNotaryExpensesSeller);

        DTOIntRegistrationRight registrationRightSeller = new DTOIntRegistrationRight(Double.parseDouble(response.getData().getAdditionalCosts().getSeller().getRegistrationFees().getRegistrationRight().getAmount()), response.getData().getAdditionalCosts().getSeller().getRegistrationFees().getRegistrationRight().getCurrency());
        DTOIntCertificateOfFreedom certificateOfFreedomSeller = new DTOIntCertificateOfFreedom(Double.parseDouble(response.getData().getAdditionalCosts().getSeller().getRegistrationFees().getCertificateOfFreedom().getAmount()), response.getData().getAdditionalCosts().getSeller().getRegistrationFees().getCertificateOfFreedom().getCurrency());
        DTOIntBenefitTax benefitTaxSeller = new DTOIntBenefitTax(Double.parseDouble(response.getData().getAdditionalCosts().getSeller().getRegistrationFees().getBenefitTax().getAmount()), response.getData().getAdditionalCosts().getSeller().getRegistrationFees().getBenefitTax().getCurrency());
        DTOIntTotalExpensesRegistration totalExpensesRegistrationSeller = new DTOIntTotalExpensesRegistration(Double.parseDouble(response.getData().getAdditionalCosts().getSeller().getRegistrationFees().getTotalExpensesRegistration().getAmount()), response.getData().getAdditionalCosts().getSeller().getRegistrationFees().getTotalExpensesRegistration().getCurrency());
        DTOIntRegistrationFees registrationFeesSeller = new DTOIntRegistrationFees(registrationRightSeller, certificateOfFreedomSeller, benefitTaxSeller, totalExpensesRegistrationSeller);

        DTOIntTotalSpends totalSpendsSeller = new DTOIntTotalSpends(Double.parseDouble(response.getData().getAdditionalCosts().getSeller().getTotalSpends().getAmount()), response.getData().getAdditionalCosts().getSeller().getTotalSpends().getCurrency());

        DTOIntSeller seller = new DTOIntSeller(otherExpensesSeller, notaryFeesSeller, registrationFeesSeller, totalSpendsSeller);

        DTOIntAdditionalCosts additionalCosts = new DTOIntAdditionalCosts(buyer, seller);
        dtoLoanSimulation.setAdditionalCosts(additionalCosts);

        DTOIntVtuPaymentCapital vtuPaymentCapital = new DTOIntVtuPaymentCapital(response.getData().getAmortizationTable().getVtuPaymentCapital().getAmount(), response.getData().getAmortizationTable().getVtuPaymentCapital().getCurrency());
        DTOIntVtuPaymentInterest vtuPaymentInterest = new DTOIntVtuPaymentInterest(response.getData().getAmortizationTable().getVtuPaymentInterest().getAmount(), response.getData().getAmortizationTable().getVtuPaymentInterest().getCurrency());
        DTOIntVtuPaymentInsurance vtuPaymentInsurance = new DTOIntVtuPaymentInsurance(response.getData().getAmortizationTable().getVtuPaymentInsurance().getAmount(), response.getData().getAmortizationTable().getVtuPaymentInsurance().getCurrency());
        DTOIntVtuPercentage vtuPercentage = new DTOIntVtuPercentage(response.getData().getAmortizationTable().getVtuPercentage().getAmount(), response.getData().getAmortizationTable().getVtuPercentage().getCurrency());

        Gson gson = new Gson();
        String monthlyAmortizationJson = gson.toJson(response.getData().getAmortizationTable().getMonthlyAmortization());
        Type TipoDTOIntMonthlyAmortizations = new TypeToken<List<DTOIntMonthlyAmortization>>() {
        }.getType();
        List<DTOIntMonthlyAmortization> monthlyAmortization = gson.fromJson(monthlyAmortizationJson, TipoDTOIntMonthlyAmortizations);

        DTOIntAmortizationTable amortizationTable = new DTOIntAmortizationTable(vtuPaymentCapital, vtuPaymentInterest, vtuPaymentInsurance, vtuPercentage, monthlyAmortization);
        dtoLoanSimulation.setAmortizationTable(amortizationTable);

        return dtoLoanSimulation;
    }


    @Override
    public SimuladorPasoAPaso simulate(SimuladorPasoAPaso simulation, RequestContext context) throws Exception {
        DTOIntLoanSimulation loanSimulation = new DTOIntLoanSimulation();


        LoanSimulation loan = new LoanSimulation();


        setCorrectProductLine(simulation, loanSimulation);
        loanSimulation.setSimulationTypeIndicator("01");
        loanSimulation.setMontlyIncome(new DTOIntMontlyIncome((double) simulation.formatStrig(simulation.getStringIngMensuales()), "COP"));
        loanSimulation.setPriceHousing(new DTOIntPriceHousing((double) simulation.formatStrig(simulation.getStringValorVivienda()), "COP"));
        loanSimulation.setAmountToFinanced(new DTOIntAmountToFinanced((double) simulation.formatStrig(simulation.getStringValorCredito()), "COP"));
        loan.setSimulationTypeIndicator(loanSimulation.getSimulationTypeIndicator());
        loan.setDocumentType(new DocumentType("1", "COP"));
        loan.setDocumentInteger("000001069473371");
        loan.setFinancialLine(loanSimulation.getFinancialLine());
        loan.setFinancialSubline(loanSimulation.getFinancialSubline());
        loan.setMontlyIncome(new MontlyIncome(loanSimulation.getMontlyIncome().getAmount(), "COP"));
        loan.setPriceHousing(new PriceHousing(loanSimulation.getPriceHousing().getAmount(), "COP"));
        loan.setAmountToFinanced(new AmountToFinanced(loanSimulation.getAmountToFinanced().getAmount(), "COP"));
        loan.setYearsToFinanced((double)loanSimulation.getYearsToFinanced());

        DataLoanSimulation respuesta = loanSimulatorClient.simulateMortgageLoan(loan,"01");


        simulation.setValorCuota(respuesta.getData().getQuotaValue().getAmount().intValue());

        simulation.setStringValorCuota(simulation.formatStrig(respuesta.getData().getQuotaValue().getAmount().intValue()));
        return simulation;
    }

    @Override
    public SimuladorPasoAPaso simulate2(SimuladorPasoAPaso simulation, RequestContext context) throws Exception {
        DTOIntLoanSimulation loanSimulation = new DTOIntLoanSimulation();
        LoanSimulation loan = new LoanSimulation();
        setCorrectProductLine(simulation, loanSimulation);
        loanSimulation.setSimulationTypeIndicator("02");
        loanSimulation.setMontlyIncome(new DTOIntMontlyIncome((double) simulation.formatStrig(simulation.getStringIngMensuales()), "1"));
        loanSimulation.setPriceHousing(new DTOIntPriceHousing((double) simulation.formatStrig(simulation.getStringValorVivienda()), "1"));
        loanSimulation.setAmountToFinanced(new DTOIntAmountToFinanced((double) simulation.formatStrig(simulation.getStringValorCredito()), "1"));
        loan.setAmortizationType(simulation.getMonedaCredito());


        loan.setIsGovernmentCoverage(simulation.getGobierno().contentEquals("Si"));
        loan.setIsLinkedMilitaryForces(simulation.getMilitares().contentEquals("Si"));
        loan.setIsNewHousing(simulation.getNueva().contentEquals("Si"));
        loan.setIsBbvaPayroll(simulation.getNominaBbva().contentEquals("Si"));
        loan.setIsHousingInProjectBbva(simulation.getProyectoFinanciado().contentEquals("Si"));
        loan.setIsLiveOnProperty(simulation.getVivencia().contentEquals("Si"));
        loan.setBuyPercentage(simulation.getPorcentajeCompra() + "");
        loan.setDocumentType(new DocumentType("1", simulation.getTipoIdentificacion()));
        loan.setDocumentInteger(simulation.getIdentificacion());
        loan.setNames(simulation.getNombres());
        loan.setSurnames(simulation.getApellidos());
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        loan.setBirthDate(formatter.format(simulation.getNacimiento()));
        loan.setEmail(simulation.getCorreo());
        loan.setPhone(simulation.getFijo().toString());
        loan.setCellPhone(simulation.getCelular());

        loan.setIsLivesInTheCountry(simulation.getResidesColombia().contains("Si"));
        loan.setStateResidence(simulation.getDepartamentoResidencia());
        loan.setCityResidence(simulation.getCiudadOficina());

        loan.setSimulationTypeIndicator(loanSimulation.getSimulationTypeIndicator());
        loan.setDocumentType(new DocumentType(loanSimulation.getDocumentType().getId(), null));
        loan.setDocumentInteger(loanSimulation.getDocumentInteger());
        loan.setFinancialLine(loanSimulation.getFinancialLine());
        loan.setFinancialSubline(loanSimulation.getFinancialSubline());
        loan.setMontlyIncome(new MontlyIncome(loanSimulation.getMontlyIncome().getAmount(), "1"));
        loan.setPriceHousing(new PriceHousing(loanSimulation.getPriceHousing().getAmount(), "1"));
        loan.setAmountToFinanced(new AmountToFinanced(loanSimulation.getAmountToFinanced().getAmount(), "1"));
        loan.setYearsToFinanced((double)loanSimulation.getYearsToFinanced());

        DataLoanSimulation respuesta = loanSimulatorClient.simulateMortgageLoan(loan,"02");

        simulation.setValorCuota(respuesta.getData().getQuotaValue().getAmount().intValue());

        simulation.setStringValorCuota(simulation.formatStrig(respuesta.getData().getQuotaValue().getAmount().intValue()));
        return simulation;
    }

    @Autowired
    public void setLoanSimulatorClient(LoanSimulatorClient loanSimulatorClient) {
        this.loanSimulatorClient = loanSimulatorClient;
    }


    public void setCorrectProductLine(SimuladorPasoAPaso simulation, DTOIntLoanSimulation loanSimulation) {
        if (simulation.getQuiero() != null && simulation.getCon() != null) {
            if (simulation.getQuiero().contains("vivienda") && simulation.getCon().contains("hipoteario")) {
                loanSimulation.setFinancialSubline("01");
                loanSimulation.setFinancialLine("01");
            } else if (simulation.getQuiero().contains("cartera") && simulation.getCon().contains("hipoteario")) {
                loanSimulation.setFinancialSubline("02");
                loanSimulation.setFinancialLine("01");
            } else if (simulation.getQuiero().contains("remodelacion") && simulation.getCon().contains("hipoteario")) {
                loanSimulation.setFinancialSubline("03");
                loanSimulation.setFinancialLine("01");
            } else if (simulation.getQuiero().contains("construcción vivienda") && simulation.getCon().contains("hipoteario")) {
                loanSimulation.setFinancialSubline("04");
                loanSimulation.setFinancialLine("01");
            } else if (simulation.getQuiero().contains("vivienda") && simulation.getCon().contains("habitacional")) {
                loanSimulation.setFinancialSubline("01");
                loanSimulation.setFinancialLine("02");
            } else if (simulation.getQuiero().contains("cesion") && simulation.getCon().contains("habitacional")) {
                loanSimulation.setFinancialSubline("02");
                loanSimulation.setFinancialLine("02");
            } else if (simulation.getQuiero().contains("cartera") && simulation.getCon().contains("habitacional")) {
                loanSimulation.setFinancialSubline("03");
                loanSimulation.setFinancialLine("02");
            }
        }
    }
}
