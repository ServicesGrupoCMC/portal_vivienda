
package co.com.bbva.portalvivienda.publico.dto;

import java.io.Serializable;




public class DTOIntVtuPaymentCapital implements Serializable{

    public final static long serialVersionUID = 1L;
    private Double amount;
    private String currency;

    public DTOIntVtuPaymentCapital() {
        //default constructor
    }
    
    public DTOIntVtuPaymentCapital(Double amount, String currency) {
        this.amount = amount;
        this.currency = currency;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

}
