package co.com.bbva.portalvivienda.publico.controller;

import co.com.bbva.portalvivienda.back.CifinService;
import co.com.bbva.portalvivienda.comun.FormatoFechas;
import co.com.bbva.portalvivienda.publico.dto.DTOIntLoanSimulation;
import co.com.bbva.portalvivienda.publico.dto.confronta.cuestionario.DTOCuestionario;
import co.com.bbva.portalvivienda.publico.dto.confronta.cuestionario.DTOPregunta;
import co.com.bbva.portalvivienda.publico.dto.confronta.cuestionario.DTORespuesta;
import com.bbva.czic.cifin.facade.v01.dto.AnswerQuestion;
import com.bbva.czic.cifin.facade.v01.dto.DataConfronta;
import com.bbva.czic.cifin.facade.v01.dto.Question;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.Serializable;

/**
 * Created by jquijano82 on 10/07/17.
 */
public class CuestionarioConfrontaController implements Serializable {

    private final static Logger LOG = Logger.getLogger(CuestionarioConfrontaController.class);

    @Autowired
    private transient CifinService cifinService;

    public CuestionarioConfrontaController() {
    }

    public DTOCuestionario generarCuestionario(DTOIntLoanSimulation datos) {

        try {
            DataConfronta resp = cifinService.getConfrontaQuestionnaire(0, datos.getDocumentType().getId(),
                    datos.getDocumentInteger().toString(), FormatoFechas.formatDate(FormatoFechas.ISO_SHORT, datos.getBirthDate()),
                    datos.getSurnames(), 11, 1, datos.getPhone(), "0");

            DTOCuestionario cuestionario = new DTOCuestionario();
            for (Question q : resp.getData().getDetailQuestionnaire().getQuestionList()) {
                DTOPregunta pregunta = new DTOPregunta();
                pregunta.setTexto(q.getQuestionText());
                for (AnswerQuestion aq : q.getAnswerList()) {
                    DTORespuesta respuesta = new DTORespuesta();
                    respuesta.setTexto(aq.getAnswerText());
                    respuesta.setIdPregunta(aq.getQuestionSequence());
                    respuesta.setIdRespuesta(aq.getAnswerSequence());
                    pregunta.getRespuestas().add(respuesta);
                }
                cuestionario.getPreguntas().add(pregunta);
            }
            cuestionario.setIndice(1);
            return cuestionario;
        } catch (Exception err) {
            LOG.error(err.getMessage(), err);
        }
        return null;
    }

    public void continuar(DTOCuestionario datos){
        if(datos.getIndice() < 5){
            datos.setIndice(datos.getIndice()+1);
        }
    }

    public void validarRespuestas(DTOCuestionario datos){
        datos.setFinalizado(true);
    }
}
