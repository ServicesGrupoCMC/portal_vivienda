package co.com.bbva.portalvivienda.publico.dto.confronta.terminos;

import java.io.Serializable;

/**
 * Created by jquijano82 on 10/07/17.
 */
public class DTOTerminosConfronta implements Serializable {

    boolean aceptoTerminos;
    boolean aceptoConsultaCentrales;

    public boolean isAceptoTerminos() {
        return aceptoTerminos;
    }

    public void setAceptoTerminos(boolean aceptoTerminos) {
        this.aceptoTerminos = aceptoTerminos;
    }

    public boolean isAceptoConsultaCentrales() {
        return aceptoConsultaCentrales;
    }

    public void setAceptoConsultaCentrales(boolean aceptoConsultaCentrales) {
        this.aceptoConsultaCentrales = aceptoConsultaCentrales;
    }
}
