package co.com.bbva.portalvivienda.publico;

import java.io.Serializable;
import java.util.Date;
import java.util.LinkedHashMap;
import co.com.bbva.portalvivienda.publico.utilidades.GeoLocalizacion;

public class AgendamientoBean implements Serializable {
	/**
	 * Created by jquijano82 on 10/07/17.
	 */
	private String nombre;
	private String apellido;
	private String tipoIdentificacion;
	private String identificacion;
	private String correo;
	private String telefono;
	private String celular;
	private String paisNacimiento;
	private String paisResidencia;
	private String departamentoResiencia;
	private String ciudadResidencia;
	private Date fechaContacto;
	private String horario;
	private String asunto;
	private LinkedHashMap<String,Object> optionsPais;
	private LinkedHashMap<String,Object> optionsDepartamento;
	private LinkedHashMap<String,Object> optionsCiudades;
	private Boolean terminos;
	private String mensajeError;
	private Boolean captchaVal;
	
	public AgendamientoBean(){
		GeoLocalizacion geoLocalizacion = new GeoLocalizacion();
		this.optionsPais = geoLocalizacion.OpcionesPais();
		this.optionsDepartamento = geoLocalizacion.opcionesDepartamentos("COL");
	}

	public LinkedHashMap<String,Object> ObtenerCiudades(String pais, String depto){
		GeoLocalizacion geoLocalizacion = new GeoLocalizacion();
		this.setOptionsCiudades(geoLocalizacion.opcionesCiudades(pais, depto));
		return this.getOptionsCiudades();
	}		
	
	public LinkedHashMap<String, Object> getOptionsPais() {
		return optionsPais;
	}

	public void setOptionsPais(LinkedHashMap<String, Object> optionsPais) {
		this.optionsPais = optionsPais;
	}

	public LinkedHashMap<String, Object> getOptionsDepartamento() {
		return optionsDepartamento;
	}

	public void setOptionsDepartamento(LinkedHashMap<String, Object> optionsDepartamento) {
		this.optionsDepartamento = optionsDepartamento;
	}

	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellido() {
		return apellido;
	}
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	public String getTipoIdentificacion() {
		return tipoIdentificacion;
	}
	public void setTipoIdentificacion(String tipoIdentificacion) {
		this.tipoIdentificacion = tipoIdentificacion;
	}
	public String getIdentificacion() {
		return identificacion;
	}
	public void setIdentificacion(String identificacion) {
		this.identificacion = identificacion;
	}
	public String getCorreo() {
		return correo;
	}
	public void setCorreo(String correo) {
		this.correo = correo;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public String getCelular() {
		return celular;
	}
	public void setCelular(String celular) {
		this.celular = celular;
	}
	public String getPaisNacimiento() {
		return paisNacimiento;
	}
	public void setPaisNacimiento(String paisNacimiento) {
		this.paisNacimiento = paisNacimiento;
	}
	public String getPaisResidencia() {
		return paisResidencia;
	}
	public void setPaisResidencia(String paisResidencia) {
		this.paisResidencia = paisResidencia;
	}
	public String getDepartamentoResiencia() {
		return departamentoResiencia;
	}
	public void setDepartamentoResiencia(String departamentoResiencia) {
		this.departamentoResiencia = departamentoResiencia;
	}
	public String getCiudadResidencia() {
		return ciudadResidencia;
	}
	public void setCiudadResidencia(String ciudadResidencia) {
		this.ciudadResidencia = ciudadResidencia;
	}
	public Date getFechaContacto() {
		return fechaContacto;
	}
	public void setFechaContacto(Date fechaContacto) {
		this.fechaContacto = fechaContacto;
	}
	public String getHorario() {
		return horario;
	}
	public void setHorario(String horario) {
		this.horario = horario;
	}
	public String getAsunto() {
		return asunto;
	}
	public void setAsunto(String asunto) {
		this.asunto = asunto;
	}

	public Boolean getTerminos() {
		return terminos;
	}

	public void setTerminos(Boolean terminos) {
		this.terminos = terminos;
	}
	public LinkedHashMap<String,Object> getOptionsCiudades() {
		return optionsCiudades;
	}
	public void setOptionsCiudades(LinkedHashMap<String,Object> optionsCiudades) {
		this.optionsCiudades = optionsCiudades;
	}
	public String getMensajeError() {
		return mensajeError;
	}
	public void setMensajeError(String mensajeError) {
		this.mensajeError = mensajeError;
	}
	public Boolean getCaptchaVal() {
		return captchaVal;
	}
	public void setCaptchaVal(Boolean captchaVal) {
		this.captchaVal = captchaVal;
	}
}
