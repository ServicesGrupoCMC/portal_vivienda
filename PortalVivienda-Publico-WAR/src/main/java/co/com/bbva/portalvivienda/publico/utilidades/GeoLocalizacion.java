package co.com.bbva.portalvivienda.publico.utilidades;

import java.io.IOException;
import java.util.LinkedHashMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class GeoLocalizacion {
	
	public LinkedHashMap<String,Object> OpcionesPais(){
		LinkedHashMap<String,Object> optionsPais = new LinkedHashMap<String,Object>();
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = null;
		try {
			dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(getClass().getResourceAsStream("/xml/paisesMundo.xml"));
			NodeList nList = doc.getElementsByTagName("pais");
			for (int temp = 0; temp < nList.getLength(); temp++) {
				Node nNode = nList.item(temp);
				
				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
					Element eElement = (Element) nNode;
					optionsPais.put(
							eElement.getElementsByTagName("nombre").item(0).getTextContent(),
							eElement.getElementsByTagName("id").item(0).getTextContent()
					);
					
				}
			}
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return optionsPais;
	}
	
	public LinkedHashMap<String,Object> opcionesDepartamento(){
		LinkedHashMap<String,Object> optionsDepartamento = new LinkedHashMap<String,Object>();
		//File xml = new File("./src/main/webapp/WEB-INF/xml/dep_colombia.xml");
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = null;
		try {
			dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(getClass().getResourceAsStream("/xml/dep_colombia.xml"));
			NodeList nList = doc.getElementsByTagName("departamento");
			
			for (int temp = 0; temp < nList.getLength(); temp++) {
				Node nNode = nList.item(temp);
				
				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
					Element eElement = (Element) nNode;
					optionsDepartamento.put(
							eElement.getElementsByTagName("nombre").item(0).getTextContent(),
							eElement.getElementsByTagName("nombre").item(0).getTextContent()
					);
					
				}
			}
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return optionsDepartamento;
	}

	public LinkedHashMap<String,Object> opcionesCiudades(String pais, String depto){
		LinkedHashMap<String,Object> optionsCiudades = new LinkedHashMap<String,Object>();
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = null;
		try {
			dBuilder = dbFactory.newDocumentBuilder();
//			Document doc = dBuilder.parse(getClass().getResourceAsStream("/xml/mun_colombia.xml"));
			Document doc = dBuilder.parse(getClass().getResourceAsStream("/xml/ciudadesMundo.xml"));			
			XPathFactory xpf = XPathFactory.newInstance();
			XPath xpath = xpf.newXPath();
			optionsCiudades = getCiudades(doc, xpath, pais, depto);
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return optionsCiudades;
	}
	
	public LinkedHashMap<String,Object> getCiudades(Document doc, XPath xpath, String pais, String depto){
		LinkedHashMap<String,Object> ciudades = new LinkedHashMap<String,Object>();
//		System.out.print("Depto "+depto+", Pais"+pais);
		String deptoTmp = String.valueOf(Integer.parseInt(depto));
		try{
			XPathExpression exp = xpath.compile("/ciudades/pais[@nombre='" + pais + "']/departamento[@nombre='" + deptoTmp + "']/ciudad/id/text()");
			XPathExpression expNombre = xpath.compile("/ciudades/pais[@nombre='" + pais + "']/departamento[@nombre='" + deptoTmp + "']/ciudad/nombre/text()");
			NodeList nodes = (NodeList) exp.evaluate(doc, XPathConstants.NODESET);
			NodeList nodesNombre = (NodeList) expNombre.evaluate(doc, XPathConstants.NODESET);
            for (int i = 0; i < nodes.getLength(); i++){
//            	System.out.print("Nombre "+nodesNombre.item(i).getNodeValue()+", ID "+nodes.item(i).getNodeValue());
            	ciudades.put(nodesNombre.item(i).getNodeValue(), nodes.item(i).getNodeValue());
            }
		} catch (XPathExpressionException e) {
			e.printStackTrace();
		}
		return ciudades;
	}
	
/*	public LinkedHashMap<String,Object> getCiudades(Document doc, XPath xpath, String depto){
		LinkedHashMap<String,Object> ciudades = new LinkedHashMap<String,Object>();
		try{
			XPathExpression exp = xpath.compile("/departamentos/departamento[@nombre='" + depto + "']/municipio/text()");
			NodeList nodes = (NodeList) exp.evaluate(doc, XPathConstants.NODESET);
            for (int i = 0; i < nodes.getLength(); i++){
            	ciudades.put(nodes.item(i).getNodeValue(), nodes.item(i).getNodeValue());
            }
//            ciudades.put("JULIAN-DEPTO=" + doc.getNodeName() + "-L=" +String.valueOf(nodes.getLength()), 1000);
		} catch (XPathExpressionException e) {
			e.printStackTrace();
		}
		return ciudades;
	}*/
	
	public LinkedHashMap<String,Object> opcionesDepartamentos(String pais){
		LinkedHashMap<String,Object> optionsDeptos = new LinkedHashMap<String,Object>();
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = null;
		try {
			dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(getClass().getResourceAsStream("/xml/deptosMundo.xml"));
			XPathFactory xpf = XPathFactory.newInstance();
			XPath xpath = xpf.newXPath();
			optionsDeptos = getDeptos(doc, xpath, pais);
		} catch (SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return optionsDeptos;
	}
	public LinkedHashMap<String,Object> getDeptos(Document doc, XPath xpath, String pais){
		LinkedHashMap<String,Object> deptos = new LinkedHashMap<String,Object>();
		try{
			XPathExpression exp = xpath.compile("/departamentos/pais[@nombre='" + pais + "']/departamento/id/text()");
			XPathExpression expNombre = xpath.compile("/departamentos/pais[@nombre='" + pais + "']/departamento/nombre/text()");
			NodeList nodes = (NodeList) exp.evaluate(doc, XPathConstants.NODESET);
			NodeList nodesNombre = (NodeList) expNombre.evaluate(doc, XPathConstants.NODESET);
            for (int i = 0; i < nodes.getLength(); i++){
            	deptos.put(nodesNombre.item(i).getNodeValue(), nodes.item(i).getNodeValue().toString());
            }
//            ciudades.put("JULIAN-DEPTO=" + doc.getNodeName() + "-L=" +String.valueOf(nodes.getLength()), 1000);
		} catch (XPathExpressionException e) {
			e.printStackTrace();
		}
		return deptos;
	}	
}