package co.com.bbva.portalvivienda.publico.dto.confronta.cuestionario;

import java.io.Serializable;

/**
 * Created by jquijano82 on 11/07/17.
 */
public class DTORespuesta implements Serializable {

    private String texto;
    private Integer idPregunta;
    private Integer idRespuesta;

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    public Integer getIdPregunta() {
        return idPregunta;
    }

    public void setIdPregunta(Integer idPregunta) {
        this.idPregunta = idPregunta;
    }

    public Integer getIdRespuesta() {
        return idRespuesta;
    }

    public void setIdRespuesta(Integer idRespuesta) {
        this.idRespuesta = idRespuesta;
    }
}
