
package co.com.bbva.portalvivienda.publico.dto;

import java.io.Serializable;




public class DTOIntOtherExpenses implements Serializable{

    public final static long serialVersionUID = 1L;
    private DTOIntValueAppraise valueAppraise;
    private DTOIntValueStudyTitle valueStudyTitle;
    private DTOIntTotalOtherExpenses totalOtherExpenses;

    public DTOIntOtherExpenses() {
        //default constructor
    }

    public DTOIntOtherExpenses(DTOIntValueAppraise valueAppraise, DTOIntValueStudyTitle valueStudyTitle, DTOIntTotalOtherExpenses totalOtherExpenses) {
        this.valueAppraise = valueAppraise;
        this.valueStudyTitle = valueStudyTitle;
        this.totalOtherExpenses = totalOtherExpenses;
    }

    public DTOIntValueAppraise getValueAppraise() {
        return valueAppraise;
    }

    public void setValueAppraise(DTOIntValueAppraise valueAppraise) {
        this.valueAppraise = valueAppraise;
    }

    public DTOIntValueStudyTitle getValueStudyTitle() {
        return valueStudyTitle;
    }

    public void setValueStudyTitle(DTOIntValueStudyTitle valueStudyTitle) {
        this.valueStudyTitle = valueStudyTitle;
    }

    public DTOIntTotalOtherExpenses getTotalOtherExpenses() {
        return totalOtherExpenses;
    }

    public void setTotalOtherExpenses(DTOIntTotalOtherExpenses totalOtherExpenses) {
        this.totalOtherExpenses = totalOtherExpenses;
    }

}
