package co.com.bbva.portalvivienda.publico.dto.confronta.cuestionario;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by jquijano82 on 11/07/17.
 */
public class DTOPregunta implements Serializable {
    private String texto;
    private List<DTORespuesta> respuestas;
    private Integer respuestaSeleccionada;

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    public List<DTORespuesta> getRespuestas() {
        if(respuestas == null){
            respuestas =  new ArrayList<DTORespuesta>();
        }
        return respuestas;
    }

    public Integer getRespuestaSeleccionada() {
        return respuestaSeleccionada;
    }

    public void setRespuestaSeleccionada(Integer respuestaSeleccionada) {
        this.respuestaSeleccionada = respuestaSeleccionada;
    }
}
