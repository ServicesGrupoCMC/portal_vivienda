
package co.com.bbva.portalvivienda.publico.dto;

import java.io.Serializable;




public class DTOIntMonthlyAmortization implements Serializable{

    public final static long serialVersionUID = 1L;
    private Double quotaNumber;
    private DTOIntCapital capital;
    private DTOIntInterests interests;
    private DTOIntUninsuredFee uninsuredFee;
    private DTOIntLifeInsuranceCost lifeInsuranceCost;
    private DTOIntValueInsuranceFiresEarthquakes valueInsuranceFiresEarthquakes;
    private DTOIntValueInsuranceQuota valueInsuranceQuota;
    private DTOIntValueCoverage valueCoverage;
    private DTOIntCoverageFee coverageFee;
    private DTOIntBalanceAmount balanceAmount;

    public DTOIntMonthlyAmortization() {
        //default constructor
    }

    public Double getQuotaNumber() {
        return quotaNumber;
    }

    public void setQuotaNumber(Double quotaNumber) {
        this.quotaNumber = quotaNumber;
    }

    public DTOIntInterests getInterests() {
        return interests;
    }

    public void setInterests(DTOIntInterests interests) {
        this.interests = interests;
    }

    public DTOIntUninsuredFee getUninsuredFee() {
        return uninsuredFee;
    }

    public void setUninsuredFee(DTOIntUninsuredFee uninsuredFee) {
        this.uninsuredFee = uninsuredFee;
    }

    public DTOIntLifeInsuranceCost getLifeInsuranceCost() {
        return lifeInsuranceCost;
    }

    public void setLifeInsuranceCost(DTOIntLifeInsuranceCost lifeInsuranceCost) {
        this.lifeInsuranceCost = lifeInsuranceCost;
    }

    public DTOIntValueInsuranceFiresEarthquakes getValueInsuranceFiresEarthquakes() {
        return valueInsuranceFiresEarthquakes;
    }

    public void setValueInsuranceFiresEarthquakes(DTOIntValueInsuranceFiresEarthquakes valueInsuranceFiresEarthquakes) {
        this.valueInsuranceFiresEarthquakes = valueInsuranceFiresEarthquakes;
    }

    public DTOIntValueInsuranceQuota getValueInsuranceQuota() {
        return valueInsuranceQuota;
    }

    public void setValueInsuranceQuota(DTOIntValueInsuranceQuota valueInsuranceQuota) {
        this.valueInsuranceQuota = valueInsuranceQuota;
    }

    public DTOIntCoverageFee getCoverageFee() {
        return coverageFee;
    }

    public void setCoverageFee(DTOIntCoverageFee coverageFee) {
        this.coverageFee = coverageFee;
    }

    public DTOIntBalanceAmount getBalanceAmount() {
        return balanceAmount;
    }

    public void setBalanceAmount(DTOIntBalanceAmount balanceAmount) {
        this.balanceAmount = balanceAmount;
    }

    /**
     * @return the capital
     */
    public DTOIntCapital getCapital() {
        return capital;
    }

    /**
     * @param capital the capital to set
     */
    public void setCapital(DTOIntCapital capital) {
        this.capital = capital;
    }

    /**
     * @return the valueCoverage
     */
    public DTOIntValueCoverage getValueCoverage() {
        return valueCoverage;
    }

    /**
     * @param valueCoverage the valueCoverage to set
     */
    public void setValueCoverage(DTOIntValueCoverage valueCoverage) {
        this.valueCoverage = valueCoverage;
    }

}
