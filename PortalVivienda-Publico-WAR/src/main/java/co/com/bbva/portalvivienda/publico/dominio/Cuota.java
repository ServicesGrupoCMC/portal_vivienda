package co.com.bbva.portalvivienda.publico.dominio;

import java.io.Serializable;

public class Cuota implements Serializable {

    private String mes;
    private String capital;
    private String intereses;
    private String cuotaSinSeguros;
    private String seguroVida;
    private String seguroIncendioTerremoto;
    private String cuotaConSeguro;
    private String saldo;
    private int consecutivo;

    public String getMes() {
        return mes;
    }

    public void setMes(String mes) {
        this.mes = mes;
    }

    public String getCapital() {
        return capital;
    }

    public void setCapital(String capital) {
        this.capital = capital;
    }

    public String getIntereses() {
        return intereses;
    }

    public void setIntereses(String intereses) {
        this.intereses = intereses;
    }

    public String getCuotaSinSeguros() {
        return cuotaSinSeguros;
    }

    public void setCuotaSinSeguros(String cuotaSinSeguros) {
        this.cuotaSinSeguros = cuotaSinSeguros;
    }

    public String getSeguroVida() {
        return seguroVida;
    }

    public void setSeguroVida(String seguroVida) {
        this.seguroVida = seguroVida;
    }

    public String getSeguroIncendioTerremoto() {
        return seguroIncendioTerremoto;
    }

    public void setSeguroIncendioTerremoto(String seguroIncendioTerremoto) {
        this.seguroIncendioTerremoto = seguroIncendioTerremoto;
    }

    public String getCuotaConSeguro() {
        return cuotaConSeguro;
    }

    public void setCuotaConSeguro(String cuotaConSeguro) {
        this.cuotaConSeguro = cuotaConSeguro;
    }

    public String getSaldo() {
        return saldo;
    }

    public void setSaldo(String saldo) {
        this.saldo = saldo;
    }

    public int getConsecutivo() {
        return consecutivo;
    }

    public void setConsecutivo(int consecutivo) {
        this.consecutivo = consecutivo;
    }

}
