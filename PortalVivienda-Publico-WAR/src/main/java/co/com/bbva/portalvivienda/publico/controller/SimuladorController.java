package co.com.bbva.portalvivienda.publico.controller;

import co.com.bbva.portalvivienda.publico.SimuladorPasoAPaso;
import co.com.bbva.portalvivienda.publico.dto.*;
import co.com.bbva.portalvivienda.publico.service.LoanSimulationService;
import co.com.bbva.portalvivienda.publico.utilidades.GeoLocalizacion;

import com.bbva.ekip.arq.clients.tipos.ExcepcionClienteRest;

import org.apache.log4j.Logger;
import org.primefaces.event.SelectEvent;
import org.primefaces.event.SlideEndEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.webflow.execution.RequestContext;
import org.springframework.webflow.execution.RequestContextHolder;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIOutput;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author alejandro.daza
 */
public class SimuladorController implements Serializable {

    private final static Logger LOG = Logger.getLogger(SimuladorController.class);

    @Autowired
    private LoanSimulationService loanSimulationService;
    private SimuladorPasoAPaso simuladorPasoAPasoSaved = new SimuladorPasoAPaso();

    private Map<String, Object> paises = new HashMap<String, Object>();
    private Map<String, Object> departamentos = new HashMap<String, Object>();
    private Map<String, Object> ciudades = new HashMap<String, Object>();
    private Map<String, Integer> aniosAmortizacion = new LinkedHashMap<String, Integer>();

    public void establecerPaises() {
        GeoLocalizacion g = new GeoLocalizacion();
        this.setPaises(g.OpcionesPais());
        /* Comentarizado para tomar los paises que indica el cliente. Julian Lopez Romero. 02/08/2017       
         * 		this.getPaises().put("COLOMBIA", 42);
         this.getPaises().put("AFGANISTAN", 1);
         this.getPaises().put("ALBANIA", 2);
         this.getPaises().put("ALEMANIA", 3);
         this.getPaises().put("ARMENIA", 4);
         this.getPaises().put("ARUBA", 5);
         this.getPaises().put("BOSNIA-HERZEGOVINA", 6);
         this.getPaises().put("BURKINA FASSO", 7);
         this.getPaises().put("ANDORRA", 8);
         this.getPaises().put("ANGOLA", 9);
         this.getPaises().put("ANGUILLA", 10);
         this.getPaises().put("ANTIGUA Y BARBUDA", 11);
         this.getPaises().put("ANTILLAS HOLANDESAS", 12);
         this.getPaises().put("ARABIA SAUDITA", 13);
         this.getPaises().put("ARGELIA", 14);
         this.getPaises().put("ARGENTINA", 15);
         this.getPaises().put("AUSTRALIA", 16);
         this.getPaises().put("AUSTRIA", 17);
         this.getPaises().put("AZERBAIJAN", 18);
         this.getPaises().put("BAHAMAS", 19);
         this.getPaises().put("BAHREIN", 20);
         this.getPaises().put("BANGLADESH", 21);
         this.getPaises().put("BARBADOS", 22);
         this.getPaises().put("BELGICA", 23);
         this.getPaises().put("BELICE", 24);
         this.getPaises().put("BERMUDAS", 25);
         this.getPaises().put("BELARUS", 26);
         this.getPaises().put("BIRMANIA (MYANMAR)", 27);
         this.getPaises().put("BOLIVIA", 28);
         this.getPaises().put("BOTSWANA", 29);
         this.getPaises().put("BRASIL", 30);
         this.getPaises().put("BRUNEI DARUSSALAM", 31);
         this.getPaises().put("BULGARIA", 32);
         this.getPaises().put("BURUNDI", 33);
         this.getPaises().put("BUTAN", 34);
         this.getPaises().put("CABO VERDE", 35);
         this.getPaises().put("CAIMAN, ISLAS", 36);
         this.getPaises().put("CAMBOYA (KAMPUCHEA)", 37);
         this.getPaises().put("CAMERUN, REPUBLICA UNIDA DEL", 38);
         this.getPaises().put("CANADA", 39);
         this.getPaises().put("SANTA SEDE", 40);
         this.getPaises().put("COCOS (KEELING), ISLAS", 41);
         this.getPaises().put("COMORAS", 43);*/
    }

    public void onPaisesChange() {
        RequestContext context = RequestContextHolder.getRequestContext();
        DTOIntLoanSimulation dtoLoanSimulation = (DTOIntLoanSimulation) context.getFlowScope().get("dtoLoanSimulation");
        String country = dtoLoanSimulation.getCountryResidence().getId();
        dtoLoanSimulation.setStateResidence("");
        this.getDepartamentos().clear();
        context.getFlowScope().put("dtoLoanSimulation", dtoLoanSimulation);
        GeoLocalizacion g = new GeoLocalizacion();
        this.setDepartamentos(g.opcionesDepartamentos(country));
        /* Comentarizado para tomar los departamentos que indica el cliente. Julian Lopez Romero. 02/08/2017       
         * 			if (country != null && !country.equals("")) {
         if (country.equals("42")) {
         this.getDepartamentos().put("ANTIOQUIA", 1);
         this.getDepartamentos().put("ATLANTICO", 2);
         this.getDepartamentos().put("BOGOTA", 3);
         this.getDepartamentos().put("BOLIVAR", 4);
         this.getDepartamentos().put("BOYACA", 5);
         this.getDepartamentos().put("CALDAS", 6);
         this.getDepartamentos().put("CAQUETA", 7);
         this.getDepartamentos().put("CAUCA", 8);
         this.getDepartamentos().put("CESAR", 9);
         this.getDepartamentos().put("CORDOBA", 10);
         this.getDepartamentos().put("CUNDINAMARCA", 11);
         this.getDepartamentos().put("CHOCO", 12);
         this.getDepartamentos().put("HUILA", 13);
         this.getDepartamentos().put("LA GUAJIRA", 14);
         this.getDepartamentos().put("MAGDALENA", 15);
         this.getDepartamentos().put("META", 16);
         this.getDepartamentos().put("NARIÑO", 17);
         this.getDepartamentos().put("N. DE SANTANDER", 18);
         this.getDepartamentos().put("QUINDIO", 19);
         this.getDepartamentos().put("RISARALDA", 20);
         this.getDepartamentos().put("SANTANDER", 21);
         this.getDepartamentos().put("SUCRE", 22);
         this.getDepartamentos().put("TOLIMA", 23);
         this.getDepartamentos().put("VALLE DEL CAUCA", 24);
         this.getDepartamentos().put("ARAUCA", 25);
         this.getDepartamentos().put("PUTUMAYO", 26);
         this.getDepartamentos().put("SAN ANDRES", 27);
         this.getDepartamentos().put("AMAZONAS", 28);
         this.getDepartamentos().put("GUAINIA", 29);
         this.getDepartamentos().put("GUAVIARE", 30);
         this.getDepartamentos().put("VAUPES", 31);
         this.getDepartamentos().put("VICHADA", 32);
         } else {
         this.getDepartamentos().clear();
         this.getCiudades().clear();
         }
         } else {
         this.getDepartamentos().clear();
         }*/
    }

    public void onDepartamentosChange() {
        RequestContext context = RequestContextHolder.getRequestContext();
        DTOIntLoanSimulation dtoLoanSimulation = (DTOIntLoanSimulation) context.getFlowScope().get("dtoLoanSimulation");
        String state = dtoLoanSimulation.getStateResidence();
        String pais = dtoLoanSimulation.getCountryResidence().getId();
        dtoLoanSimulation.setCityResidence("");
        this.getCiudades().clear();
        context.getFlowScope().put("dtoLoanSimulation", dtoLoanSimulation);
        GeoLocalizacion g = new GeoLocalizacion();
        this.setCiudades(g.opcionesCiudades(pais, state));
        /* Comentarizado para tomar las ciudades que indica el cliente. Julian Lopez Romero. 02/08/2017        
         * 			if (state != null && !state.equals("")) {
         int stateId = Integer.parseInt(state);
         switch (stateId) {
         case 1:
         this.getCiudades().put("MEDELLIN", 1);
         this.getCiudades().put("ABEJORRAL", 2);
         this.getCiudades().put("ABRIAQUI", 3);
         this.getCiudades().put("ALEJANDRIA", 4);
         this.getCiudades().put("AMAGA", 5);
         break;
         case 2:
         this.getCiudades().put("BARRANQUILLA", 1);
         this.getCiudades().put("BARANOA", 2);
         this.getCiudades().put("CAMPO DE LA CRUZ", 3);
         this.getCiudades().put("CANDELARIA", 4);
         this.getCiudades().put("GALAPA", 5);
         break;
         case 3:
         this.getCiudades().put("BOGOTA, D.C.", 1);
         break;
         case 4:
         this.getCiudades().put("CARTAGENA", 1);
         this.getCiudades().put("ACHI", 2);
         this.getCiudades().put("ALTOS DEL ROSARIO", 3);
         this.getCiudades().put("ARENAL", 4);
         this.getCiudades().put("ARJONA", 5);
         break;
         case 32:
         this.getCiudades().put("PUERTO CARREÑO", 1);
         this.getCiudades().put("LA PRIMAVERA", 2);
         this.getCiudades().put("SANTA ROSALIA", 3);
         this.getCiudades().put("CUMARIBO", 4);
         break;
         default:
         this.getCiudades().clear();
         break;
         }
         }*/
    }

    public SimuladorController() {
    }

    public void init() {
        if (LOG.isDebugEnabled()) {
            LOG.debug(">>> SimuladorController::init()");
        }
        RequestContext context = RequestContextHolder.getRequestContext();
        DTOIntLoanSimulation dtoLoanSimulation = (DTOIntLoanSimulation) context.getFlowScope().get("dtoLoanSimulation");
        dtoLoanSimulation.setFinancialLine("");
        dtoLoanSimulation.setFinancialSubline("");
        dtoLoanSimulation.setPriceHousing(new DTOIntPriceHousing(30000000, "COP"));
        dtoLoanSimulation.setMontlyIncome(new DTOIntMontlyIncome(737717, "COP"));
        dtoLoanSimulation.setAmountToFinanced(new DTOIntAmountToFinanced(8000000, "COP"));
        dtoLoanSimulation.setYearsToFinanced(5);
        dtoLoanSimulation.setBuyPercentage("15");
        dtoLoanSimulation.setQuotaValue(new DTOIntQuotaValue(0, "COP"));
        dtoLoanSimulation.setYearsToFinancedString("5 Años");
        dtoLoanSimulation.setDocumentType(new DTOIntDocumentType("", ""));
        dtoLoanSimulation.setCountryResidence(new DTOIntCountryResidence("", ""));
        dtoLoanSimulation.setAmortizationType("P");

        Map<String, String> financialLines = new HashMap<String, String>();
        financialLines.put("Crédito Hipotecario", "01");
        financialLines.put("Leasing Habitacional", "02");
        dtoLoanSimulation.setFinancialLines(financialLines);

        Map<String, String> financialSublines = new HashMap<String, String>();
        financialSublines.put("Compra de Vivienda", "01");
        financialSublines.put("Compra de Cartera", "02");
        financialSublines.put("Remodelación", "03");
        financialSublines.put("Construcción de Vivienda", "04");
        financialSublines.put("Cesión", "05");
        dtoLoanSimulation.setFinancialSublines(financialSublines);
        this.establecerPaises();

        if (LOG.isDebugEnabled()) {
            LOG.debug("<<< SimuladorController::init()");
        }
    }

    public void onFinancialLineChange() {
        RequestContext context = RequestContextHolder.getRequestContext();
        DTOIntLoanSimulation dtoLoanSimulation = (DTOIntLoanSimulation) context.getFlowScope().get("dtoLoanSimulation");
        String financialLine = dtoLoanSimulation.getFinancialLine();
        String financialSubline = dtoLoanSimulation.getFinancialSubline();
        if (financialLine.equals("") || financialSubline.equals("")) {
            String mensajeError = "";
            if (financialLine.equals("") && financialSubline.equals("")) {
                mensajeError = "Para iniciar a simular debes seleccionar los campos \"quiero\" y \"con un\".";
            } else if (financialLine.equals("") || financialLine == null) {
                mensajeError = "Para iniciar a simular debes seleccionar el campo \"con un\".";
            } else if (financialSubline.equals("") || financialSubline == null) {
                mensajeError = "Para iniciar a simular debes seleccionar el campo \"quiero\".";
            }
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, null, mensajeError);
            FacesContext.getCurrentInstance().addMessage(null, message);
        } else {
            this.loanSimulation(dtoLoanSimulation);
        }
    }

    public void onFinancialSublineChange() {
        RequestContext context = RequestContextHolder.getRequestContext();
        DTOIntLoanSimulation dtoLoanSimulation = (DTOIntLoanSimulation) context.getFlowScope().get("dtoLoanSimulation");
        String financialLine = dtoLoanSimulation.getFinancialLine();
        String financialSubline = dtoLoanSimulation.getFinancialSubline();
        boolean sw = true;

        if (financialSubline.equals("")) {
            sw = false;
        } else {
            dtoLoanSimulation.getFinancialLines().clear();
        }

        if (financialSubline.equals("01") || financialSubline.equals("02")) {
            // Leasing Habitacional y Credito Hipotecario
            Map<String, String> financialLines = new HashMap<String, String>();
            financialLines.put("Crédito Hipotecario", "01");
            financialLines.put("Leasing Habitacional", "02");
            dtoLoanSimulation.setFinancialLines(financialLines);

        } else if (financialSubline.equals("05")) {
            // Leasing Habitacional
            Map<String, String> financialLines = new HashMap<String, String>();
            financialLines.put("Leasing Habitacional", "02");
            dtoLoanSimulation.setFinancialLines(financialLines);

        } else if (financialSubline.equals("03") || financialSubline.equals("04")) {
            // Credito Hipotecario
            Map<String, String> financialLines = new HashMap<String, String>();
            financialLines.put("Crédito Hipotecario", "01");
            dtoLoanSimulation.setFinancialLines(financialLines);
        }

        if ("".equals(financialLine) || sw == false) {
            String mensajeError = "";

           if (financialLine.equals("") && financialSubline.equals("")) {
                mensajeError = "Para iniciar a simular debes seleccionar los campos \"quiero\" y \"con un\".";
            } else if (financialLine.equals("") || financialLine == null) {
                mensajeError = "Para iniciar a simular debes seleccionar el campo \"con un\".";
            } else if (financialSubline.equals("") || financialSubline == null) {
                mensajeError = "Para iniciar a simular debes seleccionar el campo \"quiero\".";
            }

            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Simulacion no Exitosa", mensajeError);
            FacesContext.getCurrentInstance().addMessage(null, message);
        } else {
            this.loanSimulation(dtoLoanSimulation);
        }
    }

    public void cambioRadio() {
    }

    public void onDateSelect(SelectEvent event) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        String fecha = format.format(event.getObject());
    }

    public void eventBuyPercentage(SlideEndEvent event) {
        RequestContext context = RequestContextHolder.getRequestContext();
        DTOIntLoanSimulation dtoLoanSimulation = (DTOIntLoanSimulation) context.getFlowScope().get("dtoLoanSimulation");
        dtoLoanSimulation.setBuyPercentage(Integer.toString(event.getValue()));
        context.getFlowScope().put("dtoLoanSimulation", dtoLoanSimulation);
    }

    public void loanSimulationComplete(DTOIntLoanSimulation dtoLoanSimulation) {
        aniosAmortizacion.clear();
        for (int i = 1; i <= dtoLoanSimulation.getYearsToFinanced(); i++) {
            aniosAmortizacion.put("Año " + i, i);
        }
        System.out.print("Datos financieros" + dtoLoanSimulation.getFinancialLine() + ", " + dtoLoanSimulation.getFinancialSubline());
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        String fecha = format.format(dtoLoanSimulation.getBirthDate());
        dtoLoanSimulation.setFechaNacimiento(fecha);

        RequestContext context = RequestContextHolder.getRequestContext();
        Object formulario3 = context.getFlowScope().get("formulario3");
        boolean f3 = (Boolean) formulario3;

        Object subformulario3 = context.getFlowScope().get("subformulario3");
        boolean sf3 = (Boolean) subformulario3;

        Object formulario4 = context.getFlowScope().get("formulario4");
        boolean f4 = (Boolean) formulario4;

        DTOIntLoanSimulation loanSimulate;

        try {
            loanSimulate = getLoanSimulationService().completeLoanSimulation(dtoLoanSimulation);
            context.getFlowScope().put("dtoLoanSimulation", loanSimulate);

            // valores en el wizard quitar esto, solo esta para cuando el servicio no responde
            context.getFlowScope().put("formulario3", false);
            context.getFlowScope().put("subformulario3", false);
            context.getFlowScope().put("formulario4", true);

            // valores para el wizard
            context.getFlowScope().put("formulario3", false);
            context.getFlowScope().put("subformulario3", false);
            context.getFlowScope().put("formulario4", true);

            FacesMessage message = new FacesMessage("Simulacion Exitosa", "Nuevo Valor de la Cuota: " + loanSimulate.getQuotaValue().getAmount());
            FacesContext.getCurrentInstance().addMessage(null, message);

        } catch (ExcepcionClienteRest ex) {
            LOG.error(ex.getMessage(), ex);
            FacesMessage message = new FacesMessage("Simulacion Falida", "Error: " + ex.getMessage());
            FacesContext.getCurrentInstance().addMessage(null, message);

        } catch (Exception ex) {
            LOG.error(ex.getMessage(), ex);
            FacesMessage message = new FacesMessage("Simulacion Falida", "Error: " + ex.getMessage());
            FacesContext.getCurrentInstance().addMessage(null, message);
        }

    }

    public void loanSimulation(DTOIntLoanSimulation dtoLoanSimulation) {
        RequestContext context = RequestContextHolder.getRequestContext();
        DTOIntLoanSimulation loanSimulate;
        String mensajeError = "";
        try {
            loanSimulate = getLoanSimulationService().loanSimulate(dtoLoanSimulation);
            context.getFlowScope().put("dtoLoanSimulation", loanSimulate);
            FacesMessage message = new FacesMessage("Simulacion Exitosa", "Nuevo Valor de la Cuota: " + loanSimulate.getQuotaValue().getAmount());
            FacesContext.getCurrentInstance().addMessage(null, message);
        } catch (ExcepcionClienteRest ex) {
            LOG.error(ex.getMessage(), ex);
            if (ex.getMessage().equals("PVE0001 SUPERA PORC. 70,00%")){
                mensajeError = "¡Ops! El monto solicitado supera los límites permitidos. Recuerda que para vivienda No VIS podemos financiar hasta el 70% del valor de la vivienda.";
            }
            else if (ex.getMessage().equals("PVE0002 SUPERA PORC. 30,00%")){
                mensajeError = "¡Ops! La cuota supera el límite de Ley. Recuerda que la cuota de tu crédito de vivienda no puede superar el 30% de los ingresos que reportas.";
            }
            else if (ex.getMessage().equals("PVE0003 SUPERA PORC. 80,00%")){
                mensajeError = "¡Ops! El monto solicitado supera los límites permitidos. Recuerda que para vivienda de interés social podemos financiar hasta el 80% del valor de la vivienda.";
            }
//            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, ex.getMessage());
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, null, mensajeError);
            FacesContext.getCurrentInstance().addMessage(null, message);
//            System.out.println("Linea2 "+ loanSimulate.getFinancialLine() + "Sublinea2 " + loanSimulate.getFinancialSubline());
        } catch (Exception ex) {
            LOG.error(ex.getMessage(), ex);
        }
    }

    public void displayData(SimuladorPasoAPaso simuladorPasoAPaso, RequestContext context) throws Exception {
        simuladorPasoAPaso = getLoanSimulationService().simulate2(simuladorPasoAPaso, context);
    }

    public void amortitationTable() throws Exception {
    }

    public void eventSlider(SlideEndEvent event) {
        RequestContext context = RequestContextHolder.getRequestContext();
        DTOIntLoanSimulation dtoLoanSimulation = (DTOIntLoanSimulation) context.getFlowScope().get("dtoLoanSimulation");
        String slider = event.getComponent().getId();
        String financialLine = dtoLoanSimulation.getFinancialLine();
        String financialSubline = dtoLoanSimulation.getFinancialSubline();

        if (slider.equals("priceHousing") || slider.equals("slideVivienda")) {
            dtoLoanSimulation.setPriceHousing(new DTOIntPriceHousing(event.getValue(), "COP"));
        } else if (slider.equals("montlyIncome") || slider.equals("slideIngresos")) {
            dtoLoanSimulation.setMontlyIncome(new DTOIntMontlyIncome(event.getValue(), "COP"));
        } else if (slider.equals("amountToFinanced") || slider.equals("slideValorCredito")) {
            dtoLoanSimulation.setAmountToFinanced(new DTOIntAmountToFinanced(event.getValue(), "COP"));
        } else if (slider.equals("yearsToFinanced") || slider.equals("slideAnoCasa")) {
            dtoLoanSimulation.setYearsToFinanced(event.getValue());
            dtoLoanSimulation.setYearsToFinancedString(String.valueOf(event.getValue()) + " Años");
        }
        
        if ("".equals(financialLine) || "".equals(financialSubline)) {
            String mensajeError = "";
            context.getFlowScope().put("dtoLoanSimulation", dtoLoanSimulation);
            if (financialLine.equals("") && financialSubline.equals("")) {
                mensajeError = "Para iniciar a simular debes seleccionar los campos \"quiero\" y \"con un\".";
            } else if (financialLine.equals("") || financialLine == null) {
                mensajeError = "Para iniciar a simular debes seleccionar el campo \"con un\".";
            } else if (financialSubline.equals("") || financialSubline == null) {
                mensajeError = "Para iniciar a simular debes seleccionar el campo \"quiero\".";
            }
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, null, mensajeError);
            FacesContext.getCurrentInstance().addMessage(null, message);
        } 
//        else if (financialLine.equals("01") && financialSubline.equals("03")){
//            String anios = dtoLoanSimulation.getYearsToFinancedString().replaceAll("[A-Za-z]?[Ññ]", " ").trim();
//            int aniosInt = Integer.parseInt(anios);
//            String mensajeError = "";
//            if (aniosInt >= 11) {
//                mensajeError = "¡Ops! El plazo solicitado no está¡ dentro del rango permitido. Recuerda que para remodelación el plazo permitido es de 5 a 10 años.";
//
//                FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, null, mensajeError);
//                FacesContext.getCurrentInstance().addMessage(null, message);
//                dtoLoanSimulation.setMensajeValidacion(mensajeError);
//            }
//        }
        else {
            this.loanSimulation(dtoLoanSimulation);
        }
    }

    /**
     * @return the loanSimulationService
     */
    public LoanSimulationService getLoanSimulationService() {
        return loanSimulationService;
    }

    /**
     * @param loanSimulationService the loanSimulationService to set
     */
    public void setLoanSimulationService(LoanSimulationService loanSimulationService) {
        this.loanSimulationService = loanSimulationService;
    }

    /**
     * @return the simuladorPasoAPasoSaved
     */
    public SimuladorPasoAPaso getSimuladorPasoAPasoSaved() {
        return simuladorPasoAPasoSaved;
    }

    /**
     * @param simuladorPasoAPasoSaved the simuladorPasoAPasoSaved to set
     */
    public void setSimuladorPasoAPasoSaved(SimuladorPasoAPaso simuladorPasoAPasoSaved) {
        this.simuladorPasoAPasoSaved = simuladorPasoAPasoSaved;
    }

    /**
     * @return the paises
     */
    public Map<String, Object> getPaises() {
        return paises;
    }

    /**
     * @param paises the paises to set
     */
    public void setPaises(Map<String, Object> paises) {
        this.paises = paises;
    }

    /**
     * @return the departamentos
     */
    public Map<String, Object> getDepartamentos() {
        return departamentos;
    }

    /**
     * @param departamentos the departamentos to set
     */
    public void setDepartamentos(Map<String, Object> departamentos) {
        this.departamentos = departamentos;
    }

    /**
     * @return the ciudades
     */
    public Map<String, Object> getCiudades() {
        return ciudades;
    }

    /**
     * @param ciudades the ciudades to set
     */
    public void setCiudades(Map<String, Object> ciudades) {
        this.ciudades = ciudades;
    }

    public Map<String, Integer> getAniosAmortizacion() {
        return aniosAmortizacion;
    }

    public void eventoChange(AjaxBehaviorEvent event) {
        RequestContext context = RequestContextHolder.getRequestContext();
        DTOIntLoanSimulation dtoLoanSimulation = (DTOIntLoanSimulation) context.getFlowScope().get("dtoLoanSimulation");
        String evento = event.getComponent().getId();
        String financialLine = dtoLoanSimulation.getFinancialLine();
        String financialSubline = dtoLoanSimulation.getFinancialSubline();

        if (evento.equals("valorVivienda") || evento.equals("valorViviendaLittle")) {
            dtoLoanSimulation.setPriceHousing(new DTOIntPriceHousing((Double) ((UIOutput) event.getSource()).getValue(), "COP"));
        } else if (evento.equals("valorIngresos") || evento.equals("slideIngresos")) {
            dtoLoanSimulation.setMontlyIncome(new DTOIntMontlyIncome((Double) ((UIOutput) event.getSource()).getValue(), "COP"));
        } else if (evento.equals("valorSolicitado") || evento.equals("valorSolicitadoLittle")) {
            dtoLoanSimulation.setAmountToFinanced(new DTOIntAmountToFinanced((Double) ((UIOutput) event.getSource()).getValue(), "COP"));
        } else if (evento.equals("anho") || evento.equals("anhoLittle")) {
            String anho = (String) ((UIOutput) event.getSource()).getValue();
            String retorno = anho.substring(0, anho.indexOf("Años"));
            retorno = retorno.trim();
            dtoLoanSimulation.setYearsToFinanced(Integer.parseInt(retorno));
            dtoLoanSimulation.setYearsToFinancedString(retorno.toString() + " Años");
        }

        if ("".equals(financialLine) || "".equals(financialSubline)) {
            String mensajeError = "";
            context.getFlowScope().put("dtoLoanSimulation", dtoLoanSimulation);
            if (financialLine.equals("") && financialSubline.equals("")) {
                mensajeError = "Para iniciar a simular debes seleccionar los campos \"quiero\" y \"con un\".";
            } else if (financialLine.equals("") || financialLine == null) {
                mensajeError = "Para iniciar a simular debes seleccionar el campo \"con un\".";
            } else if (financialSubline.equals("") || financialSubline == null) {
                mensajeError = "Para iniciar a simular debes seleccionar el campo \"quiero\".";
            }
            FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, null, mensajeError);
            FacesContext.getCurrentInstance().addMessage(null, message);
            dtoLoanSimulation.setMensajeValidacion(mensajeError);
        } else if (financialLine.equals("01") && financialSubline.equals("03")) {
            String anios = dtoLoanSimulation.getYearsToFinancedString().replaceAll("[A-Za-z]", " ").replace("ñ", " ").trim();
            int aniosInt = Integer.parseInt(anios);
            String mensajeError = "";
            if (aniosInt >= 11) {
                mensajeError = "¡Ops! El plazo solicitado no está dentro del rango permitido. Recuerda que para remodelación el plazo permitido es de 5 a 10 años.";

                FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, null, mensajeError);
                FacesContext.getCurrentInstance().addMessage(null, message);
                dtoLoanSimulation.setMensajeValidacion(mensajeError);
            }
        } else if (financialLine.equals("01") && financialSubline.equals("04")) {
            DTOIntMontlyIncome money = new DTOIntMontlyIncome();
            double montlyIncome = dtoLoanSimulation.getMontlyIncome().getAmount();
            double housePricing = dtoLoanSimulation.getPriceHousing().getAmount();
            double moneyNeeded = dtoLoanSimulation.getAmountToFinanced().getAmount();
            String mensajeError = "";
            if (montlyIncome < 6000000) {
                mensajeError = "¡Ops! El ingreso reportado no está en el rango permitido. Recuerda que para la línea Contructor Individual el ingreso mínimo es de $6 millones.";

                FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, null, mensajeError);
                FacesContext.getCurrentInstance().addMessage(null, message);
                dtoLoanSimulation.setMensajeValidacion(mensajeError);
            } else if (housePricing < 250000000){
                mensajeError = "¡Ops! El valor de la vivienda no se encuentra en el rango permitido. Recuerda que para Constructor Individual el valor estimado de la vivienda a su terminación no puede ser inferior a $250 millones.";
                
                FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, null, mensajeError);
                FacesContext.getCurrentInstance().addMessage(null, message);
                dtoLoanSimulation.setMensajeValidacion(mensajeError);
            } else if (moneyNeeded < 150000000){
                mensajeError = "¡Ops! El monto solicitado  no se encuentra en el rango permitido. Recuerda que para Constructor Individual podemos financiar desde $150 millones.";
                
                FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR, null, mensajeError);
                FacesContext.getCurrentInstance().addMessage(null, message);
                dtoLoanSimulation.setMensajeValidacion(mensajeError);
            }
        } else {
            dtoLoanSimulation.setFinancialLine(dtoLoanSimulation.getFinancialLine());
            dtoLoanSimulation.setFinancialSubline(dtoLoanSimulation.getFinancialSubline());
            this.loanSimulation(dtoLoanSimulation);
        }
    }
}
