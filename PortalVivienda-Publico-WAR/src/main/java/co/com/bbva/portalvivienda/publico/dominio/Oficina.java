package co.com.bbva.portalvivienda.publico.dominio;

import java.io.Serializable;

public class Oficina implements Serializable {
	private String nombre;
	private String direccion;
	private String telefono;
	private String horario;
	private String horarioAdicionalEntreSemana;
	private String horarioAdicionalSabados;
	private String id;
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}
	public String getHorario() {
		return horario;
	}
	public void setHorario(String horario) {
		this.horario = horario;
	}
	public String getHorarioAdicionalEntreSemana() {
		return horarioAdicionalEntreSemana;
	}
	public void setHorarioAdicionalEntreSemana(String horarioAdicionalEntreSemana) {
		this.horarioAdicionalEntreSemana = horarioAdicionalEntreSemana;
	}
	public String getHorarioAdicionalSabados() {
		return horarioAdicionalSabados;
	}
	public void setHorarioAdicionalSabados(String horarioAdicionalSabados) {
		this.horarioAdicionalSabados = horarioAdicionalSabados;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

    @Override
    public String toString() {
        return " "+ this.nombre
                + " " + this.telefono
                + " " + this.horario
                + " " + this.direccion
                + " " + this.horarioAdicionalEntreSemana
                + " " + this.horarioAdicionalSabados
                + " " + this.id;
    }
        

}
