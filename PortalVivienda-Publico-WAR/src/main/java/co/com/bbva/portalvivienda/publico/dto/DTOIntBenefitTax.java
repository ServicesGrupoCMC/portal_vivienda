
package co.com.bbva.portalvivienda.publico.dto;

import java.io.Serializable;




public class DTOIntBenefitTax implements Serializable{

    public final static long serialVersionUID = 1L;
    private double amount;
    private String currency;

    public DTOIntBenefitTax() {
        //default constructor
    }

    public DTOIntBenefitTax(double amount, String currency) {
        this.amount = amount;
        this.currency = currency;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

}
