
package co.com.bbva.portalvivienda.publico.dto;

import java.io.Serializable;




public class DTOIntRegistrationFees implements Serializable{

    public final static long serialVersionUID = 1L;
    private DTOIntRegistrationRight registrationRight;
    private DTOIntCertificateOfFreedom certificateOfFreedom;
    private DTOIntBenefitTax benefitTax;
    private DTOIntTotalExpensesRegistration totalExpensesRegistration;

    public DTOIntRegistrationFees() {
        //default constructor
    }

    public DTOIntRegistrationFees(DTOIntRegistrationRight registrationRight, DTOIntCertificateOfFreedom certificateOfFreedom, DTOIntBenefitTax benefitTax, DTOIntTotalExpensesRegistration totalExpensesRegistration) {
        this.registrationRight = registrationRight;
        this.certificateOfFreedom = certificateOfFreedom;
        this.benefitTax = benefitTax;
        this.totalExpensesRegistration = totalExpensesRegistration;
    }
    

    public DTOIntRegistrationRight getRegistrationRight() {
        return registrationRight;
    }

    public void setRegistrationRight(DTOIntRegistrationRight registrationRight) {
        this.registrationRight = registrationRight;
    }

    public DTOIntCertificateOfFreedom getCertificateOfFreedom() {
        return certificateOfFreedom;
    }

    public void setCertificateOfFreedom(DTOIntCertificateOfFreedom certificateOfFreedom) {
        this.certificateOfFreedom = certificateOfFreedom;
    }

    public DTOIntBenefitTax getBenefitTax() {
        return benefitTax;
    }

    public void setBenefitTax(DTOIntBenefitTax benefitTax) {
        this.benefitTax = benefitTax;
    }

    public DTOIntTotalExpensesRegistration getTotalExpensesRegistration() {
        return totalExpensesRegistration;
    }

    public void setTotalExpensesRegistration(DTOIntTotalExpensesRegistration totalExpensesRegistration) {
        this.totalExpensesRegistration = totalExpensesRegistration;
    }

}
