package co.com.bbva.portalvivienda.publico.dominio;

import org.primefaces.event.SlideEndEvent;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.util.*;

public class Simulador implements Serializable {
    protected long valorVivienda;
    protected String stringValorVivienda;
    protected int ingMensuales;
    protected String stringIngMensuales;
    protected int valorCredito;
    protected String stringValorCredito;
    protected int anosCredito;
    protected DecimalFormat formatter = new DecimalFormat("###,###,###");
    protected Map<String, String> optionQuiero;
    protected Map<String, String> optionCon;
    protected String quiero;
    protected String con;
    protected String monedaCredito;
    protected String gobierno;
    protected String militares;
    protected String pais;
    protected String nominaBbva;
    protected String proyectoFinanciado;
    protected String vivencia;
    protected String tipoIdentificacion;
    protected String nombres;
    protected String apellidos;
    protected Date nacimiento;
    protected String correo;
    protected Integer fijo;
    protected String celular;
    protected String actividadEco;
    protected String ocupacion;
    protected String nueva;
    protected int porcentajeCompra;
    protected int paso;
    protected int valorCuota;
    protected String stringValorCuota;
    protected String actividadEconomica;
    protected String identificacion;
    protected LinkedHashMap<String, Object> optionsActividadEconomica;
    protected LinkedHashMap<String, Object> optionsOcupacion;
    protected LinkedHashMap<String, Object> optionsPais;
    protected LinkedHashMap<String, Object> optionsDepartamento;
    protected LinkedHashMap<String, Object> optionsDepartamentoResidente;
    protected LinkedHashMap<String, Object> optionsCiudadOficina;
    protected LinkedHashMap<String, Object> optionsZonaOficina;
    protected String residesColombia;
    protected String paisResidencia;
    protected String departamentoResidencia;
    protected String ciudadOficina;
    protected String zonaOficina;
    protected ArrayList<Oficina> oficinas;
    protected Oficina oficina;
    protected String mensageNoRecords;
    protected ArrayList<Cuota> cuotas;
    protected Date fechaContacto;
    protected String horarioContacto;
    protected String telefonoContacto;

    protected void iniVars() {
        this.valorVivienda = 130000000;
        this.ingMensuales = 5000000;
        this.valorCredito = 90000000;
        this.anosCredito = 15;
        this.porcentajeCompra = 15;
        this.stringValorVivienda = this.formatStrig(this.getValorVivienda());
        this.stringIngMensuales = this.formatStrig(this.getIngMensuales());
        this.stringValorCredito = this.formatStrig(this.getValorCredito());

        this.optionQuiero = new HashMap<String, String>();
        this.optionCon = new HashMap<String, String>();

        this.optionQuiero.put("compra de vivienda", "vivienda");
        this.optionQuiero.put("compra de cartera", "cartera");
        this.optionQuiero.put("remodelación", "remodelacion");
        this.optionQuiero.put("construcción vivienda", "construcción vivienda");
        this.optionQuiero.put("cesión", "cesion");

        this.optionCon.put("crédito hipotecario", "hipoteario");
        this.optionCon.put("leasing habitacional", "habitacional");

        this.setQuiero(this.getQuiero());
        this.setQuiero(this.getCon());

        this.setVivencia("No");

        this.setAnosCredito(this.getAnosCredito());


        this.setTipoIdentificacion(this.getTipoIdentificacion());
        this.setIdentificacion(this.getIdentificacion());
        this.setNombres(this.getNombres());
        this.setApellidos(this.apellidos);
        this.setNacimiento(this.nacimiento);
        this.setCorreo(this.getCorreo());
        this.setFijo(this.getFijo());
        this.setCelular(this.getCelular());
        this.setActividadEconomica(this.getActividadEconomica());
        this.setActividadEco(this.getActividadEco());
        this.setPorcentajeCompra(this.getPorcentajeCompra());

        this.setMonedaCredito("pesos");
        this.setStringValorCuota("$1.250.000");
        this.setPaso(1);

        this.optionsDepartamentoResidente = new LinkedHashMap<String, Object>();
        this.oficinas = new ArrayList<Oficina>();
        this.setMensageNoRecords("Seleccione la ciudad");
        this.cuotas = new ArrayList<Cuota>();

    }

    public int getPorcentajeCompra() {
        return porcentajeCompra;
    }

    public void setPorcentajeCompra(int porcentajeCompra) {
        this.porcentajeCompra = porcentajeCompra;
    }


    public String getQuiero() {
        return quiero;
    }

    public void setQuiero(String quiero) {
        this.quiero = quiero;
    }

    public String getCon() {
        return con;
    }

    public void setCon(String con) {
        this.con = con;
    }

    public Map<String, String> getOptionQuiero() {
        return optionQuiero;
    }

    public void setOptionQuiero(Map<String, String> optionQuiero) {
        this.optionQuiero = optionQuiero;
    }

    public Map<String, String> getOptionCon() {
        return optionCon;
    }

    public void setOptionCon(Map<String, String> optionCon) {
        this.optionCon = optionCon;
    }

    public long getValorVivienda() {
        return valorVivienda;
    }

    public void setValorVivienda(long valorVivienda) {
        this.valorVivienda = valorVivienda;

    }

    public int getIngMensuales() {
        return ingMensuales;
    }

    public void setIngMensuales(int ingMensuales) {
        this.ingMensuales = ingMensuales;
    }

    public int getValorCredito() {
        return valorCredito;
    }

    public void setValorCredito(int valorCredito) {
        this.valorCredito = valorCredito;
    }

    public int getAnosCredito() {
        return anosCredito;
    }

    public void setAnosCredito(int anosCredito) {
        this.anosCredito = anosCredito;
    }

    public String getStringValorVivienda() {
        return stringValorVivienda;
    }

    public String getStringIngMensuales() {
        return stringIngMensuales;
    }

    public String getStringValorCredito() {
        return stringValorCredito;
    }

    public String formatStrig(long value) {
        String valReturn = "$" + this.formatter.format(value).toString();
        valReturn = valReturn.replace(",", ".");
        return valReturn;
    }

    public int formatStrig(String value) {
        String aux = value.replace("$", "");
        String aux2 = aux.replace(".", "");
        return Integer.valueOf(aux2);
    }

    public void eventValorVivienda(SlideEndEvent event) {
        this.stringValorVivienda = this.formatStrig(event.getValue());
    }

    public void eventIngMensuales(SlideEndEvent event) {
        this.stringIngMensuales = this.formatStrig(event.getValue());

    }

    public void eventValorCredito(SlideEndEvent event) {
        this.stringValorCredito = this.formatStrig(event.getValue());
    }

    public String getMonedaCredito() {
        return monedaCredito;
    }

    public void setMonedaCredito(String monedaCredito) {
        this.monedaCredito = monedaCredito;
    }

    public String getGobierno() {
        return gobierno;
    }

    public void setGobierno(String gobierno) {
        this.gobierno = gobierno;
    }

    public String getMilitares() {
        return militares;
    }

    public void setMilitares(String militares) {
        this.militares = militares;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public String getNominaBbva() {
        return nominaBbva;
    }

    public void setNominaBbva(String nominaBbva) {
        this.nominaBbva = nominaBbva;
    }

    public String getProyectoFinanciado() {
        return proyectoFinanciado;
    }

    public void setProyectoFinanciado(String proyectoFinanciado) {
        this.proyectoFinanciado = proyectoFinanciado;
    }

    public String getVivencia() {
        return vivencia;
    }

    public void setVivencia(String vivencia) {
        this.vivencia = vivencia;
    }

    public String getTipoIdentificacion() {
        return tipoIdentificacion;
    }

    public void setTipoIdentificacion(String tipoIdentificacion) {
        this.tipoIdentificacion = tipoIdentificacion;
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public Date getNacimiento() {
        return nacimiento;
    }

    public void setNacimiento(Date nacimiento) {
        this.nacimiento = nacimiento;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public Integer getFijo() {
        return fijo;
    }

    public void setFijo(Integer fijo) {
        this.fijo = fijo;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public String getActividadEco() {
        return actividadEco;
    }

    public void setActividadEco(String actividadEco) {
        this.actividadEco = actividadEco;
    }

    public String getOcupacion() {
        return ocupacion;
    }

    public void setOcupacion(String ocupacion) {
        this.ocupacion = ocupacion;
    }

    public String getNueva() {
        return nueva;
    }

    public void setNueva(String nueva) {
        this.nueva = nueva;
    }

    public int getPaso() {
        return paso;
    }

    public void setPaso(int paso) {
        this.paso = paso;
    }

    public int getValorCuota() {
        return valorCuota;
    }

    public void setValorCuota(int valorCuota) {
        this.valorCuota = valorCuota;
    }

    public String getStringValorCuota() {
        return stringValorCuota;
    }

    public void setStringValorCuota(String stringValorCuota) {
        this.stringValorCuota = stringValorCuota;
    }

    public String getActividadEconomica() {
        return actividadEconomica;
    }

    public void setActividadEconomica(String actividadEconomica) {
        this.actividadEconomica = actividadEconomica;
    }

    public LinkedHashMap<String, Object> getOptionsActividadEconomica() {
        return optionsActividadEconomica;
    }

    public void setOptionsActividadEconomica(LinkedHashMap<String, Object> optionsActividadEconomica) {
        this.optionsActividadEconomica = optionsActividadEconomica;
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public LinkedHashMap<String, Object> getOptionsOcupacion() {
        return optionsOcupacion;
    }

    public void setOptionsOcupacion(LinkedHashMap<String, Object> optionsOcupacion) {
        this.optionsOcupacion = optionsOcupacion;
    }

    public LinkedHashMap<String, Object> getOptionsPais() {
        return optionsPais;
    }

    public void setOptionsPais(LinkedHashMap<String, Object> optionsPais) {
        this.optionsPais = optionsPais;
    }

    public String getResidesColombia() {
        return residesColombia;
    }

    public void setResidesColombia(String residesColombia) {
        this.residesColombia = residesColombia;
    }

    public LinkedHashMap<String, Object> getOptionsDepartamento() {
        return optionsDepartamento;
    }

    public void setOptionsDepartamento(LinkedHashMap<String, Object> optionsDepartamento) {
        this.optionsDepartamento = optionsDepartamento;
    }

    public String getPaisResidencia() {
        return paisResidencia;
    }

    public void setPaisResidencia(String paisResidencia) {
        this.paisResidencia = paisResidencia;
    }

    public String getDepartamentoResidencia() {
        return departamentoResidencia;
    }

    public void setDepartamentoResidencia(String departamentoResidencia) {
        this.departamentoResidencia = departamentoResidencia;
    }

    public LinkedHashMap<String, Object> getOptionsDepartamentoResidente() {
        return optionsDepartamentoResidente;
    }

    public void setOptionsDepartamentoResidente(LinkedHashMap<String, Object> optionsDepartamentoResidente) {
        this.optionsDepartamentoResidente = optionsDepartamentoResidente;
    }

    public LinkedHashMap<String, Object> getOptionsCiudadOficina() {
        return optionsCiudadOficina;
    }

    public void setOptionsCiudadOficina(LinkedHashMap<String, Object> optionsCiudadOficina) {
        this.optionsCiudadOficina = optionsCiudadOficina;
    }

    public String getCiudadOficina() {
        return ciudadOficina;
    }

    public void setCiudadOficina(String ciudadOficina) {
        this.ciudadOficina = ciudadOficina;
    }

    public LinkedHashMap<String, Object> getOptionsZonaOficina() {
        return optionsZonaOficina;
    }

    public void setOptionsZonaOficina(LinkedHashMap<String, Object> optionsZonaOficina) {
        this.optionsZonaOficina = optionsZonaOficina;
    }

    public String getZonaOficina() {
        return zonaOficina;
    }

    public void setZonaOficina(String zonaOficina) {
        this.zonaOficina = zonaOficina;
    }

    public ArrayList<Oficina> getOficinas() {
        return oficinas;
    }

    public void setOficinas(ArrayList<Oficina> oficinas) {
        this.oficinas = oficinas;
    }

    public Oficina getOficina() {
        return oficina;
    }

    public void setOficina(Oficina oficina) {
        this.oficina = oficina;

    }

    public String getMensageNoRecords() {
        return mensageNoRecords;
    }

    public void setMensageNoRecords(String mensageNoRecords) {
        this.mensageNoRecords = mensageNoRecords;
    }

    public ArrayList<Cuota> getCuotas() {
        return cuotas;
    }

    public void setCuotas(ArrayList<Cuota> cuotas) {
        this.cuotas = cuotas;
    }

    public Date getFechaContacto() {
        return fechaContacto;
    }

    public void setFechaContacto(Date fechaContacto) {
        this.fechaContacto = fechaContacto;
    }

    public String getHorarioContacto() {
        return horarioContacto;
    }

    public void setHorarioContacto(String horarioContacto) {
        this.horarioContacto = horarioContacto;
    }

    public String getTelefonoContacto() {
        return telefonoContacto;
    }

    public void setTelefonoContacto(String telefonoContacto) {
        this.telefonoContacto = telefonoContacto;
    }

    @Override
    public String toString() {
        return " " + this.quiero
                + " " + this.con
                + " " + this.valorVivienda
                + " " + this.ingMensuales
                + " " + this.valorCredito
                + " " + this.anosCredito
                + " " + this.gobierno
                + " " + this.militares
                + " " + this.nueva
                + " " + this.nominaBbva
                + " " + this.proyectoFinanciado
                + " " + this.vivencia
                + " " + this.porcentajeCompra
                + " " + this.tipoIdentificacion
                + " " + this.identificacion
                + " " + this.nombres
                + " " + this.apellidos
                + " " + this.nacimiento
                + " " + this.correo
                + " " + this.fijo
                + " " + this.celular
                + " " + this.actividadEconomica
                + " " + this.ocupacion
                + " " + this.pais
                + " " + this.residesColombia
                + " " + this.paisResidencia
                + " " + this.departamentoResidencia
                + " " + this.ciudadOficina
                + " " + this.zonaOficina
                + " " + this.oficina;


    }


}
