/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.bbva.portalvivienda.publico.service;

import co.com.bbva.portalvivienda.publico.SimuladorPasoAPaso;
import co.com.bbva.portalvivienda.publico.dto.DTOIntLoanSimulation;
import org.springframework.stereotype.Service;
import org.springframework.webflow.execution.RequestContext;

/**
 *
 * @author alejandro.daza
 */
@Service
public interface LoanSimulationService {

    public SimuladorPasoAPaso simulate(SimuladorPasoAPaso simulation, RequestContext context) throws Exception;
    public SimuladorPasoAPaso simulate2 (SimuladorPasoAPaso simulation, RequestContext context) throws Exception;
    public DTOIntLoanSimulation loanSimulate (DTOIntLoanSimulation dtoLoanSimulation) throws Exception;
    public DTOIntLoanSimulation completeLoanSimulation (DTOIntLoanSimulation dtoLoanSimulation) throws Exception;
}
