
/**
 * 
 * Solo permitir numeros
 */


function valida(e) {
    tecla = (document.all) ? e.keyCode : e.which;

    //Tecla de retroceso para borrar, siempre la permite
    if (tecla == 8) {
        return true;
    }

    // Patron de entrada, en este caso solo acepta numeros
    patron = /[0-9]/;
    tecla_final = String.fromCharCode(tecla);
    return patron.test(tecla_final);
}

/*
 * 
 * 
 * */
var step_movil = document.getElementById('bs-wizard-step-movil');
/*
 * Pasos simulador
 * 
 * */

var step_one = document.getElementById("step-one-complete");
var step_two = document.getElementById("step-two-complete");
var step_three = document.getElementById("step-three-complete");
var step_four = document.getElementById("step-four-complete");

/**
 * Simulador paso a paso
 */


/*
 * Divs pasos
 * */
var div_paso_uno = document.getElementById('contenedor-paso-uno');
var div_paso_dos = document.getElementById('contenedor-paso-dos');
var div_paso_tres = document.getElementById('contenedor-paso-tres');
var div_paso_cuatro = document.getElementById('contenedor-paso-cuatro');

/*
 * Adorno gris
 * 
 * */

var adorno_gris = document.getElementById('adorno-gris');

/*
 * 
 * Funcion par cambiar el paso
 */

function stepAction(div_currently, div_chage, step) {

    div_currently.style.display = 'none';
    div_chage.style.display = 'block';


    if (step_simulator == 1) {
        adorno_gris.style.display = 'none';
    } else {
        adorno_gris.style.display = 'block';
    }

    step_one.style.display = 'none';
    step_two.style.display = 'none';
    step_three.style.display = 'none';
    step_four.style.display = 'none';

    step.style.display = 'block';

    switch (step_simulator) {
        case 1:
            step_movil.innerHTML = 'Datos iniciales';
            break;
        case 2:
            step_movil.innerHTML = 'Detalle crédito';
            break;
        case 3:
            step_movil.innerHTML = 'Datos del solicitante';
            break;
        case 4:
            step_movil.innerHTML = 'Resumen de tu simulación';
            break;
    }

    window.scrollTo(0, 0);

}

function changeUpStep() {
    console.log("evento de click Simulacion");
    var div_currently = '';
    var div_chage = '';
    var step;
    
    console.log("step del simulador => " + step_simulator );
    
    switch (step_simulator) {
        case 1:
            step_simulator = 2;
            div_currently = div_paso_uno;
            div_chage = div_paso_dos;
            step = step_two;
            break;
        case 2:
            step_simulator = 3;
            div_currently = div_paso_dos;
            div_chage = div_paso_tres;
            step = step_three;
            break;
        case 3:
            step_simulator = 4;
            div_currently = div_paso_tres;
            div_chage = div_paso_cuatro;
            step = step_four;
            break;
    }
    stepAction(div_currently, div_chage, step);
}

function changeDownStep() {

    var div_currently = '';
    var div_chage = '';
    var step;

    switch (step_simulator) {
        case 2:
            step_simulator = 1;
            div_currently = div_paso_dos;
            div_chage = div_paso_uno;
            step = step_one;
            break;
        case 3:
            step_simulator = 2;
            div_currently = div_paso_tres;
            div_chage = div_paso_dos;
            step = step_two;
            break;
        case 4:
            step_simulator = 3;
            div_currently = div_paso_cuatro;
            div_chage = div_paso_tres;
            step = step_three
            break;
    }

    stepAction(div_currently, div_chage, step);

}

/*
 * Habilitar componenetes ocultos paso 3
 * 
 * 
 * */
var div_botones_mostrar = document.getElementById('botones-mostrar-todo-simulador');
var div_display_ajax = document.getElementById('display-ajax');
var div_componentes_ocultos = document.getElementById('componentes-ocultos');

function terminarHabilitaciondeBotones() {
    div_display_ajax.style.display = 'none';
    div_componentes_ocultos.style.display = 'block';
}

function habilitarBotonesOcultos() {
    div_botones_mostrar.style.display = 'none';
    div_display_ajax.style.display = 'block';
    setTimeout('terminarHabilitaciondeBotones()', 2000);
}

function updateSliderLabel() {
    alert('Hola Mundo');
}