
package com.bbva.czic.cifin.facade.v01.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para questionnaire complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="questionnaire"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="person" type="{urn:com:bbva:czic:cifin:facade:v01:dto}person" minOccurs="0"/&gt;
 *         &lt;element name="detailQuestionnaire" type="{urn:com:bbva:czic:cifin:facade:v01:dto}questionsQuestionnaire" minOccurs="0"/&gt;
 *         &lt;element name="traceInfo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "questionnaire", propOrder = {
    "person",
    "detailQuestionnaire",
    "traceInfo"
})
public class Questionnaire {

    protected Person person;
    protected QuestionsQuestionnaire detailQuestionnaire;
    protected String traceInfo;

    /**
     * Obtiene el valor de la propiedad person.
     * 
     * @return
     *     possible object is
     *     {@link Person }
     *     
     */
    public Person getPerson() {
        return person;
    }

    /**
     * Define el valor de la propiedad person.
     * 
     * @param value
     *     allowed object is
     *     {@link Person }
     *     
     */
    public void setPerson(Person value) {
        this.person = value;
    }

    /**
     * Obtiene el valor de la propiedad detailQuestionnaire.
     * 
     * @return
     *     possible object is
     *     {@link QuestionsQuestionnaire }
     *     
     */
    public QuestionsQuestionnaire getDetailQuestionnaire() {
        return detailQuestionnaire;
    }

    /**
     * Define el valor de la propiedad detailQuestionnaire.
     * 
     * @param value
     *     allowed object is
     *     {@link QuestionsQuestionnaire }
     *     
     */
    public void setDetailQuestionnaire(QuestionsQuestionnaire value) {
        this.detailQuestionnaire = value;
    }

    /**
     * Obtiene el valor de la propiedad traceInfo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTraceInfo() {
        return traceInfo;
    }

    /**
     * Define el valor de la propiedad traceInfo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTraceInfo(String value) {
        this.traceInfo = value;
    }

}
