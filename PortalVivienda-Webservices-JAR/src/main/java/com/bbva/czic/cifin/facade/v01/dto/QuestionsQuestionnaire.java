
package com.bbva.czic.cifin.facade.v01.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Clase Java para questionsQuestionnaire complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="questionsQuestionnaire"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="processAnswer" type="{urn:com:bbva:czic:cifin:facade:v01:dto}processAnswer" minOccurs="0"/&gt;
 *         &lt;element name="consultTraceList" type="{urn:com:bbva:czic:cifin:facade:v01:dto}consultTrace" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="questionList" type="{urn:com:bbva:czic:cifin:facade:v01:dto}Question" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="sequenceQuestionnaire" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="cifinPassword" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="descriptionQuestionnaire" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="code" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "questionsQuestionnaire", propOrder = {
    "processAnswer",
    "consultTraceList",
    "questionList",
    "sequenceQuestionnaire",
    "cifinPassword",
    "descriptionQuestionnaire",
    "code"
})
public class QuestionsQuestionnaire {

    protected ProcessAnswer processAnswer;
    @XmlElement(nillable = true)
    protected List<ConsultTrace> consultTraceList;
    @XmlElement(nillable = true)
    protected List<Question> questionList;
    protected String sequenceQuestionnaire;
    protected String cifinPassword;
    protected String descriptionQuestionnaire;
    protected int code;

    /**
     * Obtiene el valor de la propiedad processAnswer.
     * 
     * @return
     *     possible object is
     *     {@link ProcessAnswer }
     *     
     */
    public ProcessAnswer getProcessAnswer() {
        return processAnswer;
    }

    /**
     * Define el valor de la propiedad processAnswer.
     * 
     * @param value
     *     allowed object is
     *     {@link ProcessAnswer }
     *     
     */
    public void setProcessAnswer(ProcessAnswer value) {
        this.processAnswer = value;
    }

    /**
     * Gets the value of the consultTraceList property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the consultTraceList property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getConsultTraceList().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ConsultTrace }
     * 
     * 
     */
    public List<ConsultTrace> getConsultTraceList() {
        if (consultTraceList == null) {
            consultTraceList = new ArrayList<ConsultTrace>();
        }
        return this.consultTraceList;
    }

    /**
     * Gets the value of the questionList property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the questionList property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getQuestionList().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Question }
     * 
     * 
     */
    public List<Question> getQuestionList() {
        if (questionList == null) {
            questionList = new ArrayList<Question>();
        }
        return this.questionList;
    }

    /**
     * Obtiene el valor de la propiedad sequenceQuestionnaire.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSequenceQuestionnaire() {
        return sequenceQuestionnaire;
    }

    /**
     * Define el valor de la propiedad sequenceQuestionnaire.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSequenceQuestionnaire(String value) {
        this.sequenceQuestionnaire = value;
    }

    /**
     * Obtiene el valor de la propiedad cifinPassword.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCifinPassword() {
        return cifinPassword;
    }

    /**
     * Define el valor de la propiedad cifinPassword.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCifinPassword(String value) {
        this.cifinPassword = value;
    }

    /**
     * Obtiene el valor de la propiedad descriptionQuestionnaire.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescriptionQuestionnaire() {
        return descriptionQuestionnaire;
    }

    /**
     * Define el valor de la propiedad descriptionQuestionnaire.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescriptionQuestionnaire(String value) {
        this.descriptionQuestionnaire = value;
    }

    /**
     * Obtiene el valor de la propiedad code.
     * 
     */
    public int getCode() {
        return code;
    }

    /**
     * Define el valor de la propiedad code.
     * 
     */
    public void setCode(int value) {
        this.code = value;
    }

}
