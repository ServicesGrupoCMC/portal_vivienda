
package com.bbva.czic.cifin.facade.v01.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para answer complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="answer"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="sequenceQuestion" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="sequenceAnswer" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "answer", propOrder = {
    "sequenceQuestion",
    "sequenceAnswer"
})
public class Answer {

    protected int sequenceQuestion;
    protected int sequenceAnswer;

    /**
     * Obtiene el valor de la propiedad sequenceQuestion.
     * 
     */
    public int getSequenceQuestion() {
        return sequenceQuestion;
    }

    /**
     * Define el valor de la propiedad sequenceQuestion.
     * 
     */
    public void setSequenceQuestion(int value) {
        this.sequenceQuestion = value;
    }

    /**
     * Obtiene el valor de la propiedad sequenceAnswer.
     * 
     */
    public int getSequenceAnswer() {
        return sequenceAnswer;
    }

    /**
     * Define el valor de la propiedad sequenceAnswer.
     * 
     */
    public void setSequenceAnswer(int value) {
        this.sequenceAnswer = value;
    }

}
