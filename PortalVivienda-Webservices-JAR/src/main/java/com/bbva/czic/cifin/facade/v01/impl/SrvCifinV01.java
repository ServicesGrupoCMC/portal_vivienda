/**
 * Created by Apache CXF WadlToJava code generator
**/
package com.bbva.czic.cifin.facade.v01.impl;

import com.bbva.czic.cifin.facade.v01.dto.QuestionnaireAnswer;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;

@Path("/V00")
public interface SrvCifinV01 {

    @POST
    @Consumes("application/json")
    @Produces("application/json")
    //DataCifinAnswer sendQuestionnaire(QuestionnaireAnswer questionnaireanswer);
    Response sendQuestionnaire(QuestionnaireAnswer questionnaireanswer);

    @GET
    @Produces("application/json")
    @Path("/commercialInformation")
    /*DataValidador getCommertialInformation(@QueryParam("code") String code, @QueryParam("motive") String motive, @QueryParam("person_lastName") String person_lastName, @QueryParam("person_id_type") String person_id_type,
                @QueryParam("person_id_number") String person_id_number);*/
    Response getCommertialInformation(@QueryParam("code") String code, @QueryParam("motive") String motive, @QueryParam("person_lastName") String person_lastName, @QueryParam("person_id_type") String person_id_type,
                @QueryParam("person_id_number") String person_id_number);

    @GET
    @Produces("application/json")
    @Path("/confrontaQuestionnaire")
    /*DataConfronta getConfrontaQuestionnaire(@QueryParam("questionnaire_code") Integer questionnaire_code, @QueryParam("person_id_type") String person_id_type, @QueryParam("person_id_number") String person_id_number, @QueryParam("person_id_date") String person_id_date,
                @QueryParam("person_lastName") String person_lastName, @QueryParam("person_departament_code") Integer person_departament_code, @QueryParam("person_city_code") Integer person_city_code, @QueryParam("person_phone_number") String person_phone_number, 
                @QueryParam("sequenceQuestionnaire") String sequenceQuestionnaire);*/
    Response getConfrontaQuestionnaire(@QueryParam("questionnaire_code") Integer questionnaire_code, @QueryParam("person_id_type") String person_id_type, @QueryParam("person_id_number") String person_id_number, @QueryParam("person_id_date") String person_id_date,
                @QueryParam("person_lastName") String person_lastName, @QueryParam("person_departament_code") Integer person_departament_code, @QueryParam("person_city_code") Integer person_city_code, @QueryParam("person_phone_number") String person_phone_number,
                @QueryParam("sequenceQuestionnaire") String sequenceQuestionnaire);

    @GET
    @Produces("application/json")
    @Path("/ubica")
    /*DataUbica getUblica(@QueryParam("code") String code, @QueryParam("motive") String motive, @QueryParam("person_lastName") String person_lastName, @QueryParam("person_id_type") String person_id_type,
                @QueryParam("person_id_number") String person_id_number);*/
    Response getUblica(@QueryParam("code") String code, @QueryParam("motive") String motive, @QueryParam("person_lastName") String person_lastName, @QueryParam("person_id_type") String person_id_type,
                @QueryParam("person_id_number") String person_id_number);

    @GET
    @Produces("application/json")
    @Path("/validator")
    /*DataValidador getValidador(@QueryParam("code") String code, @QueryParam("motive") String motive, @QueryParam("person_lastName") String person_lastName, @QueryParam("person_id_type") String person_id_type,
                @QueryParam("person_id_number") String person_id_number);*/
    Response getValidador(@QueryParam("code") String code, @QueryParam("motive") String motive, @QueryParam("person_lastName") String person_lastName, @QueryParam("person_id_type") String person_id_type,
                @QueryParam("person_id_number") String person_id_number);

}