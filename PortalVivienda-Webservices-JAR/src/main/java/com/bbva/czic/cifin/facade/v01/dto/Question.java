
package com.bbva.czic.cifin.facade.v01.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Clase Java para Question complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="Question"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="answerList" type="{urn:com:bbva:czic:cifin:facade:v01:dto}answerQuestion" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="questionPosition" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="questionText" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="questionSequence" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Question", propOrder = {
    "answerList",
    "questionPosition",
    "questionText",
    "questionSequence"
})
public class Question {

    @XmlElement(nillable = true)
    protected List<AnswerQuestion> answerList;
    protected int questionPosition;
    protected String questionText;
    protected int questionSequence;

    /**
     * Gets the value of the answerList property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the answerList property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAnswerList().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AnswerQuestion }
     * 
     * 
     */
    public List<AnswerQuestion> getAnswerList() {
        if (answerList == null) {
            answerList = new ArrayList<AnswerQuestion>();
        }
        return this.answerList;
    }

    /**
     * Obtiene el valor de la propiedad questionPosition.
     * 
     */
    public int getQuestionPosition() {
        return questionPosition;
    }

    /**
     * Define el valor de la propiedad questionPosition.
     * 
     */
    public void setQuestionPosition(int value) {
        this.questionPosition = value;
    }

    /**
     * Obtiene el valor de la propiedad questionText.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQuestionText() {
        return questionText;
    }

    /**
     * Define el valor de la propiedad questionText.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQuestionText(String value) {
        this.questionText = value;
    }

    /**
     * Obtiene el valor de la propiedad questionSequence.
     * 
     */
    public int getQuestionSequence() {
        return questionSequence;
    }

    /**
     * Define el valor de la propiedad questionSequence.
     * 
     */
    public void setQuestionSequence(int value) {
        this.questionSequence = value;
    }

}
