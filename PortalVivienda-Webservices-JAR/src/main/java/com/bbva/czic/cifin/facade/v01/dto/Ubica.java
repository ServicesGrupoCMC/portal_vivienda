
package com.bbva.czic.cifin.facade.v01.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para ubica complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="ubica"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="person" type="{urn:com:bbva:czic:cifin:facade:v01:dto}person" minOccurs="0"/&gt;
 *         &lt;element name="ubicaInformation" type="{urn:com:bbva:czic:cifin:facade:v01:dto}ubicaInformation" minOccurs="0"/&gt;
 *         &lt;element name="motive" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="informationCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ubica", propOrder = {
    "person",
    "ubicaInformation",
    "motive",
    "informationCode"
})
public class Ubica {

    protected Person person;
    protected UbicaInformation ubicaInformation;
    protected String motive;
    protected String informationCode;

    /**
     * Obtiene el valor de la propiedad person.
     * 
     * @return
     *     possible object is
     *     {@link Person }
     *     
     */
    public Person getPerson() {
        return person;
    }

    /**
     * Define el valor de la propiedad person.
     * 
     * @param value
     *     allowed object is
     *     {@link Person }
     *     
     */
    public void setPerson(Person value) {
        this.person = value;
    }

    /**
     * Obtiene el valor de la propiedad ubicaInformation.
     * 
     * @return
     *     possible object is
     *     {@link UbicaInformation }
     *     
     */
    public UbicaInformation getUbicaInformation() {
        return ubicaInformation;
    }

    /**
     * Define el valor de la propiedad ubicaInformation.
     * 
     * @param value
     *     allowed object is
     *     {@link UbicaInformation }
     *     
     */
    public void setUbicaInformation(UbicaInformation value) {
        this.ubicaInformation = value;
    }

    /**
     * Obtiene el valor de la propiedad motive.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMotive() {
        return motive;
    }

    /**
     * Define el valor de la propiedad motive.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMotive(String value) {
        this.motive = value;
    }

    /**
     * Obtiene el valor de la propiedad informationCode.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInformationCode() {
        return informationCode;
    }

    /**
     * Define el valor de la propiedad informationCode.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInformationCode(String value) {
        this.informationCode = value;
    }

}
