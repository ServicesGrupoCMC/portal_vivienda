
package com.bbva.czic.cifin.facade.v01.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para location complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="location"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="departmentCode" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="cityCode" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="numberPhone" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "location", propOrder = {
    "departmentCode",
    "cityCode",
    "numberPhone"
})
public class Location {

    protected int departmentCode;
    protected int cityCode;
    protected String numberPhone;

    /**
     * Obtiene el valor de la propiedad departmentCode.
     * 
     */
    public int getDepartmentCode() {
        return departmentCode;
    }

    /**
     * Define el valor de la propiedad departmentCode.
     * 
     */
    public void setDepartmentCode(int value) {
        this.departmentCode = value;
    }

    /**
     * Obtiene el valor de la propiedad cityCode.
     * 
     */
    public int getCityCode() {
        return cityCode;
    }

    /**
     * Define el valor de la propiedad cityCode.
     * 
     */
    public void setCityCode(int value) {
        this.cityCode = value;
    }

    /**
     * Obtiene el valor de la propiedad numberPhone.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumberPhone() {
        return numberPhone;
    }

    /**
     * Define el valor de la propiedad numberPhone.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumberPhone(String value) {
        this.numberPhone = value;
    }

}
