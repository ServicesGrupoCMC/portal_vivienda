
package com.bbva.czic.cifin.facade.v01.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para answerCifin complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="answerCifin"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="code" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="attempts" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="periodTimeAttempts" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="aditional" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "answerCifin", propOrder = {
    "code",
    "description",
    "attempts",
    "periodTimeAttempts",
    "aditional"
})
public class AnswerCifin {

    protected int code;
    protected String description;
    protected int attempts;
    protected int periodTimeAttempts;
    protected boolean aditional;

    /**
     * Obtiene el valor de la propiedad code.
     * 
     */
    public int getCode() {
        return code;
    }

    /**
     * Define el valor de la propiedad code.
     * 
     */
    public void setCode(int value) {
        this.code = value;
    }

    /**
     * Obtiene el valor de la propiedad description.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Define el valor de la propiedad description.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Obtiene el valor de la propiedad attempts.
     * 
     */
    public int getAttempts() {
        return attempts;
    }

    /**
     * Define el valor de la propiedad attempts.
     * 
     */
    public void setAttempts(int value) {
        this.attempts = value;
    }

    /**
     * Obtiene el valor de la propiedad periodTimeAttempts.
     * 
     */
    public int getPeriodTimeAttempts() {
        return periodTimeAttempts;
    }

    /**
     * Define el valor de la propiedad periodTimeAttempts.
     * 
     */
    public void setPeriodTimeAttempts(int value) {
        this.periodTimeAttempts = value;
    }

    /**
     * Obtiene el valor de la propiedad aditional.
     * 
     */
    public boolean isAditional() {
        return aditional;
    }

    /**
     * Define el valor de la propiedad aditional.
     * 
     */
    public void setAditional(boolean value) {
        this.aditional = value;
    }

}
