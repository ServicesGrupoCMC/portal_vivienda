
package com.bbva.czic.cifin.facade.v01.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para answerQuestion complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="answerQuestion"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="answerText" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="answerSequence" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="questionSequence" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "answerQuestion", propOrder = {
    "answerText",
    "answerSequence",
    "questionSequence"
})
public class AnswerQuestion {

    protected String answerText;
    protected int answerSequence;
    protected int questionSequence;

    /**
     * Obtiene el valor de la propiedad answerText.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAnswerText() {
        return answerText;
    }

    /**
     * Define el valor de la propiedad answerText.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAnswerText(String value) {
        this.answerText = value;
    }

    /**
     * Obtiene el valor de la propiedad answerSequence.
     * 
     */
    public int getAnswerSequence() {
        return answerSequence;
    }

    /**
     * Define el valor de la propiedad answerSequence.
     * 
     */
    public void setAnswerSequence(int value) {
        this.answerSequence = value;
    }

    /**
     * Obtiene el valor de la propiedad questionSequence.
     * 
     */
    public int getQuestionSequence() {
        return questionSequence;
    }

    /**
     * Define el valor de la propiedad questionSequence.
     * 
     */
    public void setQuestionSequence(int value) {
        this.questionSequence = value;
    }

}
