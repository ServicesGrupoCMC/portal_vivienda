
package com.bbva.czic.cifin.facade.v01.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para processAnswer complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="processAnswer"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="anotherQuestionaire" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="periorAttempts" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="attempts" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="answerDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="answerCode" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "processAnswer", propOrder = {
    "anotherQuestionaire",
    "periorAttempts",
    "attempts",
    "answerDescription",
    "answerCode"
})
public class ProcessAnswer {

    protected boolean anotherQuestionaire;
    protected int periorAttempts;
    protected String attempts;
    protected String answerDescription;
    protected int answerCode;

    /**
     * Obtiene el valor de la propiedad anotherQuestionaire.
     * 
     */
    public boolean isAnotherQuestionaire() {
        return anotherQuestionaire;
    }

    /**
     * Define el valor de la propiedad anotherQuestionaire.
     * 
     */
    public void setAnotherQuestionaire(boolean value) {
        this.anotherQuestionaire = value;
    }

    /**
     * Obtiene el valor de la propiedad periorAttempts.
     * 
     */
    public int getPeriorAttempts() {
        return periorAttempts;
    }

    /**
     * Define el valor de la propiedad periorAttempts.
     * 
     */
    public void setPeriorAttempts(int value) {
        this.periorAttempts = value;
    }

    /**
     * Obtiene el valor de la propiedad attempts.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAttempts() {
        return attempts;
    }

    /**
     * Define el valor de la propiedad attempts.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAttempts(String value) {
        this.attempts = value;
    }

    /**
     * Obtiene el valor de la propiedad answerDescription.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAnswerDescription() {
        return answerDescription;
    }

    /**
     * Define el valor de la propiedad answerDescription.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAnswerDescription(String value) {
        this.answerDescription = value;
    }

    /**
     * Obtiene el valor de la propiedad answerCode.
     * 
     */
    public int getAnswerCode() {
        return answerCode;
    }

    /**
     * Define el valor de la propiedad answerCode.
     * 
     */
    public void setAnswerCode(int value) {
        this.answerCode = value;
    }

}
