
package com.bbva.czic.cifin.facade.v01.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para cifinAnswer complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="cifinAnswer"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="code" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="sequence" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="answerCifin" type="{urn:com:bbva:czic:cifin:facade:v01:dto}answerCifin" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "cifinAnswer", propOrder = {
    "code",
    "sequence",
    "answerCifin"
})
public class CifinAnswer {

    protected int code;
    protected int sequence;
    protected AnswerCifin answerCifin;

    /**
     * Obtiene el valor de la propiedad code.
     * 
     */
    public int getCode() {
        return code;
    }

    /**
     * Define el valor de la propiedad code.
     * 
     */
    public void setCode(int value) {
        this.code = value;
    }

    /**
     * Obtiene el valor de la propiedad sequence.
     * 
     */
    public int getSequence() {
        return sequence;
    }

    /**
     * Define el valor de la propiedad sequence.
     * 
     */
    public void setSequence(int value) {
        this.sequence = value;
    }

    /**
     * Obtiene el valor de la propiedad answerCifin.
     * 
     * @return
     *     possible object is
     *     {@link AnswerCifin }
     *     
     */
    public AnswerCifin getAnswerCifin() {
        return answerCifin;
    }

    /**
     * Define el valor de la propiedad answerCifin.
     * 
     * @param value
     *     allowed object is
     *     {@link AnswerCifin }
     *     
     */
    public void setAnswerCifin(AnswerCifin value) {
        this.answerCifin = value;
    }

}
