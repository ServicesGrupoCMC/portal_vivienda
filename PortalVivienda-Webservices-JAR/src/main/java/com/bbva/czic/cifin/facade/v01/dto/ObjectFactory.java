
package com.bbva.czic.cifin.facade.v01.dto;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.bbva.czic.cifin.facade.v01.dto package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Question_QNAME = new QName("urn:com:bbva:czic:cifin:facade:v01:dto", "Question");
    private final static QName _Answer_QNAME = new QName("urn:com:bbva:czic:cifin:facade:v01:dto", "answer");
    private final static QName _AnswerCifin_QNAME = new QName("urn:com:bbva:czic:cifin:facade:v01:dto", "answerCifin");
    private final static QName _AnswerQuestion_QNAME = new QName("urn:com:bbva:czic:cifin:facade:v01:dto", "answerQuestion");
    private final static QName _CifinAnswer_QNAME = new QName("urn:com:bbva:czic:cifin:facade:v01:dto", "cifinAnswer");
    private final static QName _ConsultTrace_QNAME = new QName("urn:com:bbva:czic:cifin:facade:v01:dto", "consultTrace");
    private final static QName _DataCifinAnswer_QNAME = new QName("urn:com:bbva:czic:cifin:facade:v01:dto", "dataCifinAnswer");
    private final static QName _DataConfronta_QNAME = new QName("urn:com:bbva:czic:cifin:facade:v01:dto", "dataConfronta");
    private final static QName _DataUbica_QNAME = new QName("urn:com:bbva:czic:cifin:facade:v01:dto", "dataUbica");
    private final static QName _DataValidador_QNAME = new QName("urn:com:bbva:czic:cifin:facade:v01:dto", "dataValidador");
    private final static QName _Id_QNAME = new QName("urn:com:bbva:czic:cifin:facade:v01:dto", "id");
    private final static QName _Location_QNAME = new QName("urn:com:bbva:czic:cifin:facade:v01:dto", "location");
    private final static QName _Person_QNAME = new QName("urn:com:bbva:czic:cifin:facade:v01:dto", "person");
    private final static QName _ProcessAnswer_QNAME = new QName("urn:com:bbva:czic:cifin:facade:v01:dto", "processAnswer");
    private final static QName _Questionnaire_QNAME = new QName("urn:com:bbva:czic:cifin:facade:v01:dto", "questionnaire");
    private final static QName _QuestionnaireAnswer_QNAME = new QName("urn:com:bbva:czic:cifin:facade:v01:dto", "questionnaireAnswer");
    private final static QName _QuestionsQuestionnaire_QNAME = new QName("urn:com:bbva:czic:cifin:facade:v01:dto", "questionsQuestionnaire");
    private final static QName _Tag_QNAME = new QName("urn:com:bbva:czic:cifin:facade:v01:dto", "tag");
    private final static QName _Ubica_QNAME = new QName("urn:com:bbva:czic:cifin:facade:v01:dto", "ubica");
    private final static QName _UbicaInformation_QNAME = new QName("urn:com:bbva:czic:cifin:facade:v01:dto", "ubicaInformation");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.bbva.czic.cifin.facade.v01.dto
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Question }
     * 
     */
    public Question createQuestion() {
        return new Question();
    }

    /**
     * Create an instance of {@link Answer }
     * 
     */
    public Answer createAnswer() {
        return new Answer();
    }

    /**
     * Create an instance of {@link AnswerCifin }
     * 
     */
    public AnswerCifin createAnswerCifin() {
        return new AnswerCifin();
    }

    /**
     * Create an instance of {@link AnswerQuestion }
     * 
     */
    public AnswerQuestion createAnswerQuestion() {
        return new AnswerQuestion();
    }

    /**
     * Create an instance of {@link CifinAnswer }
     * 
     */
    public CifinAnswer createCifinAnswer() {
        return new CifinAnswer();
    }

    /**
     * Create an instance of {@link ConsultTrace }
     * 
     */
    public ConsultTrace createConsultTrace() {
        return new ConsultTrace();
    }

    /**
     * Create an instance of {@link DataCifinAnswer }
     * 
     */
    public DataCifinAnswer createDataCifinAnswer() {
        return new DataCifinAnswer();
    }

    /**
     * Create an instance of {@link DataConfronta }
     * 
     */
    public DataConfronta createDataConfronta() {
        return new DataConfronta();
    }

    /**
     * Create an instance of {@link DataUbica }
     * 
     */
    public DataUbica createDataUbica() {
        return new DataUbica();
    }

    /**
     * Create an instance of {@link DataValidador }
     * 
     */
    public DataValidador createDataValidador() {
        return new DataValidador();
    }

    /**
     * Create an instance of {@link Id }
     * 
     */
    public Id createId() {
        return new Id();
    }

    /**
     * Create an instance of {@link Location }
     * 
     */
    public Location createLocation() {
        return new Location();
    }

    /**
     * Create an instance of {@link Person }
     * 
     */
    public Person createPerson() {
        return new Person();
    }

    /**
     * Create an instance of {@link ProcessAnswer }
     * 
     */
    public ProcessAnswer createProcessAnswer() {
        return new ProcessAnswer();
    }

    /**
     * Create an instance of {@link Questionnaire }
     * 
     */
    public Questionnaire createQuestionnaire() {
        return new Questionnaire();
    }

    /**
     * Create an instance of {@link QuestionnaireAnswer }
     * 
     */
    public QuestionnaireAnswer createQuestionnaireAnswer() {
        return new QuestionnaireAnswer();
    }

    /**
     * Create an instance of {@link QuestionsQuestionnaire }
     * 
     */
    public QuestionsQuestionnaire createQuestionsQuestionnaire() {
        return new QuestionsQuestionnaire();
    }

    /**
     * Create an instance of {@link Tag }
     * 
     */
    public Tag createTag() {
        return new Tag();
    }

    /**
     * Create an instance of {@link Ubica }
     * 
     */
    public Ubica createUbica() {
        return new Ubica();
    }

    /**
     * Create an instance of {@link UbicaInformation }
     * 
     */
    public UbicaInformation createUbicaInformation() {
        return new UbicaInformation();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Question }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com:bbva:czic:cifin:facade:v01:dto", name = "Question")
    public JAXBElement<Question> createQuestion(Question value) {
        return new JAXBElement<Question>(_Question_QNAME, Question.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Answer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com:bbva:czic:cifin:facade:v01:dto", name = "answer")
    public JAXBElement<Answer> createAnswer(Answer value) {
        return new JAXBElement<Answer>(_Answer_QNAME, Answer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AnswerCifin }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com:bbva:czic:cifin:facade:v01:dto", name = "answerCifin")
    public JAXBElement<AnswerCifin> createAnswerCifin(AnswerCifin value) {
        return new JAXBElement<AnswerCifin>(_AnswerCifin_QNAME, AnswerCifin.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AnswerQuestion }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com:bbva:czic:cifin:facade:v01:dto", name = "answerQuestion")
    public JAXBElement<AnswerQuestion> createAnswerQuestion(AnswerQuestion value) {
        return new JAXBElement<AnswerQuestion>(_AnswerQuestion_QNAME, AnswerQuestion.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CifinAnswer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com:bbva:czic:cifin:facade:v01:dto", name = "cifinAnswer")
    public JAXBElement<CifinAnswer> createCifinAnswer(CifinAnswer value) {
        return new JAXBElement<CifinAnswer>(_CifinAnswer_QNAME, CifinAnswer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ConsultTrace }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com:bbva:czic:cifin:facade:v01:dto", name = "consultTrace")
    public JAXBElement<ConsultTrace> createConsultTrace(ConsultTrace value) {
        return new JAXBElement<ConsultTrace>(_ConsultTrace_QNAME, ConsultTrace.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataCifinAnswer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com:bbva:czic:cifin:facade:v01:dto", name = "dataCifinAnswer")
    public JAXBElement<DataCifinAnswer> createDataCifinAnswer(DataCifinAnswer value) {
        return new JAXBElement<DataCifinAnswer>(_DataCifinAnswer_QNAME, DataCifinAnswer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataConfronta }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com:bbva:czic:cifin:facade:v01:dto", name = "dataConfronta")
    public JAXBElement<DataConfronta> createDataConfronta(DataConfronta value) {
        return new JAXBElement<DataConfronta>(_DataConfronta_QNAME, DataConfronta.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataUbica }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com:bbva:czic:cifin:facade:v01:dto", name = "dataUbica")
    public JAXBElement<DataUbica> createDataUbica(DataUbica value) {
        return new JAXBElement<DataUbica>(_DataUbica_QNAME, DataUbica.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataValidador }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com:bbva:czic:cifin:facade:v01:dto", name = "dataValidador")
    public JAXBElement<DataValidador> createDataValidador(DataValidador value) {
        return new JAXBElement<DataValidador>(_DataValidador_QNAME, DataValidador.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Id }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com:bbva:czic:cifin:facade:v01:dto", name = "id")
    public JAXBElement<Id> createId(Id value) {
        return new JAXBElement<Id>(_Id_QNAME, Id.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Location }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com:bbva:czic:cifin:facade:v01:dto", name = "location")
    public JAXBElement<Location> createLocation(Location value) {
        return new JAXBElement<Location>(_Location_QNAME, Location.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Person }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com:bbva:czic:cifin:facade:v01:dto", name = "person")
    public JAXBElement<Person> createPerson(Person value) {
        return new JAXBElement<Person>(_Person_QNAME, Person.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ProcessAnswer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com:bbva:czic:cifin:facade:v01:dto", name = "processAnswer")
    public JAXBElement<ProcessAnswer> createProcessAnswer(ProcessAnswer value) {
        return new JAXBElement<ProcessAnswer>(_ProcessAnswer_QNAME, ProcessAnswer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Questionnaire }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com:bbva:czic:cifin:facade:v01:dto", name = "questionnaire")
    public JAXBElement<Questionnaire> createQuestionnaire(Questionnaire value) {
        return new JAXBElement<Questionnaire>(_Questionnaire_QNAME, Questionnaire.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link QuestionnaireAnswer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com:bbva:czic:cifin:facade:v01:dto", name = "questionnaireAnswer")
    public JAXBElement<QuestionnaireAnswer> createQuestionnaireAnswer(QuestionnaireAnswer value) {
        return new JAXBElement<QuestionnaireAnswer>(_QuestionnaireAnswer_QNAME, QuestionnaireAnswer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link QuestionsQuestionnaire }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com:bbva:czic:cifin:facade:v01:dto", name = "questionsQuestionnaire")
    public JAXBElement<QuestionsQuestionnaire> createQuestionsQuestionnaire(QuestionsQuestionnaire value) {
        return new JAXBElement<QuestionsQuestionnaire>(_QuestionsQuestionnaire_QNAME, QuestionsQuestionnaire.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Tag }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com:bbva:czic:cifin:facade:v01:dto", name = "tag")
    public JAXBElement<Tag> createTag(Tag value) {
        return new JAXBElement<Tag>(_Tag_QNAME, Tag.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Ubica }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com:bbva:czic:cifin:facade:v01:dto", name = "ubica")
    public JAXBElement<Ubica> createUbica(Ubica value) {
        return new JAXBElement<Ubica>(_Ubica_QNAME, Ubica.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UbicaInformation }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com:bbva:czic:cifin:facade:v01:dto", name = "ubicaInformation")
    public JAXBElement<UbicaInformation> createUbicaInformation(UbicaInformation value) {
        return new JAXBElement<UbicaInformation>(_UbicaInformation_QNAME, UbicaInformation.class, null, value);
    }

}
