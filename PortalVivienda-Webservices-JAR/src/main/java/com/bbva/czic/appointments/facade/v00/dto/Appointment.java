
package com.bbva.czic.appointments.facade.v00.dto;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para appointment complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="appointment"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *         &lt;element name="originAppointmentScheduling" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="subProduct" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *         &lt;element name="names" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="surname" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="documentType" type="{urn:com:bbva:czic:appointments:facade:v00:dto}documentType" minOccurs="0"/&gt;
 *         &lt;element name="documentNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="phone" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="cellPhone" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="countryBirth" type="{urn:com:bbva:czic:appointments:facade:v00:dto}countryBirth" minOccurs="0"/&gt;
 *         &lt;element name="countryResidence" type="{urn:com:bbva:czic:appointments:facade:v00:dto}countryResidence" minOccurs="0"/&gt;
 *         &lt;element name="stateResidence" type="{urn:com:bbva:czic:appointments:facade:v00:dto}stateResidence" minOccurs="0"/&gt;
 *         &lt;element name="cityResidence" type="{urn:com:bbva:czic:appointments:facade:v00:dto}cityResidence" minOccurs="0"/&gt;
 *         &lt;element name="dateContact" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="subject" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="schedule" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="amountToFinanced" type="{urn:com:bbva:czic:appointments:facade:v00:dto}amountToFinanced" minOccurs="0"/&gt;
 *         &lt;element name="priceOfHousing" type="{urn:com:bbva:czic:appointments:facade:v00:dto}priceOfHousing" minOccurs="0"/&gt;
 *         &lt;element name="replyScoreBuro" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "appointment", propOrder = {
    "id",
    "originAppointmentScheduling",
    "subProduct",
    "names",
    "surname",
    "documentType",
    "documentNumber",
    "email",
    "phone",
    "cellPhone",
    "countryBirth",
    "countryResidence",
    "stateResidence",
    "cityResidence",
    "dateContact",
    "subject",
    "schedule",
    "amountToFinanced",
    "priceOfHousing",
    "replyScoreBuro"
})
public class Appointment implements Serializable{

    protected Integer id;
    protected String originAppointmentScheduling;
    protected Integer subProduct;
    protected String names;
    protected String surname;
    protected DocumentType documentType;
    protected String documentNumber;
    protected String email;
    protected String phone;
    protected String cellPhone;
    protected CountryBirth countryBirth;
    protected CountryResidence countryResidence;
    protected StateResidence stateResidence;
    protected CityResidence cityResidence;
    protected String dateContact;
    protected String subject;
    protected String schedule;
    protected AmountToFinanced amountToFinanced;
    protected PriceOfHousing priceOfHousing;
    protected Integer replyScoreBuro;

    /**
     * Obtiene el valor de la propiedad id.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getId() {
        return id;
    }

    /**
     * Define el valor de la propiedad id.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setId(Integer value) {
        this.id = value;
    }

    /**
     * Obtiene el valor de la propiedad originAppointmentScheduling.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOriginAppointmentScheduling() {
        return originAppointmentScheduling;
    }

    /**
     * Define el valor de la propiedad originAppointmentScheduling.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOriginAppointmentScheduling(String value) {
        this.originAppointmentScheduling = value;
    }

    /**
     * Obtiene el valor de la propiedad subProduct.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getSubProduct() {
        return subProduct;
    }

    /**
     * Define el valor de la propiedad subProduct.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setSubProduct(Integer value) {
        this.subProduct = value;
    }

    /**
     * Obtiene el valor de la propiedad names.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNames() {
        return names;
    }

    /**
     * Define el valor de la propiedad names.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNames(String value) {
        this.names = value;
    }

    /**
     * Obtiene el valor de la propiedad surname.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSurname() {
        return surname;
    }

    /**
     * Define el valor de la propiedad surname.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSurname(String value) {
        this.surname = value;
    }

    /**
     * Obtiene el valor de la propiedad documentType.
     * 
     * @return
     *     possible object is
     *     {@link DocumentType }
     *     
     */
    public DocumentType getDocumentType() {
        return documentType;
    }

    /**
     * Define el valor de la propiedad documentType.
     * 
     * @param value
     *     allowed object is
     *     {@link DocumentType }
     *     
     */
    public void setDocumentType(DocumentType value) {
        this.documentType = value;
    }

    /**
     * Obtiene el valor de la propiedad documentNumber.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDocumentNumber() {
        return documentNumber;
    }

    /**
     * Define el valor de la propiedad documentNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDocumentNumber(String value) {
        this.documentNumber = value;
    }

    /**
     * Obtiene el valor de la propiedad email.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmail() {
        return email;
    }

    /**
     * Define el valor de la propiedad email.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmail(String value) {
        this.email = value;
    }

    /**
     * Obtiene el valor de la propiedad phone.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPhone() {
        return phone;
    }

    /**
     * Define el valor de la propiedad phone.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPhone(String value) {
        this.phone = value;
    }

    /**
     * Obtiene el valor de la propiedad cellPhone.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCellPhone() {
        return cellPhone;
    }

    /**
     * Define el valor de la propiedad cellPhone.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCellPhone(String value) {
        this.cellPhone = value;
    }

    /**
     * Obtiene el valor de la propiedad countryBirth.
     * 
     * @return
     *     possible object is
     *     {@link CountryBirth }
     *     
     */
    public CountryBirth getCountryBirth() {
        return countryBirth;
    }

    /**
     * Define el valor de la propiedad countryBirth.
     * 
     * @param value
     *     allowed object is
     *     {@link CountryBirth }
     *     
     */
    public void setCountryBirth(CountryBirth value) {
        this.countryBirth = value;
    }

    /**
     * Obtiene el valor de la propiedad countryResidence.
     * 
     * @return
     *     possible object is
     *     {@link CountryResidence }
     *     
     */
    public CountryResidence getCountryResidence() {
        return countryResidence;
    }

    /**
     * Define el valor de la propiedad countryResidence.
     * 
     * @param value
     *     allowed object is
     *     {@link CountryResidence }
     *     
     */
    public void setCountryResidence(CountryResidence value) {
        this.countryResidence = value;
    }

    /**
     * Obtiene el valor de la propiedad stateResidence.
     * 
     * @return
     *     possible object is
     *     {@link StateResidence }
     *     
     */
    public StateResidence getStateResidence() {
        return stateResidence;
    }

    /**
     * Define el valor de la propiedad stateResidence.
     * 
     * @param value
     *     allowed object is
     *     {@link StateResidence }
     *     
     */
    public void setStateResidence(StateResidence value) {
        this.stateResidence = value;
    }

    /**
     * Obtiene el valor de la propiedad cityResidence.
     * 
     * @return
     *     possible object is
     *     {@link CityResidence }
     *     
     */
    public CityResidence getCityResidence() {
        return cityResidence;
    }

    /**
     * Define el valor de la propiedad cityResidence.
     * 
     * @param value
     *     allowed object is
     *     {@link CityResidence }
     *     
     */
    public void setCityResidence(CityResidence value) {
        this.cityResidence = value;
    }

    /**
     * Obtiene el valor de la propiedad dateContact.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDateContact() {
        return dateContact;
    }

    /**
     * Define el valor de la propiedad dateContact.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDateContact(String value) {
        this.dateContact = value;
    }

    /**
     * Obtiene el valor de la propiedad subject.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubject() {
        return subject;
    }

    /**
     * Define el valor de la propiedad subject.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubject(String value) {
        this.subject = value;
    }

    /**
     * Obtiene el valor de la propiedad schedule.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSchedule() {
        return schedule;
    }

    /**
     * Define el valor de la propiedad schedule.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSchedule(String value) {
        this.schedule = value;
    }

    /**
     * Obtiene el valor de la propiedad amountToFinanced.
     * 
     * @return
     *     possible object is
     *     {@link AmountToFinanced }
     *     
     */
    public AmountToFinanced getAmountToFinanced() {
        return amountToFinanced;
    }

    /**
     * Define el valor de la propiedad amountToFinanced.
     * 
     * @param value
     *     allowed object is
     *     {@link AmountToFinanced }
     *     
     */
    public void setAmountToFinanced(AmountToFinanced value) {
        this.amountToFinanced = value;
    }

    /**
     * Obtiene el valor de la propiedad priceOfHousing.
     * 
     * @return
     *     possible object is
     *     {@link PriceOfHousing }
     *     
     */
    public PriceOfHousing getPriceOfHousing() {
        return priceOfHousing;
    }

    /**
     * Define el valor de la propiedad priceOfHousing.
     * 
     * @param value
     *     allowed object is
     *     {@link PriceOfHousing }
     *     
     */
    public void setPriceOfHousing(PriceOfHousing value) {
        this.priceOfHousing = value;
    }

    /**
     * Obtiene el valor de la propiedad replyScoreBuro.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getReplyScoreBuro() {
        return replyScoreBuro;
    }

    /**
     * Define el valor de la propiedad replyScoreBuro.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setReplyScoreBuro(Integer value) {
        this.replyScoreBuro = value;
    }

}
