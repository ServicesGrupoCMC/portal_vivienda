/**
 * Created by Apache CXF WadlToJava code generator
**/
package com.bbva.czic.appointments.facade.v00;

import com.bbva.czic.appointments.facade.v00.dto.Appointment;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

@Path("/V00")
public interface SrvAppointmentsV00 {

    @POST
    @Consumes("application/json")
    @Produces("application/json")
    Response createAppointment(Appointment appointment);

}