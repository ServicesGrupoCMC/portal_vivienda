
package com.bbva.czic.appointments.facade.v00.dto;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.bbva.czic.appointments.facade.v00.dto package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _AmountToFinanced_QNAME = new QName("urn:com:bbva:czic:appointments:facade:v00:dto", "amountToFinanced");
    private final static QName _Appointment_QNAME = new QName("urn:com:bbva:czic:appointments:facade:v00:dto", "appointment");
    private final static QName _CityResidence_QNAME = new QName("urn:com:bbva:czic:appointments:facade:v00:dto", "cityResidence");
    private final static QName _CountryBirth_QNAME = new QName("urn:com:bbva:czic:appointments:facade:v00:dto", "countryBirth");
    private final static QName _CountryResidence_QNAME = new QName("urn:com:bbva:czic:appointments:facade:v00:dto", "countryResidence");
    private final static QName _DocumentType_QNAME = new QName("urn:com:bbva:czic:appointments:facade:v00:dto", "documentType");
    private final static QName _PriceOfHousing_QNAME = new QName("urn:com:bbva:czic:appointments:facade:v00:dto", "priceOfHousing");
    private final static QName _StateResidence_QNAME = new QName("urn:com:bbva:czic:appointments:facade:v00:dto", "stateResidence");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.bbva.czic.appointments.facade.v00.dto
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link AmountToFinanced }
     * 
     */
    public AmountToFinanced createAmountToFinanced() {
        return new AmountToFinanced();
    }

    /**
     * Create an instance of {@link Appointment }
     * 
     */
    public Appointment createAppointment() {
        return new Appointment();
    }

    /**
     * Create an instance of {@link CityResidence }
     * 
     */
    public CityResidence createCityResidence() {
        return new CityResidence();
    }

    /**
     * Create an instance of {@link CountryBirth }
     * 
     */
    public CountryBirth createCountryBirth() {
        return new CountryBirth();
    }

    /**
     * Create an instance of {@link CountryResidence }
     * 
     */
    public CountryResidence createCountryResidence() {
        return new CountryResidence();
    }

    /**
     * Create an instance of {@link DocumentType }
     * 
     */
    public DocumentType createDocumentType() {
        return new DocumentType();
    }

    /**
     * Create an instance of {@link PriceOfHousing }
     * 
     */
    public PriceOfHousing createPriceOfHousing() {
        return new PriceOfHousing();
    }

    /**
     * Create an instance of {@link StateResidence }
     * 
     */
    public StateResidence createStateResidence() {
        return new StateResidence();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AmountToFinanced }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com:bbva:czic:appointments:facade:v00:dto", name = "amountToFinanced")
    public JAXBElement<AmountToFinanced> createAmountToFinanced(AmountToFinanced value) {
        return new JAXBElement<AmountToFinanced>(_AmountToFinanced_QNAME, AmountToFinanced.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Appointment }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com:bbva:czic:appointments:facade:v00:dto", name = "appointment")
    public JAXBElement<Appointment> createAppointment(Appointment value) {
        return new JAXBElement<Appointment>(_Appointment_QNAME, Appointment.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CityResidence }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com:bbva:czic:appointments:facade:v00:dto", name = "cityResidence")
    public JAXBElement<CityResidence> createCityResidence(CityResidence value) {
        return new JAXBElement<CityResidence>(_CityResidence_QNAME, CityResidence.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CountryBirth }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com:bbva:czic:appointments:facade:v00:dto", name = "countryBirth")
    public JAXBElement<CountryBirth> createCountryBirth(CountryBirth value) {
        return new JAXBElement<CountryBirth>(_CountryBirth_QNAME, CountryBirth.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CountryResidence }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com:bbva:czic:appointments:facade:v00:dto", name = "countryResidence")
    public JAXBElement<CountryResidence> createCountryResidence(CountryResidence value) {
        return new JAXBElement<CountryResidence>(_CountryResidence_QNAME, CountryResidence.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DocumentType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com:bbva:czic:appointments:facade:v00:dto", name = "documentType")
    public JAXBElement<DocumentType> createDocumentType(DocumentType value) {
        return new JAXBElement<DocumentType>(_DocumentType_QNAME, DocumentType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PriceOfHousing }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com:bbva:czic:appointments:facade:v00:dto", name = "priceOfHousing")
    public JAXBElement<PriceOfHousing> createPriceOfHousing(PriceOfHousing value) {
        return new JAXBElement<PriceOfHousing>(_PriceOfHousing_QNAME, PriceOfHousing.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link StateResidence }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com:bbva:czic:appointments:facade:v00:dto", name = "stateResidence")
    public JAXBElement<StateResidence> createStateResidence(StateResidence value) {
        return new JAXBElement<StateResidence>(_StateResidence_QNAME, StateResidence.class, null, value);
    }

}
