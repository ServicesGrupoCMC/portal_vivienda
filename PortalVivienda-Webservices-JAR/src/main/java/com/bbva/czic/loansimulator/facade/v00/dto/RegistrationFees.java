
package com.bbva.czic.loansimulator.facade.v00.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para registrationFees complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="registrationFees"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{urn:com:bbva:czic:loansimulator:facade:v00:dto}registrationRight" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:com:bbva:czic:loansimulator:facade:v00:dto}certificateOfFreedom" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:com:bbva:czic:loansimulator:facade:v00:dto}benefitTax" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:com:bbva:czic:loansimulator:facade:v00:dto}totalExpensesRegistration" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "registrationFees", propOrder = {
    "registrationRight",
    "certificateOfFreedom",
    "benefitTax",
    "totalExpensesRegistration"
})
public class RegistrationFees {

    @XmlElement(namespace = "urn:com:bbva:czic:loansimulator:facade:v00:dto")
    protected RegistrationRight registrationRight;
    @XmlElement(namespace = "urn:com:bbva:czic:loansimulator:facade:v00:dto")
    protected CertificateOfFreedom certificateOfFreedom;
    @XmlElement(namespace = "urn:com:bbva:czic:loansimulator:facade:v00:dto")
    protected BenefitTax benefitTax;
    @XmlElement(namespace = "urn:com:bbva:czic:loansimulator:facade:v00:dto")
    protected TotalExpensesRegistration totalExpensesRegistration;

    /**
     * Obtiene el valor de la propiedad registrationRight.
     * 
     * @return
     *     possible object is
     *     {@link RegistrationRight }
     *     
     */
    public RegistrationRight getRegistrationRight() {
        return registrationRight;
    }

    /**
     * Define el valor de la propiedad registrationRight.
     * 
     * @param value
     *     allowed object is
     *     {@link RegistrationRight }
     *     
     */
    public void setRegistrationRight(RegistrationRight value) {
        this.registrationRight = value;
    }

    /**
     * Obtiene el valor de la propiedad certificateOfFreedom.
     * 
     * @return
     *     possible object is
     *     {@link CertificateOfFreedom }
     *     
     */
    public CertificateOfFreedom getCertificateOfFreedom() {
        return certificateOfFreedom;
    }

    /**
     * Define el valor de la propiedad certificateOfFreedom.
     * 
     * @param value
     *     allowed object is
     *     {@link CertificateOfFreedom }
     *     
     */
    public void setCertificateOfFreedom(CertificateOfFreedom value) {
        this.certificateOfFreedom = value;
    }

    /**
     * Obtiene el valor de la propiedad benefitTax.
     * 
     * @return
     *     possible object is
     *     {@link BenefitTax }
     *     
     */
    public BenefitTax getBenefitTax() {
        return benefitTax;
    }

    /**
     * Define el valor de la propiedad benefitTax.
     * 
     * @param value
     *     allowed object is
     *     {@link BenefitTax }
     *     
     */
    public void setBenefitTax(BenefitTax value) {
        this.benefitTax = value;
    }

    /**
     * Obtiene el valor de la propiedad totalExpensesRegistration.
     * 
     * @return
     *     possible object is
     *     {@link TotalExpensesRegistration }
     *     
     */
    public TotalExpensesRegistration getTotalExpensesRegistration() {
        return totalExpensesRegistration;
    }

    /**
     * Define el valor de la propiedad totalExpensesRegistration.
     * 
     * @param value
     *     allowed object is
     *     {@link TotalExpensesRegistration }
     *     
     */
    public void setTotalExpensesRegistration(TotalExpensesRegistration value) {
        this.totalExpensesRegistration = value;
    }

}
