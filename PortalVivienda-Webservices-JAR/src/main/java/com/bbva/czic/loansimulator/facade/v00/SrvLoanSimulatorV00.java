/**
 * Created by Apache CXF WadlToJava code generator
**/
package com.bbva.czic.loansimulator.facade.v00;

import com.bbva.czic.loansimulator.facade.v00.dto.LoanSimulation;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;

@Path("/V00")
public interface SrvLoanSimulatorV00 {

    @POST
    @Consumes("application/json")
    @Produces("application/json")
    //DataLoanSimulation simulateMortgageLoan(@QueryParam("$expands") @DefaultValue("01") String $expands, LoanSimulation loansimulation);
    Response simulateMortgageLoan(@QueryParam("$expands") @DefaultValue("01") String $expands, LoanSimulation loansimulation);

}