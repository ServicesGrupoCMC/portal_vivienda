
package com.bbva.czic.loansimulator.facade.v00.dto;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.bbva.czic.loansimulator.facade.v00.dto package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _AdditionalCosts_QNAME = new QName("urn:com:bbva:czic:loansimulator:facade:v00:dto", "additionalCosts");
    private final static QName _AmortizationTable_QNAME = new QName("urn:com:bbva:czic:loansimulator:facade:v00:dto", "amortizationTable");
    private final static QName _AmountToFinanced_QNAME = new QName("urn:com:bbva:czic:loansimulator:facade:v00:dto", "amountToFinanced");
    private final static QName _BalanceAmount_QNAME = new QName("urn:com:bbva:czic:loansimulator:facade:v00:dto", "balanceAmount");
    private final static QName _BenefitTax_QNAME = new QName("urn:com:bbva:czic:loansimulator:facade:v00:dto", "benefitTax");
    private final static QName _Buyer_QNAME = new QName("urn:com:bbva:czic:loansimulator:facade:v00:dto", "buyer");
    private final static QName _Capital_QNAME = new QName("urn:com:bbva:czic:loansimulator:facade:v00:dto", "capital");
    private final static QName _CertificateOfFreedom_QNAME = new QName("urn:com:bbva:czic:loansimulator:facade:v00:dto", "certificateOfFreedom");
    private final static QName _CountryResidence_QNAME = new QName("urn:com:bbva:czic:loansimulator:facade:v00:dto", "countryResidence");
    private final static QName _CoverageFee_QNAME = new QName("urn:com:bbva:czic:loansimulator:facade:v00:dto", "coverageFee");
    private final static QName _DataLoanSimulation_QNAME = new QName("urn:com:bbva:czic:loansimulator:facade:v00:dto", "dataLoanSimulation");
    private final static QName _DocumentType_QNAME = new QName("urn:com:bbva:czic:loansimulator:facade:v00:dto", "documentType");
    private final static QName _Interests_QNAME = new QName("urn:com:bbva:czic:loansimulator:facade:v00:dto", "interests");
    private final static QName _LifeInsuranceCost_QNAME = new QName("urn:com:bbva:czic:loansimulator:facade:v00:dto", "lifeInsuranceCost");
    private final static QName _LifeInsuranceValue_QNAME = new QName("urn:com:bbva:czic:loansimulator:facade:v00:dto", "lifeInsuranceValue");
    private final static QName _LoanSimulation_QNAME = new QName("urn:com:bbva:czic:loansimulator:facade:v00:dto", "loanSimulation");
    private final static QName _MonthlyAmortization_QNAME = new QName("urn:com:bbva:czic:loansimulator:facade:v00:dto", "monthlyAmortization");
    private final static QName _MontlyIncome_QNAME = new QName("urn:com:bbva:czic:loansimulator:facade:v00:dto", "montlyIncome");
    private final static QName _NotarialCharges_QNAME = new QName("urn:com:bbva:czic:loansimulator:facade:v00:dto", "notarialCharges");
    private final static QName _NotaryFees_QNAME = new QName("urn:com:bbva:czic:loansimulator:facade:v00:dto", "notaryFees");
    private final static QName _OtherExpenses_QNAME = new QName("urn:com:bbva:czic:loansimulator:facade:v00:dto", "otherExpenses");
    private final static QName _PriceHousing_QNAME = new QName("urn:com:bbva:czic:loansimulator:facade:v00:dto", "priceHousing");
    private final static QName _QuotaValue_QNAME = new QName("urn:com:bbva:czic:loansimulator:facade:v00:dto", "quotaValue");
    private final static QName _RegistrationFees_QNAME = new QName("urn:com:bbva:czic:loansimulator:facade:v00:dto", "registrationFees");
    private final static QName _RegistrationRight_QNAME = new QName("urn:com:bbva:czic:loansimulator:facade:v00:dto", "registrationRight");
    private final static QName _RetefuenteValue_QNAME = new QName("urn:com:bbva:czic:loansimulator:facade:v00:dto", "retefuenteValue");
    private final static QName _Seller_QNAME = new QName("urn:com:bbva:czic:loansimulator:facade:v00:dto", "seller");
    private final static QName _TotalExpensesRegistration_QNAME = new QName("urn:com:bbva:czic:loansimulator:facade:v00:dto", "totalExpensesRegistration");
    private final static QName _TotalNotaryExpenses_QNAME = new QName("urn:com:bbva:czic:loansimulator:facade:v00:dto", "totalNotaryExpenses");
    private final static QName _TotalOtherExpenses_QNAME = new QName("urn:com:bbva:czic:loansimulator:facade:v00:dto", "totalOtherExpenses");
    private final static QName _TotalSpends_QNAME = new QName("urn:com:bbva:czic:loansimulator:facade:v00:dto", "totalSpends");
    private final static QName _UninsuredFee_QNAME = new QName("urn:com:bbva:czic:loansimulator:facade:v00:dto", "uninsuredFee");
    private final static QName _ValueAppraise_QNAME = new QName("urn:com:bbva:czic:loansimulator:facade:v00:dto", "valueAppraise");
    private final static QName _ValueCapitalAmount_QNAME = new QName("urn:com:bbva:czic:loansimulator:facade:v00:dto", "valueCapitalAmount");
    private final static QName _ValueCopies_QNAME = new QName("urn:com:bbva:czic:loansimulator:facade:v00:dto", "valueCopies");
    private final static QName _ValueCoverage_QNAME = new QName("urn:com:bbva:czic:loansimulator:facade:v00:dto", "valueCoverage");
    private final static QName _ValueFireEarthquake_QNAME = new QName("urn:com:bbva:czic:loansimulator:facade:v00:dto", "valueFireEarthquake");
    private final static QName _ValueInsuranceFiresEarthquakes_QNAME = new QName("urn:com:bbva:czic:loansimulator:facade:v00:dto", "valueInsuranceFiresEarthquakes");
    private final static QName _ValueInsuranceQuota_QNAME = new QName("urn:com:bbva:czic:loansimulator:facade:v00:dto", "valueInsuranceQuota");
    private final static QName _ValueInterestBonus_QNAME = new QName("urn:com:bbva:czic:loansimulator:facade:v00:dto", "valueInterestBonus");
    private final static QName _ValueStudyTitle_QNAME = new QName("urn:com:bbva:czic:loansimulator:facade:v00:dto", "valueStudyTitle");
    private final static QName _VtuPaymentCapital_QNAME = new QName("urn:com:bbva:czic:loansimulator:facade:v00:dto", "vtuPaymentCapital");
    private final static QName _VtuPaymentInsurance_QNAME = new QName("urn:com:bbva:czic:loansimulator:facade:v00:dto", "vtuPaymentInsurance");
    private final static QName _VtuPaymentInterest_QNAME = new QName("urn:com:bbva:czic:loansimulator:facade:v00:dto", "vtuPaymentInterest");
    private final static QName _VtuPercentage_QNAME = new QName("urn:com:bbva:czic:loansimulator:facade:v00:dto", "vtuPercentage");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.bbva.czic.loansimulator.facade.v00.dto
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link AdditionalCosts }
     * 
     */
    public AdditionalCosts createAdditionalCosts() {
        return new AdditionalCosts();
    }

    /**
     * Create an instance of {@link AmortizationTable }
     * 
     */
    public AmortizationTable createAmortizationTable() {
        return new AmortizationTable();
    }

    /**
     * Create an instance of {@link AmountToFinanced }
     * 
     */
    public AmountToFinanced createAmountToFinanced() {
        return new AmountToFinanced();
    }

    /**
     * Create an instance of {@link BalanceAmount }
     * 
     */
    public BalanceAmount createBalanceAmount() {
        return new BalanceAmount();
    }

    /**
     * Create an instance of {@link BenefitTax }
     * 
     */
    public BenefitTax createBenefitTax() {
        return new BenefitTax();
    }

    /**
     * Create an instance of {@link Buyer }
     * 
     */
    public Buyer createBuyer() {
        return new Buyer();
    }

    /**
     * Create an instance of {@link Capital }
     * 
     */
    public Capital createCapital() {
        return new Capital();
    }

    /**
     * Create an instance of {@link CertificateOfFreedom }
     * 
     */
    public CertificateOfFreedom createCertificateOfFreedom() {
        return new CertificateOfFreedom();
    }

    /**
     * Create an instance of {@link CountryResidence }
     * 
     */
    public CountryResidence createCountryResidence() {
        return new CountryResidence();
    }

    /**
     * Create an instance of {@link CoverageFee }
     * 
     */
    public CoverageFee createCoverageFee() {
        return new CoverageFee();
    }

    /**
     * Create an instance of {@link DataLoanSimulation }
     * 
     */
    public DataLoanSimulation createDataLoanSimulation() {
        return new DataLoanSimulation();
    }

    /**
     * Create an instance of {@link DocumentType }
     * 
     */
    public DocumentType createDocumentType() {
        return new DocumentType();
    }

    /**
     * Create an instance of {@link Interests }
     * 
     */
    public Interests createInterests() {
        return new Interests();
    }

    /**
     * Create an instance of {@link LifeInsuranceCost }
     * 
     */
    public LifeInsuranceCost createLifeInsuranceCost() {
        return new LifeInsuranceCost();
    }

    /**
     * Create an instance of {@link LifeInsuranceValue }
     * 
     */
    public LifeInsuranceValue createLifeInsuranceValue() {
        return new LifeInsuranceValue();
    }

    /**
     * Create an instance of {@link LoanSimulation }
     * 
     */
    public LoanSimulation createLoanSimulation() {
        return new LoanSimulation();
    }

    /**
     * Create an instance of {@link MonthlyAmortization }
     * 
     */
    public MonthlyAmortization createMonthlyAmortization() {
        return new MonthlyAmortization();
    }

    /**
     * Create an instance of {@link MontlyIncome }
     * 
     */
    public MontlyIncome createMontlyIncome() {
        return new MontlyIncome();
    }

    /**
     * Create an instance of {@link NotarialCharges }
     * 
     */
    public NotarialCharges createNotarialCharges() {
        return new NotarialCharges();
    }

    /**
     * Create an instance of {@link NotaryFees }
     * 
     */
    public NotaryFees createNotaryFees() {
        return new NotaryFees();
    }

    /**
     * Create an instance of {@link OtherExpenses }
     * 
     */
    public OtherExpenses createOtherExpenses() {
        return new OtherExpenses();
    }

    /**
     * Create an instance of {@link PriceHousing }
     * 
     */
    public PriceHousing createPriceHousing() {
        return new PriceHousing();
    }

    /**
     * Create an instance of {@link QuotaValue }
     * 
     */
    public QuotaValue createQuotaValue() {
        return new QuotaValue();
    }

    /**
     * Create an instance of {@link RegistrationFees }
     * 
     */
    public RegistrationFees createRegistrationFees() {
        return new RegistrationFees();
    }

    /**
     * Create an instance of {@link RegistrationRight }
     * 
     */
    public RegistrationRight createRegistrationRight() {
        return new RegistrationRight();
    }

    /**
     * Create an instance of {@link RetefuenteValue }
     * 
     */
    public RetefuenteValue createRetefuenteValue() {
        return new RetefuenteValue();
    }

    /**
     * Create an instance of {@link Seller }
     * 
     */
    public Seller createSeller() {
        return new Seller();
    }

    /**
     * Create an instance of {@link TotalExpensesRegistration }
     * 
     */
    public TotalExpensesRegistration createTotalExpensesRegistration() {
        return new TotalExpensesRegistration();
    }

    /**
     * Create an instance of {@link TotalNotaryExpenses }
     * 
     */
    public TotalNotaryExpenses createTotalNotaryExpenses() {
        return new TotalNotaryExpenses();
    }

    /**
     * Create an instance of {@link TotalOtherExpenses }
     * 
     */
    public TotalOtherExpenses createTotalOtherExpenses() {
        return new TotalOtherExpenses();
    }

    /**
     * Create an instance of {@link TotalSpends }
     * 
     */
    public TotalSpends createTotalSpends() {
        return new TotalSpends();
    }

    /**
     * Create an instance of {@link UninsuredFee }
     * 
     */
    public UninsuredFee createUninsuredFee() {
        return new UninsuredFee();
    }

    /**
     * Create an instance of {@link ValueAppraise }
     * 
     */
    public ValueAppraise createValueAppraise() {
        return new ValueAppraise();
    }

    /**
     * Create an instance of {@link ValueCapitalAmount }
     * 
     */
    public ValueCapitalAmount createValueCapitalAmount() {
        return new ValueCapitalAmount();
    }

    /**
     * Create an instance of {@link ValueCopies }
     * 
     */
    public ValueCopies createValueCopies() {
        return new ValueCopies();
    }

    /**
     * Create an instance of {@link ValueCoverage }
     * 
     */
    public ValueCoverage createValueCoverage() {
        return new ValueCoverage();
    }

    /**
     * Create an instance of {@link ValueFireEarthquake }
     * 
     */
    public ValueFireEarthquake createValueFireEarthquake() {
        return new ValueFireEarthquake();
    }

    /**
     * Create an instance of {@link ValueInsuranceFiresEarthquakes }
     * 
     */
    public ValueInsuranceFiresEarthquakes createValueInsuranceFiresEarthquakes() {
        return new ValueInsuranceFiresEarthquakes();
    }

    /**
     * Create an instance of {@link ValueInsuranceQuota }
     * 
     */
    public ValueInsuranceQuota createValueInsuranceQuota() {
        return new ValueInsuranceQuota();
    }

    /**
     * Create an instance of {@link ValueInterestBonus }
     * 
     */
    public ValueInterestBonus createValueInterestBonus() {
        return new ValueInterestBonus();
    }

    /**
     * Create an instance of {@link ValueStudyTitle }
     * 
     */
    public ValueStudyTitle createValueStudyTitle() {
        return new ValueStudyTitle();
    }

    /**
     * Create an instance of {@link VtuPaymentCapital }
     * 
     */
    public VtuPaymentCapital createVtuPaymentCapital() {
        return new VtuPaymentCapital();
    }

    /**
     * Create an instance of {@link VtuPaymentInsurance }
     * 
     */
    public VtuPaymentInsurance createVtuPaymentInsurance() {
        return new VtuPaymentInsurance();
    }

    /**
     * Create an instance of {@link VtuPaymentInterest }
     * 
     */
    public VtuPaymentInterest createVtuPaymentInterest() {
        return new VtuPaymentInterest();
    }

    /**
     * Create an instance of {@link VtuPercentage }
     * 
     */
    public VtuPercentage createVtuPercentage() {
        return new VtuPercentage();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AdditionalCosts }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com:bbva:czic:loansimulator:facade:v00:dto", name = "additionalCosts")
    public JAXBElement<AdditionalCosts> createAdditionalCosts(AdditionalCosts value) {
        return new JAXBElement<AdditionalCosts>(_AdditionalCosts_QNAME, AdditionalCosts.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AmortizationTable }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com:bbva:czic:loansimulator:facade:v00:dto", name = "amortizationTable")
    public JAXBElement<AmortizationTable> createAmortizationTable(AmortizationTable value) {
        return new JAXBElement<AmortizationTable>(_AmortizationTable_QNAME, AmortizationTable.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AmountToFinanced }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com:bbva:czic:loansimulator:facade:v00:dto", name = "amountToFinanced")
    public JAXBElement<AmountToFinanced> createAmountToFinanced(AmountToFinanced value) {
        return new JAXBElement<AmountToFinanced>(_AmountToFinanced_QNAME, AmountToFinanced.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BalanceAmount }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com:bbva:czic:loansimulator:facade:v00:dto", name = "balanceAmount")
    public JAXBElement<BalanceAmount> createBalanceAmount(BalanceAmount value) {
        return new JAXBElement<BalanceAmount>(_BalanceAmount_QNAME, BalanceAmount.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link BenefitTax }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com:bbva:czic:loansimulator:facade:v00:dto", name = "benefitTax")
    public JAXBElement<BenefitTax> createBenefitTax(BenefitTax value) {
        return new JAXBElement<BenefitTax>(_BenefitTax_QNAME, BenefitTax.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Buyer }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com:bbva:czic:loansimulator:facade:v00:dto", name = "buyer")
    public JAXBElement<Buyer> createBuyer(Buyer value) {
        return new JAXBElement<Buyer>(_Buyer_QNAME, Buyer.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Capital }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com:bbva:czic:loansimulator:facade:v00:dto", name = "capital")
    public JAXBElement<Capital> createCapital(Capital value) {
        return new JAXBElement<Capital>(_Capital_QNAME, Capital.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CertificateOfFreedom }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com:bbva:czic:loansimulator:facade:v00:dto", name = "certificateOfFreedom")
    public JAXBElement<CertificateOfFreedom> createCertificateOfFreedom(CertificateOfFreedom value) {
        return new JAXBElement<CertificateOfFreedom>(_CertificateOfFreedom_QNAME, CertificateOfFreedom.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CountryResidence }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com:bbva:czic:loansimulator:facade:v00:dto", name = "countryResidence")
    public JAXBElement<CountryResidence> createCountryResidence(CountryResidence value) {
        return new JAXBElement<CountryResidence>(_CountryResidence_QNAME, CountryResidence.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CoverageFee }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com:bbva:czic:loansimulator:facade:v00:dto", name = "coverageFee")
    public JAXBElement<CoverageFee> createCoverageFee(CoverageFee value) {
        return new JAXBElement<CoverageFee>(_CoverageFee_QNAME, CoverageFee.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataLoanSimulation }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com:bbva:czic:loansimulator:facade:v00:dto", name = "dataLoanSimulation")
    public JAXBElement<DataLoanSimulation> createDataLoanSimulation(DataLoanSimulation value) {
        return new JAXBElement<DataLoanSimulation>(_DataLoanSimulation_QNAME, DataLoanSimulation.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DocumentType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com:bbva:czic:loansimulator:facade:v00:dto", name = "documentType")
    public JAXBElement<DocumentType> createDocumentType(DocumentType value) {
        return new JAXBElement<DocumentType>(_DocumentType_QNAME, DocumentType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Interests }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com:bbva:czic:loansimulator:facade:v00:dto", name = "interests")
    public JAXBElement<Interests> createInterests(Interests value) {
        return new JAXBElement<Interests>(_Interests_QNAME, Interests.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LifeInsuranceCost }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com:bbva:czic:loansimulator:facade:v00:dto", name = "lifeInsuranceCost")
    public JAXBElement<LifeInsuranceCost> createLifeInsuranceCost(LifeInsuranceCost value) {
        return new JAXBElement<LifeInsuranceCost>(_LifeInsuranceCost_QNAME, LifeInsuranceCost.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LifeInsuranceValue }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com:bbva:czic:loansimulator:facade:v00:dto", name = "lifeInsuranceValue")
    public JAXBElement<LifeInsuranceValue> createLifeInsuranceValue(LifeInsuranceValue value) {
        return new JAXBElement<LifeInsuranceValue>(_LifeInsuranceValue_QNAME, LifeInsuranceValue.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LoanSimulation }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com:bbva:czic:loansimulator:facade:v00:dto", name = "loanSimulation")
    public JAXBElement<LoanSimulation> createLoanSimulation(LoanSimulation value) {
        return new JAXBElement<LoanSimulation>(_LoanSimulation_QNAME, LoanSimulation.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MonthlyAmortization }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com:bbva:czic:loansimulator:facade:v00:dto", name = "monthlyAmortization")
    public JAXBElement<MonthlyAmortization> createMonthlyAmortization(MonthlyAmortization value) {
        return new JAXBElement<MonthlyAmortization>(_MonthlyAmortization_QNAME, MonthlyAmortization.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MontlyIncome }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com:bbva:czic:loansimulator:facade:v00:dto", name = "montlyIncome")
    public JAXBElement<MontlyIncome> createMontlyIncome(MontlyIncome value) {
        return new JAXBElement<MontlyIncome>(_MontlyIncome_QNAME, MontlyIncome.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link NotarialCharges }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com:bbva:czic:loansimulator:facade:v00:dto", name = "notarialCharges")
    public JAXBElement<NotarialCharges> createNotarialCharges(NotarialCharges value) {
        return new JAXBElement<NotarialCharges>(_NotarialCharges_QNAME, NotarialCharges.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link NotaryFees }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com:bbva:czic:loansimulator:facade:v00:dto", name = "notaryFees")
    public JAXBElement<NotaryFees> createNotaryFees(NotaryFees value) {
        return new JAXBElement<NotaryFees>(_NotaryFees_QNAME, NotaryFees.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link OtherExpenses }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com:bbva:czic:loansimulator:facade:v00:dto", name = "otherExpenses")
    public JAXBElement<OtherExpenses> createOtherExpenses(OtherExpenses value) {
        return new JAXBElement<OtherExpenses>(_OtherExpenses_QNAME, OtherExpenses.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PriceHousing }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com:bbva:czic:loansimulator:facade:v00:dto", name = "priceHousing")
    public JAXBElement<PriceHousing> createPriceHousing(PriceHousing value) {
        return new JAXBElement<PriceHousing>(_PriceHousing_QNAME, PriceHousing.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link QuotaValue }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com:bbva:czic:loansimulator:facade:v00:dto", name = "quotaValue")
    public JAXBElement<QuotaValue> createQuotaValue(QuotaValue value) {
        return new JAXBElement<QuotaValue>(_QuotaValue_QNAME, QuotaValue.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RegistrationFees }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com:bbva:czic:loansimulator:facade:v00:dto", name = "registrationFees")
    public JAXBElement<RegistrationFees> createRegistrationFees(RegistrationFees value) {
        return new JAXBElement<RegistrationFees>(_RegistrationFees_QNAME, RegistrationFees.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RegistrationRight }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com:bbva:czic:loansimulator:facade:v00:dto", name = "registrationRight")
    public JAXBElement<RegistrationRight> createRegistrationRight(RegistrationRight value) {
        return new JAXBElement<RegistrationRight>(_RegistrationRight_QNAME, RegistrationRight.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RetefuenteValue }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com:bbva:czic:loansimulator:facade:v00:dto", name = "retefuenteValue")
    public JAXBElement<RetefuenteValue> createRetefuenteValue(RetefuenteValue value) {
        return new JAXBElement<RetefuenteValue>(_RetefuenteValue_QNAME, RetefuenteValue.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Seller }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com:bbva:czic:loansimulator:facade:v00:dto", name = "seller")
    public JAXBElement<Seller> createSeller(Seller value) {
        return new JAXBElement<Seller>(_Seller_QNAME, Seller.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TotalExpensesRegistration }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com:bbva:czic:loansimulator:facade:v00:dto", name = "totalExpensesRegistration")
    public JAXBElement<TotalExpensesRegistration> createTotalExpensesRegistration(TotalExpensesRegistration value) {
        return new JAXBElement<TotalExpensesRegistration>(_TotalExpensesRegistration_QNAME, TotalExpensesRegistration.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TotalNotaryExpenses }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com:bbva:czic:loansimulator:facade:v00:dto", name = "totalNotaryExpenses")
    public JAXBElement<TotalNotaryExpenses> createTotalNotaryExpenses(TotalNotaryExpenses value) {
        return new JAXBElement<TotalNotaryExpenses>(_TotalNotaryExpenses_QNAME, TotalNotaryExpenses.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TotalOtherExpenses }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com:bbva:czic:loansimulator:facade:v00:dto", name = "totalOtherExpenses")
    public JAXBElement<TotalOtherExpenses> createTotalOtherExpenses(TotalOtherExpenses value) {
        return new JAXBElement<TotalOtherExpenses>(_TotalOtherExpenses_QNAME, TotalOtherExpenses.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link TotalSpends }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com:bbva:czic:loansimulator:facade:v00:dto", name = "totalSpends")
    public JAXBElement<TotalSpends> createTotalSpends(TotalSpends value) {
        return new JAXBElement<TotalSpends>(_TotalSpends_QNAME, TotalSpends.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UninsuredFee }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com:bbva:czic:loansimulator:facade:v00:dto", name = "uninsuredFee")
    public JAXBElement<UninsuredFee> createUninsuredFee(UninsuredFee value) {
        return new JAXBElement<UninsuredFee>(_UninsuredFee_QNAME, UninsuredFee.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ValueAppraise }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com:bbva:czic:loansimulator:facade:v00:dto", name = "valueAppraise")
    public JAXBElement<ValueAppraise> createValueAppraise(ValueAppraise value) {
        return new JAXBElement<ValueAppraise>(_ValueAppraise_QNAME, ValueAppraise.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ValueCapitalAmount }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com:bbva:czic:loansimulator:facade:v00:dto", name = "valueCapitalAmount")
    public JAXBElement<ValueCapitalAmount> createValueCapitalAmount(ValueCapitalAmount value) {
        return new JAXBElement<ValueCapitalAmount>(_ValueCapitalAmount_QNAME, ValueCapitalAmount.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ValueCopies }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com:bbva:czic:loansimulator:facade:v00:dto", name = "valueCopies")
    public JAXBElement<ValueCopies> createValueCopies(ValueCopies value) {
        return new JAXBElement<ValueCopies>(_ValueCopies_QNAME, ValueCopies.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ValueCoverage }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com:bbva:czic:loansimulator:facade:v00:dto", name = "valueCoverage")
    public JAXBElement<ValueCoverage> createValueCoverage(ValueCoverage value) {
        return new JAXBElement<ValueCoverage>(_ValueCoverage_QNAME, ValueCoverage.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ValueFireEarthquake }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com:bbva:czic:loansimulator:facade:v00:dto", name = "valueFireEarthquake")
    public JAXBElement<ValueFireEarthquake> createValueFireEarthquake(ValueFireEarthquake value) {
        return new JAXBElement<ValueFireEarthquake>(_ValueFireEarthquake_QNAME, ValueFireEarthquake.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ValueInsuranceFiresEarthquakes }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com:bbva:czic:loansimulator:facade:v00:dto", name = "valueInsuranceFiresEarthquakes")
    public JAXBElement<ValueInsuranceFiresEarthquakes> createValueInsuranceFiresEarthquakes(ValueInsuranceFiresEarthquakes value) {
        return new JAXBElement<ValueInsuranceFiresEarthquakes>(_ValueInsuranceFiresEarthquakes_QNAME, ValueInsuranceFiresEarthquakes.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ValueInsuranceQuota }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com:bbva:czic:loansimulator:facade:v00:dto", name = "valueInsuranceQuota")
    public JAXBElement<ValueInsuranceQuota> createValueInsuranceQuota(ValueInsuranceQuota value) {
        return new JAXBElement<ValueInsuranceQuota>(_ValueInsuranceQuota_QNAME, ValueInsuranceQuota.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ValueInterestBonus }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com:bbva:czic:loansimulator:facade:v00:dto", name = "valueInterestBonus")
    public JAXBElement<ValueInterestBonus> createValueInterestBonus(ValueInterestBonus value) {
        return new JAXBElement<ValueInterestBonus>(_ValueInterestBonus_QNAME, ValueInterestBonus.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ValueStudyTitle }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com:bbva:czic:loansimulator:facade:v00:dto", name = "valueStudyTitle")
    public JAXBElement<ValueStudyTitle> createValueStudyTitle(ValueStudyTitle value) {
        return new JAXBElement<ValueStudyTitle>(_ValueStudyTitle_QNAME, ValueStudyTitle.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VtuPaymentCapital }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com:bbva:czic:loansimulator:facade:v00:dto", name = "vtuPaymentCapital")
    public JAXBElement<VtuPaymentCapital> createVtuPaymentCapital(VtuPaymentCapital value) {
        return new JAXBElement<VtuPaymentCapital>(_VtuPaymentCapital_QNAME, VtuPaymentCapital.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VtuPaymentInsurance }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com:bbva:czic:loansimulator:facade:v00:dto", name = "vtuPaymentInsurance")
    public JAXBElement<VtuPaymentInsurance> createVtuPaymentInsurance(VtuPaymentInsurance value) {
        return new JAXBElement<VtuPaymentInsurance>(_VtuPaymentInsurance_QNAME, VtuPaymentInsurance.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VtuPaymentInterest }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com:bbva:czic:loansimulator:facade:v00:dto", name = "vtuPaymentInterest")
    public JAXBElement<VtuPaymentInterest> createVtuPaymentInterest(VtuPaymentInterest value) {
        return new JAXBElement<VtuPaymentInterest>(_VtuPaymentInterest_QNAME, VtuPaymentInterest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link VtuPercentage }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "urn:com:bbva:czic:loansimulator:facade:v00:dto", name = "vtuPercentage")
    public JAXBElement<VtuPercentage> createVtuPercentage(VtuPercentage value) {
        return new JAXBElement<VtuPercentage>(_VtuPercentage_QNAME, VtuPercentage.class, null, value);
    }

}
