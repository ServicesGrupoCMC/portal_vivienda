
package com.bbva.czic.loansimulator.facade.v00.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para seller complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="seller"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{urn:com:bbva:czic:loansimulator:facade:v00:dto}otherExpenses" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:com:bbva:czic:loansimulator:facade:v00:dto}notaryFees" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:com:bbva:czic:loansimulator:facade:v00:dto}registrationFees" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:com:bbva:czic:loansimulator:facade:v00:dto}totalSpends" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "seller", propOrder = {
    "otherExpenses",
    "notaryFees",
    "registrationFees",
    "totalSpends"
})
public class Seller {

    @XmlElement(namespace = "urn:com:bbva:czic:loansimulator:facade:v00:dto")
    protected OtherExpenses otherExpenses;
    @XmlElement(namespace = "urn:com:bbva:czic:loansimulator:facade:v00:dto")
    protected NotaryFees notaryFees;
    @XmlElement(namespace = "urn:com:bbva:czic:loansimulator:facade:v00:dto")
    protected RegistrationFees registrationFees;
    @XmlElement(namespace = "urn:com:bbva:czic:loansimulator:facade:v00:dto")
    protected TotalSpends totalSpends;

    /**
     * Obtiene el valor de la propiedad otherExpenses.
     * 
     * @return
     *     possible object is
     *     {@link OtherExpenses }
     *     
     */
    public OtherExpenses getOtherExpenses() {
        return otherExpenses;
    }

    /**
     * Define el valor de la propiedad otherExpenses.
     * 
     * @param value
     *     allowed object is
     *     {@link OtherExpenses }
     *     
     */
    public void setOtherExpenses(OtherExpenses value) {
        this.otherExpenses = value;
    }

    /**
     * Obtiene el valor de la propiedad notaryFees.
     * 
     * @return
     *     possible object is
     *     {@link NotaryFees }
     *     
     */
    public NotaryFees getNotaryFees() {
        return notaryFees;
    }

    /**
     * Define el valor de la propiedad notaryFees.
     * 
     * @param value
     *     allowed object is
     *     {@link NotaryFees }
     *     
     */
    public void setNotaryFees(NotaryFees value) {
        this.notaryFees = value;
    }

    /**
     * Obtiene el valor de la propiedad registrationFees.
     * 
     * @return
     *     possible object is
     *     {@link RegistrationFees }
     *     
     */
    public RegistrationFees getRegistrationFees() {
        return registrationFees;
    }

    /**
     * Define el valor de la propiedad registrationFees.
     * 
     * @param value
     *     allowed object is
     *     {@link RegistrationFees }
     *     
     */
    public void setRegistrationFees(RegistrationFees value) {
        this.registrationFees = value;
    }

    /**
     * Obtiene el valor de la propiedad totalSpends.
     * 
     * @return
     *     possible object is
     *     {@link TotalSpends }
     *     
     */
    public TotalSpends getTotalSpends() {
        return totalSpends;
    }

    /**
     * Define el valor de la propiedad totalSpends.
     * 
     * @param value
     *     allowed object is
     *     {@link TotalSpends }
     *     
     */
    public void setTotalSpends(TotalSpends value) {
        this.totalSpends = value;
    }

}
