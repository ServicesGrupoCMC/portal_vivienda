
package com.bbva.czic.loansimulator.facade.v00.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para loanSimulation complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="loanSimulation"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="financialLine" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="financialSubline" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="simulationTypeIndicator" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:com:bbva:czic:loansimulator:facade:v00:dto}priceHousing" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:com:bbva:czic:loansimulator:facade:v00:dto}montlyIncome" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:com:bbva:czic:loansimulator:facade:v00:dto}amountToFinanced" minOccurs="0"/&gt;
 *         &lt;element name="yearsToFinanced" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/&gt;
 *         &lt;element name="buyPercentage" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="yearIntegerAmortization" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/&gt;
 *         &lt;element name="amortizationType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="isGovernmentCoverage" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="isLinkedMilitaryForces" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="isLivesInTheCountry" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="isBbvaPayroll" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="isHousingInProjectBbva" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="isLiveOnProperty" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="isNewHousing" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="names" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="surnames" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:com:bbva:czic:loansimulator:facade:v00:dto}documentType" minOccurs="0"/&gt;
 *         &lt;element name="documentInteger" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="birthDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="phone" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="cellPhone" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:com:bbva:czic:loansimulator:facade:v00:dto}countryResidence" minOccurs="0"/&gt;
 *         &lt;element name="stateResidence" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="cityResidence" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:com:bbva:czic:loansimulator:facade:v00:dto}quotaValue" minOccurs="0"/&gt;
 *         &lt;element name="percentageFixedQuota" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="subProduct" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/&gt;
 *         &lt;element name="rateCoverage" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:com:bbva:czic:loansimulator:facade:v00:dto}valueCapitalAmount" minOccurs="0"/&gt;
 *         &lt;element name="loanTermYears" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:com:bbva:czic:loansimulator:facade:v00:dto}valueInterestBonus" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:com:bbva:czic:loansimulator:facade:v00:dto}lifeInsuranceValue" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:com:bbva:czic:loansimulator:facade:v00:dto}valueFireEarthquake" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:com:bbva:czic:loansimulator:facade:v00:dto}valueCoverage" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:com:bbva:czic:loansimulator:facade:v00:dto}additionalCosts" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:com:bbva:czic:loansimulator:facade:v00:dto}amortizationTable" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "loanSimulation", propOrder = {
    "financialLine",
    "financialSubline",
    "simulationTypeIndicator",
    "priceHousing",
    "montlyIncome",
    "amountToFinanced",
    "yearsToFinanced",
    "buyPercentage",
    "yearIntegerAmortization",
    "amortizationType",
    "isGovernmentCoverage",
    "isLinkedMilitaryForces",
    "isLivesInTheCountry",
    "isBbvaPayroll",
    "isHousingInProjectBbva",
    "isLiveOnProperty",
    "isNewHousing",
    "names",
    "surnames",
    "documentType",
    "documentInteger",
    "birthDate",
    "email",
    "phone",
    "cellPhone",
    "countryResidence",
    "stateResidence",
    "cityResidence",
    "quotaValue",
    "percentageFixedQuota",
    "subProduct",
    "rateCoverage",
    "valueCapitalAmount",
    "loanTermYears",
    "valueInterestBonus",
    "lifeInsuranceValue",
    "valueFireEarthquake",
    "valueCoverage",
    "additionalCosts",
    "amortizationTable"
})
public class LoanSimulation {

    protected String financialLine;
    protected String financialSubline;
    protected String simulationTypeIndicator;
    @XmlElement(namespace = "urn:com:bbva:czic:loansimulator:facade:v00:dto")
    protected PriceHousing priceHousing;
    @XmlElement(namespace = "urn:com:bbva:czic:loansimulator:facade:v00:dto")
    protected MontlyIncome montlyIncome;
    @XmlElement(namespace = "urn:com:bbva:czic:loansimulator:facade:v00:dto")
    protected AmountToFinanced amountToFinanced;
    protected Double yearsToFinanced;
    protected String buyPercentage;
    protected Double yearIntegerAmortization;
    protected String amortizationType;
    protected Boolean isGovernmentCoverage;
    protected Boolean isLinkedMilitaryForces;
    protected Boolean isLivesInTheCountry;
    protected Boolean isBbvaPayroll;
    protected Boolean isHousingInProjectBbva;
    protected Boolean isLiveOnProperty;
    protected Boolean isNewHousing;
    protected String names;
    protected String surnames;
    @XmlElement(namespace = "urn:com:bbva:czic:loansimulator:facade:v00:dto")
    protected DocumentType documentType;
    protected String documentInteger;
    protected String birthDate;
    protected String email;
    protected String phone;
    protected String cellPhone;
    @XmlElement(namespace = "urn:com:bbva:czic:loansimulator:facade:v00:dto")
    protected CountryResidence countryResidence;
    protected String stateResidence;
    protected String cityResidence;
    @XmlElement(namespace = "urn:com:bbva:czic:loansimulator:facade:v00:dto")
    protected QuotaValue quotaValue;
    protected String percentageFixedQuota;
    protected Double subProduct;
    protected String rateCoverage;
    @XmlElement(namespace = "urn:com:bbva:czic:loansimulator:facade:v00:dto")
    protected ValueCapitalAmount valueCapitalAmount;
    protected Double loanTermYears;
    @XmlElement(namespace = "urn:com:bbva:czic:loansimulator:facade:v00:dto")
    protected ValueInterestBonus valueInterestBonus;
    @XmlElement(namespace = "urn:com:bbva:czic:loansimulator:facade:v00:dto")
    protected LifeInsuranceValue lifeInsuranceValue;
    @XmlElement(namespace = "urn:com:bbva:czic:loansimulator:facade:v00:dto")
    protected ValueFireEarthquake valueFireEarthquake;
    @XmlElement(namespace = "urn:com:bbva:czic:loansimulator:facade:v00:dto")
    protected ValueCoverage valueCoverage;
    @XmlElement(namespace = "urn:com:bbva:czic:loansimulator:facade:v00:dto")
    protected AdditionalCosts additionalCosts;
    @XmlElement(namespace = "urn:com:bbva:czic:loansimulator:facade:v00:dto")
    protected AmortizationTable amortizationTable;

    /**
     * Obtiene el valor de la propiedad financialLine.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFinancialLine() {
        return financialLine;
    }

    /**
     * Define el valor de la propiedad financialLine.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFinancialLine(String value) {
        this.financialLine = value;
    }

    /**
     * Obtiene el valor de la propiedad financialSubline.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFinancialSubline() {
        return financialSubline;
    }

    /**
     * Define el valor de la propiedad financialSubline.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFinancialSubline(String value) {
        this.financialSubline = value;
    }

    /**
     * Obtiene el valor de la propiedad simulationTypeIndicator.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSimulationTypeIndicator() {
        return simulationTypeIndicator;
    }

    /**
     * Define el valor de la propiedad simulationTypeIndicator.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSimulationTypeIndicator(String value) {
        this.simulationTypeIndicator = value;
    }

    /**
     * Obtiene el valor de la propiedad priceHousing.
     * 
     * @return
     *     possible object is
     *     {@link PriceHousing }
     *     
     */
    public PriceHousing getPriceHousing() {
        return priceHousing;
    }

    /**
     * Define el valor de la propiedad priceHousing.
     * 
     * @param value
     *     allowed object is
     *     {@link PriceHousing }
     *     
     */
    public void setPriceHousing(PriceHousing value) {
        this.priceHousing = value;
    }

    /**
     * Obtiene el valor de la propiedad montlyIncome.
     * 
     * @return
     *     possible object is
     *     {@link MontlyIncome }
     *     
     */
    public MontlyIncome getMontlyIncome() {
        return montlyIncome;
    }

    /**
     * Define el valor de la propiedad montlyIncome.
     * 
     * @param value
     *     allowed object is
     *     {@link MontlyIncome }
     *     
     */
    public void setMontlyIncome(MontlyIncome value) {
        this.montlyIncome = value;
    }

    /**
     * Obtiene el valor de la propiedad amountToFinanced.
     * 
     * @return
     *     possible object is
     *     {@link AmountToFinanced }
     *     
     */
    public AmountToFinanced getAmountToFinanced() {
        return amountToFinanced;
    }

    /**
     * Define el valor de la propiedad amountToFinanced.
     * 
     * @param value
     *     allowed object is
     *     {@link AmountToFinanced }
     *     
     */
    public void setAmountToFinanced(AmountToFinanced value) {
        this.amountToFinanced = value;
    }

    /**
     * Obtiene el valor de la propiedad yearsToFinanced.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getYearsToFinanced() {
        return yearsToFinanced;
    }

    /**
     * Define el valor de la propiedad yearsToFinanced.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setYearsToFinanced(Double value) {
        this.yearsToFinanced = value;
    }

    /**
     * Obtiene el valor de la propiedad buyPercentage.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBuyPercentage() {
        return buyPercentage;
    }

    /**
     * Define el valor de la propiedad buyPercentage.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBuyPercentage(String value) {
        this.buyPercentage = value;
    }

    /**
     * Obtiene el valor de la propiedad yearIntegerAmortization.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getYearIntegerAmortization() {
        return yearIntegerAmortization;
    }

    /**
     * Define el valor de la propiedad yearIntegerAmortization.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setYearIntegerAmortization(Double value) {
        this.yearIntegerAmortization = value;
    }

    /**
     * Obtiene el valor de la propiedad amortizationType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAmortizationType() {
        return amortizationType;
    }

    /**
     * Define el valor de la propiedad amortizationType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAmortizationType(String value) {
        this.amortizationType = value;
    }

    /**
     * Obtiene el valor de la propiedad isGovernmentCoverage.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsGovernmentCoverage() {
        return isGovernmentCoverage;
    }

    /**
     * Define el valor de la propiedad isGovernmentCoverage.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsGovernmentCoverage(Boolean value) {
        this.isGovernmentCoverage = value;
    }

    /**
     * Obtiene el valor de la propiedad isLinkedMilitaryForces.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsLinkedMilitaryForces() {
        return isLinkedMilitaryForces;
    }

    /**
     * Define el valor de la propiedad isLinkedMilitaryForces.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsLinkedMilitaryForces(Boolean value) {
        this.isLinkedMilitaryForces = value;
    }

    /**
     * Obtiene el valor de la propiedad isLivesInTheCountry.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsLivesInTheCountry() {
        return isLivesInTheCountry;
    }

    /**
     * Define el valor de la propiedad isLivesInTheCountry.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsLivesInTheCountry(Boolean value) {
        this.isLivesInTheCountry = value;
    }

    /**
     * Obtiene el valor de la propiedad isBbvaPayroll.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsBbvaPayroll() {
        return isBbvaPayroll;
    }

    /**
     * Define el valor de la propiedad isBbvaPayroll.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsBbvaPayroll(Boolean value) {
        this.isBbvaPayroll = value;
    }

    /**
     * Obtiene el valor de la propiedad isHousingInProjectBbva.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsHousingInProjectBbva() {
        return isHousingInProjectBbva;
    }

    /**
     * Define el valor de la propiedad isHousingInProjectBbva.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsHousingInProjectBbva(Boolean value) {
        this.isHousingInProjectBbva = value;
    }

    /**
     * Obtiene el valor de la propiedad isLiveOnProperty.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsLiveOnProperty() {
        return isLiveOnProperty;
    }

    /**
     * Define el valor de la propiedad isLiveOnProperty.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsLiveOnProperty(Boolean value) {
        this.isLiveOnProperty = value;
    }

    /**
     * Obtiene el valor de la propiedad isNewHousing.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIsNewHousing() {
        return isNewHousing;
    }

    /**
     * Define el valor de la propiedad isNewHousing.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIsNewHousing(Boolean value) {
        this.isNewHousing = value;
    }

    /**
     * Obtiene el valor de la propiedad names.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNames() {
        return names;
    }

    /**
     * Define el valor de la propiedad names.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNames(String value) {
        this.names = value;
    }

    /**
     * Obtiene el valor de la propiedad surnames.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSurnames() {
        return surnames;
    }

    /**
     * Define el valor de la propiedad surnames.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSurnames(String value) {
        this.surnames = value;
    }

    /**
     * Obtiene el valor de la propiedad documentType.
     * 
     * @return
     *     possible object is
     *     {@link DocumentType }
     *     
     */
    public DocumentType getDocumentType() {
        return documentType;
    }

    /**
     * Define el valor de la propiedad documentType.
     * 
     * @param value
     *     allowed object is
     *     {@link DocumentType }
     *     
     */
    public void setDocumentType(DocumentType value) {
        this.documentType = value;
    }

    /**
     * Obtiene el valor de la propiedad documentInteger.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDocumentInteger() {
        return documentInteger;
    }

    /**
     * Define el valor de la propiedad documentInteger.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDocumentInteger(String value) {
        this.documentInteger = value;
    }

    /**
     * Obtiene el valor de la propiedad birthDate.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBirthDate() {
        return birthDate;
    }

    /**
     * Define el valor de la propiedad birthDate.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBirthDate(String value) {
        this.birthDate = value;
    }

    /**
     * Obtiene el valor de la propiedad email.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmail() {
        return email;
    }

    /**
     * Define el valor de la propiedad email.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmail(String value) {
        this.email = value;
    }

    /**
     * Obtiene el valor de la propiedad phone.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPhone() {
        return phone;
    }

    /**
     * Define el valor de la propiedad phone.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPhone(String value) {
        this.phone = value;
    }

    /**
     * Obtiene el valor de la propiedad cellPhone.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCellPhone() {
        return cellPhone;
    }

    /**
     * Define el valor de la propiedad cellPhone.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCellPhone(String value) {
        this.cellPhone = value;
    }

    /**
     * Obtiene el valor de la propiedad countryResidence.
     * 
     * @return
     *     possible object is
     *     {@link CountryResidence }
     *     
     */
    public CountryResidence getCountryResidence() {
        return countryResidence;
    }

    /**
     * Define el valor de la propiedad countryResidence.
     * 
     * @param value
     *     allowed object is
     *     {@link CountryResidence }
     *     
     */
    public void setCountryResidence(CountryResidence value) {
        this.countryResidence = value;
    }

    /**
     * Obtiene el valor de la propiedad stateResidence.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStateResidence() {
        return stateResidence;
    }

    /**
     * Define el valor de la propiedad stateResidence.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStateResidence(String value) {
        this.stateResidence = value;
    }

    /**
     * Obtiene el valor de la propiedad cityResidence.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCityResidence() {
        return cityResidence;
    }

    /**
     * Define el valor de la propiedad cityResidence.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCityResidence(String value) {
        this.cityResidence = value;
    }

    /**
     * Obtiene el valor de la propiedad quotaValue.
     * 
     * @return
     *     possible object is
     *     {@link QuotaValue }
     *     
     */
    public QuotaValue getQuotaValue() {
        return quotaValue;
    }

    /**
     * Define el valor de la propiedad quotaValue.
     * 
     * @param value
     *     allowed object is
     *     {@link QuotaValue }
     *     
     */
    public void setQuotaValue(QuotaValue value) {
        this.quotaValue = value;
    }

    /**
     * Obtiene el valor de la propiedad percentageFixedQuota.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPercentageFixedQuota() {
        return percentageFixedQuota;
    }

    /**
     * Define el valor de la propiedad percentageFixedQuota.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPercentageFixedQuota(String value) {
        this.percentageFixedQuota = value;
    }

    /**
     * Obtiene el valor de la propiedad subProduct.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getSubProduct() {
        return subProduct;
    }

    /**
     * Define el valor de la propiedad subProduct.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setSubProduct(Double value) {
        this.subProduct = value;
    }

    /**
     * Obtiene el valor de la propiedad rateCoverage.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRateCoverage() {
        return rateCoverage;
    }

    /**
     * Define el valor de la propiedad rateCoverage.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRateCoverage(String value) {
        this.rateCoverage = value;
    }

    /**
     * Obtiene el valor de la propiedad valueCapitalAmount.
     * 
     * @return
     *     possible object is
     *     {@link ValueCapitalAmount }
     *     
     */
    public ValueCapitalAmount getValueCapitalAmount() {
        return valueCapitalAmount;
    }

    /**
     * Define el valor de la propiedad valueCapitalAmount.
     * 
     * @param value
     *     allowed object is
     *     {@link ValueCapitalAmount }
     *     
     */
    public void setValueCapitalAmount(ValueCapitalAmount value) {
        this.valueCapitalAmount = value;
    }

    /**
     * Obtiene el valor de la propiedad loanTermYears.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getLoanTermYears() {
        return loanTermYears;
    }

    /**
     * Define el valor de la propiedad loanTermYears.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setLoanTermYears(Double value) {
        this.loanTermYears = value;
    }

    /**
     * Obtiene el valor de la propiedad valueInterestBonus.
     * 
     * @return
     *     possible object is
     *     {@link ValueInterestBonus }
     *     
     */
    public ValueInterestBonus getValueInterestBonus() {
        return valueInterestBonus;
    }

    /**
     * Define el valor de la propiedad valueInterestBonus.
     * 
     * @param value
     *     allowed object is
     *     {@link ValueInterestBonus }
     *     
     */
    public void setValueInterestBonus(ValueInterestBonus value) {
        this.valueInterestBonus = value;
    }

    /**
     * Obtiene el valor de la propiedad lifeInsuranceValue.
     * 
     * @return
     *     possible object is
     *     {@link LifeInsuranceValue }
     *     
     */
    public LifeInsuranceValue getLifeInsuranceValue() {
        return lifeInsuranceValue;
    }

    /**
     * Define el valor de la propiedad lifeInsuranceValue.
     * 
     * @param value
     *     allowed object is
     *     {@link LifeInsuranceValue }
     *     
     */
    public void setLifeInsuranceValue(LifeInsuranceValue value) {
        this.lifeInsuranceValue = value;
    }

    /**
     * Obtiene el valor de la propiedad valueFireEarthquake.
     * 
     * @return
     *     possible object is
     *     {@link ValueFireEarthquake }
     *     
     */
    public ValueFireEarthquake getValueFireEarthquake() {
        return valueFireEarthquake;
    }

    /**
     * Define el valor de la propiedad valueFireEarthquake.
     * 
     * @param value
     *     allowed object is
     *     {@link ValueFireEarthquake }
     *     
     */
    public void setValueFireEarthquake(ValueFireEarthquake value) {
        this.valueFireEarthquake = value;
    }

    /**
     * Obtiene el valor de la propiedad valueCoverage.
     * 
     * @return
     *     possible object is
     *     {@link ValueCoverage }
     *     
     */
    public ValueCoverage getValueCoverage() {
        return valueCoverage;
    }

    /**
     * Define el valor de la propiedad valueCoverage.
     * 
     * @param value
     *     allowed object is
     *     {@link ValueCoverage }
     *     
     */
    public void setValueCoverage(ValueCoverage value) {
        this.valueCoverage = value;
    }

    /**
     * Obtiene el valor de la propiedad additionalCosts.
     * 
     * @return
     *     possible object is
     *     {@link AdditionalCosts }
     *     
     */
    public AdditionalCosts getAdditionalCosts() {
        return additionalCosts;
    }

    /**
     * Define el valor de la propiedad additionalCosts.
     * 
     * @param value
     *     allowed object is
     *     {@link AdditionalCosts }
     *     
     */
    public void setAdditionalCosts(AdditionalCosts value) {
        this.additionalCosts = value;
    }

    /**
     * Obtiene el valor de la propiedad amortizationTable.
     * 
     * @return
     *     possible object is
     *     {@link AmortizationTable }
     *     
     */
    public AmortizationTable getAmortizationTable() {
        return amortizationTable;
    }

    /**
     * Define el valor de la propiedad amortizationTable.
     * 
     * @param value
     *     allowed object is
     *     {@link AmortizationTable }
     *     
     */
    public void setAmortizationTable(AmortizationTable value) {
        this.amortizationTable = value;
    }

}
