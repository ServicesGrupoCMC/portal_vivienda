
package com.bbva.czic.loansimulator.facade.v00.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para notaryFees complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="notaryFees"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{urn:com:bbva:czic:loansimulator:facade:v00:dto}retefuenteValue" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:com:bbva:czic:loansimulator:facade:v00:dto}notarialCharges" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:com:bbva:czic:loansimulator:facade:v00:dto}valueCopies" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:com:bbva:czic:loansimulator:facade:v00:dto}totalNotaryExpenses" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "notaryFees", propOrder = {
    "retefuenteValue",
    "notarialCharges",
    "valueCopies",
    "totalNotaryExpenses"
})
public class NotaryFees {

    @XmlElement(namespace = "urn:com:bbva:czic:loansimulator:facade:v00:dto")
    protected RetefuenteValue retefuenteValue;
    @XmlElement(namespace = "urn:com:bbva:czic:loansimulator:facade:v00:dto")
    protected NotarialCharges notarialCharges;
    @XmlElement(namespace = "urn:com:bbva:czic:loansimulator:facade:v00:dto")
    protected ValueCopies valueCopies;
    @XmlElement(namespace = "urn:com:bbva:czic:loansimulator:facade:v00:dto")
    protected TotalNotaryExpenses totalNotaryExpenses;

    /**
     * Obtiene el valor de la propiedad retefuenteValue.
     * 
     * @return
     *     possible object is
     *     {@link RetefuenteValue }
     *     
     */
    public RetefuenteValue getRetefuenteValue() {
        return retefuenteValue;
    }

    /**
     * Define el valor de la propiedad retefuenteValue.
     * 
     * @param value
     *     allowed object is
     *     {@link RetefuenteValue }
     *     
     */
    public void setRetefuenteValue(RetefuenteValue value) {
        this.retefuenteValue = value;
    }

    /**
     * Obtiene el valor de la propiedad notarialCharges.
     * 
     * @return
     *     possible object is
     *     {@link NotarialCharges }
     *     
     */
    public NotarialCharges getNotarialCharges() {
        return notarialCharges;
    }

    /**
     * Define el valor de la propiedad notarialCharges.
     * 
     * @param value
     *     allowed object is
     *     {@link NotarialCharges }
     *     
     */
    public void setNotarialCharges(NotarialCharges value) {
        this.notarialCharges = value;
    }

    /**
     * Obtiene el valor de la propiedad valueCopies.
     * 
     * @return
     *     possible object is
     *     {@link ValueCopies }
     *     
     */
    public ValueCopies getValueCopies() {
        return valueCopies;
    }

    /**
     * Define el valor de la propiedad valueCopies.
     * 
     * @param value
     *     allowed object is
     *     {@link ValueCopies }
     *     
     */
    public void setValueCopies(ValueCopies value) {
        this.valueCopies = value;
    }

    /**
     * Obtiene el valor de la propiedad totalNotaryExpenses.
     * 
     * @return
     *     possible object is
     *     {@link TotalNotaryExpenses }
     *     
     */
    public TotalNotaryExpenses getTotalNotaryExpenses() {
        return totalNotaryExpenses;
    }

    /**
     * Define el valor de la propiedad totalNotaryExpenses.
     * 
     * @param value
     *     allowed object is
     *     {@link TotalNotaryExpenses }
     *     
     */
    public void setTotalNotaryExpenses(TotalNotaryExpenses value) {
        this.totalNotaryExpenses = value;
    }

}
