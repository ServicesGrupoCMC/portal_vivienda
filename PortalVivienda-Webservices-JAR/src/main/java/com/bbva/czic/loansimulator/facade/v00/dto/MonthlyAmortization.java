
package com.bbva.czic.loansimulator.facade.v00.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para monthlyAmortization complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="monthlyAmortization"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="quotaNumber" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:com:bbva:czic:loansimulator:facade:v00:dto}capital" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:com:bbva:czic:loansimulator:facade:v00:dto}interests" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:com:bbva:czic:loansimulator:facade:v00:dto}uninsuredFee" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:com:bbva:czic:loansimulator:facade:v00:dto}lifeInsuranceCost" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:com:bbva:czic:loansimulator:facade:v00:dto}valueInsuranceFiresEarthquakes" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:com:bbva:czic:loansimulator:facade:v00:dto}valueInsuranceQuota" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:com:bbva:czic:loansimulator:facade:v00:dto}valueCoverage" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:com:bbva:czic:loansimulator:facade:v00:dto}coverageFee" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:com:bbva:czic:loansimulator:facade:v00:dto}balanceAmount" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "monthlyAmortization", propOrder = {
    "quotaNumber",
    "capital",
    "interests",
    "uninsuredFee",
    "lifeInsuranceCost",
    "valueInsuranceFiresEarthquakes",
    "valueInsuranceQuota",
    "valueCoverage",
    "coverageFee",
    "balanceAmount"
})
public class MonthlyAmortization {

    protected Double quotaNumber;
    @XmlElement(namespace = "urn:com:bbva:czic:loansimulator:facade:v00:dto")
    protected Capital capital;
    @XmlElement(namespace = "urn:com:bbva:czic:loansimulator:facade:v00:dto")
    protected Interests interests;
    @XmlElement(namespace = "urn:com:bbva:czic:loansimulator:facade:v00:dto")
    protected UninsuredFee uninsuredFee;
    @XmlElement(namespace = "urn:com:bbva:czic:loansimulator:facade:v00:dto")
    protected LifeInsuranceCost lifeInsuranceCost;
    @XmlElement(namespace = "urn:com:bbva:czic:loansimulator:facade:v00:dto")
    protected ValueInsuranceFiresEarthquakes valueInsuranceFiresEarthquakes;
    @XmlElement(namespace = "urn:com:bbva:czic:loansimulator:facade:v00:dto")
    protected ValueInsuranceQuota valueInsuranceQuota;
    @XmlElement(namespace = "urn:com:bbva:czic:loansimulator:facade:v00:dto")
    protected ValueCoverage valueCoverage;
    @XmlElement(namespace = "urn:com:bbva:czic:loansimulator:facade:v00:dto")
    protected CoverageFee coverageFee;
    @XmlElement(namespace = "urn:com:bbva:czic:loansimulator:facade:v00:dto")
    protected BalanceAmount balanceAmount;

    /**
     * Obtiene el valor de la propiedad quotaNumber.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getQuotaNumber() {
        return quotaNumber;
    }

    /**
     * Define el valor de la propiedad quotaNumber.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setQuotaNumber(Double value) {
        this.quotaNumber = value;
    }

    /**
     * Obtiene el valor de la propiedad capital.
     * 
     * @return
     *     possible object is
     *     {@link Capital }
     *     
     */
    public Capital getCapital() {
        return capital;
    }

    /**
     * Define el valor de la propiedad capital.
     * 
     * @param value
     *     allowed object is
     *     {@link Capital }
     *     
     */
    public void setCapital(Capital value) {
        this.capital = value;
    }

    /**
     * Obtiene el valor de la propiedad interests.
     * 
     * @return
     *     possible object is
     *     {@link Interests }
     *     
     */
    public Interests getInterests() {
        return interests;
    }

    /**
     * Define el valor de la propiedad interests.
     * 
     * @param value
     *     allowed object is
     *     {@link Interests }
     *     
     */
    public void setInterests(Interests value) {
        this.interests = value;
    }

    /**
     * Obtiene el valor de la propiedad uninsuredFee.
     * 
     * @return
     *     possible object is
     *     {@link UninsuredFee }
     *     
     */
    public UninsuredFee getUninsuredFee() {
        return uninsuredFee;
    }

    /**
     * Define el valor de la propiedad uninsuredFee.
     * 
     * @param value
     *     allowed object is
     *     {@link UninsuredFee }
     *     
     */
    public void setUninsuredFee(UninsuredFee value) {
        this.uninsuredFee = value;
    }

    /**
     * Obtiene el valor de la propiedad lifeInsuranceCost.
     * 
     * @return
     *     possible object is
     *     {@link LifeInsuranceCost }
     *     
     */
    public LifeInsuranceCost getLifeInsuranceCost() {
        return lifeInsuranceCost;
    }

    /**
     * Define el valor de la propiedad lifeInsuranceCost.
     * 
     * @param value
     *     allowed object is
     *     {@link LifeInsuranceCost }
     *     
     */
    public void setLifeInsuranceCost(LifeInsuranceCost value) {
        this.lifeInsuranceCost = value;
    }

    /**
     * Obtiene el valor de la propiedad valueInsuranceFiresEarthquakes.
     * 
     * @return
     *     possible object is
     *     {@link ValueInsuranceFiresEarthquakes }
     *     
     */
    public ValueInsuranceFiresEarthquakes getValueInsuranceFiresEarthquakes() {
        return valueInsuranceFiresEarthquakes;
    }

    /**
     * Define el valor de la propiedad valueInsuranceFiresEarthquakes.
     * 
     * @param value
     *     allowed object is
     *     {@link ValueInsuranceFiresEarthquakes }
     *     
     */
    public void setValueInsuranceFiresEarthquakes(ValueInsuranceFiresEarthquakes value) {
        this.valueInsuranceFiresEarthquakes = value;
    }

    /**
     * Obtiene el valor de la propiedad valueInsuranceQuota.
     * 
     * @return
     *     possible object is
     *     {@link ValueInsuranceQuota }
     *     
     */
    public ValueInsuranceQuota getValueInsuranceQuota() {
        return valueInsuranceQuota;
    }

    /**
     * Define el valor de la propiedad valueInsuranceQuota.
     * 
     * @param value
     *     allowed object is
     *     {@link ValueInsuranceQuota }
     *     
     */
    public void setValueInsuranceQuota(ValueInsuranceQuota value) {
        this.valueInsuranceQuota = value;
    }

    /**
     * Obtiene el valor de la propiedad valueCoverage.
     * 
     * @return
     *     possible object is
     *     {@link ValueCoverage }
     *     
     */
    public ValueCoverage getValueCoverage() {
        return valueCoverage;
    }

    /**
     * Define el valor de la propiedad valueCoverage.
     * 
     * @param value
     *     allowed object is
     *     {@link ValueCoverage }
     *     
     */
    public void setValueCoverage(ValueCoverage value) {
        this.valueCoverage = value;
    }

    /**
     * Obtiene el valor de la propiedad coverageFee.
     * 
     * @return
     *     possible object is
     *     {@link CoverageFee }
     *     
     */
    public CoverageFee getCoverageFee() {
        return coverageFee;
    }

    /**
     * Define el valor de la propiedad coverageFee.
     * 
     * @param value
     *     allowed object is
     *     {@link CoverageFee }
     *     
     */
    public void setCoverageFee(CoverageFee value) {
        this.coverageFee = value;
    }

    /**
     * Obtiene el valor de la propiedad balanceAmount.
     * 
     * @return
     *     possible object is
     *     {@link BalanceAmount }
     *     
     */
    public BalanceAmount getBalanceAmount() {
        return balanceAmount;
    }

    /**
     * Define el valor de la propiedad balanceAmount.
     * 
     * @param value
     *     allowed object is
     *     {@link BalanceAmount }
     *     
     */
    public void setBalanceAmount(BalanceAmount value) {
        this.balanceAmount = value;
    }

}
