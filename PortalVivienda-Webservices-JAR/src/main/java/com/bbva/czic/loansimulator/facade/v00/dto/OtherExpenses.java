
package com.bbva.czic.loansimulator.facade.v00.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para otherExpenses complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="otherExpenses"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{urn:com:bbva:czic:loansimulator:facade:v00:dto}valueAppraise" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:com:bbva:czic:loansimulator:facade:v00:dto}valueStudyTitle" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:com:bbva:czic:loansimulator:facade:v00:dto}totalOtherExpenses" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "otherExpenses", propOrder = {
    "valueAppraise",
    "valueStudyTitle",
    "totalOtherExpenses"
})
public class OtherExpenses {

    @XmlElement(namespace = "urn:com:bbva:czic:loansimulator:facade:v00:dto")
    protected ValueAppraise valueAppraise;
    @XmlElement(namespace = "urn:com:bbva:czic:loansimulator:facade:v00:dto")
    protected ValueStudyTitle valueStudyTitle;
    @XmlElement(namespace = "urn:com:bbva:czic:loansimulator:facade:v00:dto")
    protected TotalOtherExpenses totalOtherExpenses;

    /**
     * Obtiene el valor de la propiedad valueAppraise.
     * 
     * @return
     *     possible object is
     *     {@link ValueAppraise }
     *     
     */
    public ValueAppraise getValueAppraise() {
        return valueAppraise;
    }

    /**
     * Define el valor de la propiedad valueAppraise.
     * 
     * @param value
     *     allowed object is
     *     {@link ValueAppraise }
     *     
     */
    public void setValueAppraise(ValueAppraise value) {
        this.valueAppraise = value;
    }

    /**
     * Obtiene el valor de la propiedad valueStudyTitle.
     * 
     * @return
     *     possible object is
     *     {@link ValueStudyTitle }
     *     
     */
    public ValueStudyTitle getValueStudyTitle() {
        return valueStudyTitle;
    }

    /**
     * Define el valor de la propiedad valueStudyTitle.
     * 
     * @param value
     *     allowed object is
     *     {@link ValueStudyTitle }
     *     
     */
    public void setValueStudyTitle(ValueStudyTitle value) {
        this.valueStudyTitle = value;
    }

    /**
     * Obtiene el valor de la propiedad totalOtherExpenses.
     * 
     * @return
     *     possible object is
     *     {@link TotalOtherExpenses }
     *     
     */
    public TotalOtherExpenses getTotalOtherExpenses() {
        return totalOtherExpenses;
    }

    /**
     * Define el valor de la propiedad totalOtherExpenses.
     * 
     * @param value
     *     allowed object is
     *     {@link TotalOtherExpenses }
     *     
     */
    public void setTotalOtherExpenses(TotalOtherExpenses value) {
        this.totalOtherExpenses = value;
    }

}
