
package com.bbva.czic.loansimulator.facade.v00.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import java.util.ArrayList;
import java.util.List;


/**
 * <p>Clase Java para amortizationTable complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="amortizationTable"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{urn:com:bbva:czic:loansimulator:facade:v00:dto}vtuPaymentCapital" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:com:bbva:czic:loansimulator:facade:v00:dto}vtuPaymentInterest" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:com:bbva:czic:loansimulator:facade:v00:dto}vtuPaymentInsurance" minOccurs="0"/&gt;
 *         &lt;element ref="{urn:com:bbva:czic:loansimulator:facade:v00:dto}vtuPercentage" minOccurs="0"/&gt;
 *         &lt;element name="monthlyAmortization" type="{urn:com:bbva:czic:loansimulator:facade:v00:dto}monthlyAmortization" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "amortizationTable", propOrder = {
    "vtuPaymentCapital",
    "vtuPaymentInterest",
    "vtuPaymentInsurance",
    "vtuPercentage",
    "monthlyAmortization"
})
public class AmortizationTable {

    @XmlElement(namespace = "urn:com:bbva:czic:loansimulator:facade:v00:dto")
    protected VtuPaymentCapital vtuPaymentCapital;
    @XmlElement(namespace = "urn:com:bbva:czic:loansimulator:facade:v00:dto")
    protected VtuPaymentInterest vtuPaymentInterest;
    @XmlElement(namespace = "urn:com:bbva:czic:loansimulator:facade:v00:dto")
    protected VtuPaymentInsurance vtuPaymentInsurance;
    @XmlElement(namespace = "urn:com:bbva:czic:loansimulator:facade:v00:dto")
    protected VtuPercentage vtuPercentage;
    @XmlElement(nillable = true)
    protected List<MonthlyAmortization> monthlyAmortization;

    /**
     * Obtiene el valor de la propiedad vtuPaymentCapital.
     * 
     * @return
     *     possible object is
     *     {@link VtuPaymentCapital }
     *     
     */
    public VtuPaymentCapital getVtuPaymentCapital() {
        return vtuPaymentCapital;
    }

    /**
     * Define el valor de la propiedad vtuPaymentCapital.
     * 
     * @param value
     *     allowed object is
     *     {@link VtuPaymentCapital }
     *     
     */
    public void setVtuPaymentCapital(VtuPaymentCapital value) {
        this.vtuPaymentCapital = value;
    }

    /**
     * Obtiene el valor de la propiedad vtuPaymentInterest.
     * 
     * @return
     *     possible object is
     *     {@link VtuPaymentInterest }
     *     
     */
    public VtuPaymentInterest getVtuPaymentInterest() {
        return vtuPaymentInterest;
    }

    /**
     * Define el valor de la propiedad vtuPaymentInterest.
     * 
     * @param value
     *     allowed object is
     *     {@link VtuPaymentInterest }
     *     
     */
    public void setVtuPaymentInterest(VtuPaymentInterest value) {
        this.vtuPaymentInterest = value;
    }

    /**
     * Obtiene el valor de la propiedad vtuPaymentInsurance.
     * 
     * @return
     *     possible object is
     *     {@link VtuPaymentInsurance }
     *     
     */
    public VtuPaymentInsurance getVtuPaymentInsurance() {
        return vtuPaymentInsurance;
    }

    /**
     * Define el valor de la propiedad vtuPaymentInsurance.
     * 
     * @param value
     *     allowed object is
     *     {@link VtuPaymentInsurance }
     *     
     */
    public void setVtuPaymentInsurance(VtuPaymentInsurance value) {
        this.vtuPaymentInsurance = value;
    }

    /**
     * Obtiene el valor de la propiedad vtuPercentage.
     * 
     * @return
     *     possible object is
     *     {@link VtuPercentage }
     *     
     */
    public VtuPercentage getVtuPercentage() {
        return vtuPercentage;
    }

    /**
     * Define el valor de la propiedad vtuPercentage.
     * 
     * @param value
     *     allowed object is
     *     {@link VtuPercentage }
     *     
     */
    public void setVtuPercentage(VtuPercentage value) {
        this.vtuPercentage = value;
    }

    /**
     * Gets the value of the monthlyAmortization property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the monthlyAmortization property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMonthlyAmortization().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MonthlyAmortization }
     * 
     * 
     */
    public List<MonthlyAmortization> getMonthlyAmortization() {
        if (monthlyAmortization == null) {
            monthlyAmortization = new ArrayList<MonthlyAmortization>();
        }
        return this.monthlyAmortization;
    }

}
