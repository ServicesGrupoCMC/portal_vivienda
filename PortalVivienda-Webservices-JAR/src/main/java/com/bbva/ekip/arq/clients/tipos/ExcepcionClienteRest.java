package com.bbva.ekip.arq.clients.tipos;

/**
 * Created by jquijano82 on 13/07/17.
 */
public class ExcepcionClienteRest extends Exception {
    public ExcepcionClienteRest(String s) {
        super(s);
    }
}
