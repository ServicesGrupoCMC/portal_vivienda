package com.bbva.ekip.arq.interceptors;

import com.bbva.ekip.arq.clients.GrantingTicket;
import com.bbva.saz.co.grantingticket.v02.AuthenticationData;
import com.bbva.saz.co.grantingticket.v02.AuthenticationRequest;
import com.bbva.saz.co.grantingticket.v02.BackendUserRequest;
import com.bbva.saz.co.grantingticket.v02.CreateTicketRequest;
import org.apache.cxf.interceptor.AbstractInDatabindingInterceptor;
import org.apache.cxf.interceptor.Fault;
import org.apache.cxf.message.Message;
import org.apache.cxf.phase.Phase;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import java.util.ArrayList;
import java.util.Map;

/**
 * Created by jquijano82 on 8/06/16.
 */
public class RequestInterceptor extends AbstractInDatabindingInterceptor {

    private final static Logger LOG = Logger.getLogger(RequestInterceptor.class);

    @Value(value = "${iv_ticketService}")
    private String KEY_AUTH_GT;

    @Value(value = "${authenticationType}")
    private String AUTH_TYPE_GT;

    @Value(value = "${grantingTicket.url}")
    private String GT_URL;

    @Value(value = "${consumerID}")
    private String CODIGO_AAP;

    @Value(value = "${userID}")
    private String USUARIO;

    @Autowired
    private GrantingTicket servicioGT;

    public RequestInterceptor() {
        super(Phase.SETUP);
    }

    @Override
    public void handleMessage(Message message) throws Fault {
        //Si no es peticion de GT - org.apache.cxf.request.uri
        if (!((String) message.get("org.apache.cxf.request.uri")).contains(GT_URL)) {
            Map<String, ArrayList<String>> headers = (Map<String, ArrayList<String>>) message.get(Message.PROTOCOL_HEADERS);
            try {
                String tsec = llamarGT();

                ArrayList<String> ltsec = new ArrayList<String>();
                ltsec.add(tsec);

                headers.put("tsec", ltsec);

            } catch (Exception e) {
                LOG.error(e);
            }

        }
    }

    private String llamarGT() throws Exception {
        String tsec = "";

        CreateTicketRequest peticionCrearGT = new CreateTicketRequest();

        //definicion authentication - INICIO
        AuthenticationRequest peticionAutenticacionGT = new AuthenticationRequest();
        AuthenticationData dataAutenticacion = new AuthenticationData();
        peticionAutenticacionGT.setAuthenticationType(AUTH_TYPE_GT);
        peticionAutenticacionGT.setConsumerID(CODIGO_AAP);
        peticionAutenticacionGT.setUserID(USUARIO);
        dataAutenticacion.setIdAuthenticationData(KEY_AUTH_GT);
        dataAutenticacion.getAuthenticationData().add("iv_ticketService");
        peticionAutenticacionGT.getAuthenticationData().add(dataAutenticacion);
        //definicion authentication - FIN

        //definicion backendUserRequest - INICIO
        BackendUserRequest peticionUsuarioBack = new BackendUserRequest();
        peticionUsuarioBack.setAccessCode("");
        peticionUsuarioBack.setDialogId("");
        peticionUsuarioBack.setUserId("");
        //definicion backendUserRequest - FIN

        peticionCrearGT.setAuthentication(peticionAutenticacionGT);
        peticionCrearGT.setBackendUserRequest(peticionUsuarioBack);

        try {
            tsec = servicioGT.createTicket(peticionCrearGT);

        } catch (Exception e) {
            throw e;
        }
        return tsec;
    }

}
