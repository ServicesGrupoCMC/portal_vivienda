package co.com.bbva.portalvivienda.back;


import com.bbva.czic.loansimulator.facade.v00.dto.DataLoanSimulation;
import com.bbva.czic.loansimulator.facade.v00.dto.LoanSimulation;

/**
 * Created by mile on 12/08/2016.
 */
public interface LoanSimulatorClient {

    DataLoanSimulation simulateMortgageLoan(LoanSimulation infoLoanSimulation, String expands) throws Exception;
}
