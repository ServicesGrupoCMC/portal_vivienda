package co.com.bbva.portalvivienda.back.impl;

import co.com.bbva.portalvivienda.back.LoanSimulatorClient;
import com.bbva.czic.loansimulator.facade.v00.SrvLoanSimulatorV00;
import com.bbva.czic.loansimulator.facade.v00.dto.DataLoanSimulation;
import com.bbva.czic.loansimulator.facade.v00.dto.LoanSimulation;
import com.bbva.ekip.arq.clients.impl.ServiceBase;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.ws.rs.core.Response;
import java.io.Serializable;

/**
 * Created by mile on 12/08/2016.
 */
@Service
public class LoanSimulatorClientImpl extends ServiceBase implements LoanSimulatorClient, Serializable {

    @Value(value = "${loansimulator.url}")
    private String URL_SERVICE;

    @Override
    public DataLoanSimulation simulateMortgageLoan(LoanSimulation infoLoanSimulation, String expands) throws Exception {
        SrvLoanSimulatorV00 srv = (SrvLoanSimulatorV00) getClient(URL_SERVICE, SrvLoanSimulatorV00.class);
        Response res = srv.simulateMortgageLoan(expands,infoLoanSimulation);
        evaluateResponse(res);
        return res.readEntity(DataLoanSimulation.class);
    }

}
