package co.com.bbva.portalvivienda.back;


import javax.ws.rs.core.Response;

import com.bbva.czic.appointments.facade.v00.dto.Appointment;

/**
 * Created by mile on 12/08/2016.
 */
public interface AppoimentClient {

    Response createAppoiment(Appointment appoiment) throws Exception;
}
