package co.com.bbva.portalvivienda.back.impl;

import javax.ws.rs.core.Response;
import java.io.Serializable;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import co.com.bbva.portalvivienda.back.AppoimentClient;

import com.bbva.czic.appointments.facade.v00.SrvAppointmentsV00;
import com.bbva.czic.appointments.facade.v00.dto.Appointment;
import com.bbva.ekip.arq.clients.impl.ServiceBase;

/**
 * Created by mile on 12/08/2016.
 */
@Service
public class AppoimentClientImpl extends ServiceBase implements AppoimentClient, Serializable {

    @Value(value = "${appoiment.url}")
    private String URL_SERVICE;

    @Override
    public Response createAppoiment(Appointment appoiment) throws Exception {
        SrvAppointmentsV00 srv = (SrvAppointmentsV00) getClient(URL_SERVICE, SrvAppointmentsV00.class);
        Response res = srv.createAppointment(appoiment);
        evaluateResponse(res);
        return res;
    }
}
