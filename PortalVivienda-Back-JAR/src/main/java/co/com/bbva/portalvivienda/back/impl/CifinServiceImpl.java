package co.com.bbva.portalvivienda.back.impl;

import co.com.bbva.portalvivienda.back.CifinService;
import com.bbva.czic.cifin.facade.v01.dto.DataCifinAnswer;
import com.bbva.czic.cifin.facade.v01.dto.DataConfronta;
import com.bbva.czic.cifin.facade.v01.dto.QuestionnaireAnswer;
import com.bbva.czic.cifin.facade.v01.impl.SrvCifinV01;
import com.bbva.ekip.arq.clients.impl.ServiceBase;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.ws.rs.core.Response;

/**
 * Created by jquijano82 on 10/07/17.
 */
@Service
public class CifinServiceImpl extends ServiceBase implements CifinService {

    @Value("${cifin.url}")
    private String URL;

    @Override
    public DataConfronta getConfrontaQuestionnaire(Integer questionnaire_code, String person_id_type, String person_id_number, String person_id_date,
                                                   String person_lastName, Integer person_departament_code, Integer person_city_code, String person_phone_number,
                                                   String sequenceQuestionnaire) throws Exception {
        SrvCifinV01 srv = (SrvCifinV01) getClient(URL, SrvCifinV01.class);
        Response resp = srv.getConfrontaQuestionnaire(questionnaire_code, person_id_type, person_id_number, person_id_date,
                person_lastName, person_departament_code, person_city_code, person_phone_number,
                sequenceQuestionnaire);
        evaluateResponse(resp);
        return resp.readEntity(DataConfronta.class);
    }

    @Override
    public DataCifinAnswer sendQuestionnaire(QuestionnaireAnswer questionnaireanswer) throws Exception {
        SrvCifinV01 srv = (SrvCifinV01) getClient(URL, SrvCifinV01.class);
        Response resp = srv.sendQuestionnaire(questionnaireanswer);
        evaluateResponse(resp);
        return resp.readEntity(DataCifinAnswer.class);
    }
}
