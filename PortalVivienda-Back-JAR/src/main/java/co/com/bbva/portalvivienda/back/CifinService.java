package co.com.bbva.portalvivienda.back;

import com.bbva.czic.cifin.facade.v01.dto.DataCifinAnswer;
import com.bbva.czic.cifin.facade.v01.dto.DataConfronta;
import com.bbva.czic.cifin.facade.v01.dto.QuestionnaireAnswer;

/**
 * Created by jquijano82 on 10/07/17.
 */
public interface CifinService {

    DataConfronta getConfrontaQuestionnaire(Integer questionnaire_code, String person_id_type, String person_id_number, String person_id_date,
                                            String person_lastName, Integer person_departament_code, Integer person_city_code, String person_phone_number,
                                            String sequenceQuestionnaire) throws Exception;

    DataCifinAnswer sendQuestionnaire(QuestionnaireAnswer questionnaireanswer) throws Exception;
}
